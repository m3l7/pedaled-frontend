// require('child_process').exec('grunt snapshots')
var spawn = require('child_process').spawn;
var gruntTerminated = true;
var CronJob = require('cron').CronJob;

var job = new CronJob('* * * * 3', run, function(){},
  true /* Start the job right now */
);

function run(){
	if (!gruntTerminated) console.log(new Date()+ " Grunt is already running!!!!!");

	var gruntInstance = spawn('grunt',['snapshots']);
	gruntInstance.stdout.on('data', function (data) {
	  gruntTerminated = false;
	  console.log(new Date()+ " GruntCron snapshots: "+data);
	});

	gruntInstance.stdout.on('close', function () {
	  gruntTerminated = true;
	  console.log("Snapshots completed ");
	});

}