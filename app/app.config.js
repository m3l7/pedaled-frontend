(function(){
    var config = {
        logging:{
          http: true
        },
        API:{
          // basePath: 'http://192.168.0.21:91',
          // basePath: 'http://pedaled.local:91/',
          // basePath: 'http://localhost:8888/moatest/api/public/',
          basePath: 'http://hideto.pedaled.com:15000/',
          idAttribute: 'entity_id',
        },
        baseUrl: "http://pedaled.com/",
        lang:{
          langs: ['it','en','de','fr'],
          defaultLang: 'it'
        },
        url:{
          prefix: '/#!'
        },
        carousel:{
          interval: 7000
        },
        popover:{
          popoverTimes: 2
        },
        screenSize:{
          xs: 767,
          md: 1025
        },
        colors: {
          '0':  [
            { code: 0, string: 'Black' },
            { code: 0, string: 'BlackDots' }
          ],
          '1':  [
            { code: 1, string: 'White' },
            { code: 1, string: 'WhiteFlag' },
          ],
          '2':  [
            { code: 2, string: 'Grey' },
            { code: 2, string: 'LightGrey' },
          ],
          '3':  [
            { code: 3, string: 'MilitaryGreen' }
          ],
          '4':  [
            { code: 4, string: 'Beige' }
          ],
          '5':  [
            { code: 5, string: 'Navy' },
            { code: 5, string: 'Slate' }
          ],
          '6':  [
            { code: 6, string: 'Red' }
          ],
          '7':  [
            { code: 7, string: 'Blue' },
            { code: 7, string: 'MarineBlue' }
          ],
          '9':  [
            { code: 9, string: 'Indigo' }
          ],
          '11': [
            { code: 11, string: 'Green' }
          ],
          '13': [
            { code: 13, string: 'Bordeaux' }
          ],
          '14': [
            { code: 14, string: 'Brown' },
            { code: 14, string: 'Khaki' }
          ],
          '16': [
            { code: 16, string: 'LightGreyMelange' }
          ],
          '27': [
            { code: 27, string: 'Petrol' }
          ],
          '47': [
            { code: 47, string: 'Blue' }
          ],
          '48': [
            { code: 48, string: 'Black'},
            { code: 48, string: 'DarkGrey'}
          ],
        }
    }

    angular
        .module('app')
        .constant('config',config)
        .config(['$locationProvider', function($location) {
          $location.hashPrefix('!');
        }])
        .config(['$compileProvider', function ($compileProvider) {
          $compileProvider.debugInfoEnabled(false);
        }])
        .config(['$httpProvider', function($httpProvider) {
          // delete $httpProvider.defaults.headers.common['X-Requested-With'];h
        }])
})();
