(function(){
    angular
        .module('app')
        .config(languages);

        languages.$inject = ["$translateProvider"];

        function languages($translateProvider){

            //set the path for languages
            $translateProvider.useStaticFilesLoader({
              prefix: '/languages/',
              suffix: '.json'
            });

            $translateProvider.translations('it', {

                //CONTATTI
                    "contatti":{
                        "chiSiamo": "Chi Siamo"
                    }

            })            

            $translateProvider.preferredLanguage('it');
        }
})();