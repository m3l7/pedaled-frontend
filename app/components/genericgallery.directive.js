(function(){
	angular
		.module("app")
		.directive("genericgallery",genericgallery);

		genericgallery.$inject = [];

		function genericgallery(){

			Controller.$inject = ["$scope", "utils", "$document"];

			return {
				restrict: "EA",
				link: link,
				templateUrl: "components/genericgallery.html",
				transclude: true,
				replace: true,
				scope: {
					items: "=",
					active: "=",
					activeIndex: "="
				},
				controller: Controller
			}

			function Controller($scope, utils, $document){
				$scope.slideLeft = slideLeft;
				$scope.slideRight = slideRight;

				document.onkeydown = function(evt) {
				    evt = evt || window.event;
				    if (evt.keyCode == 27) {
				    	$scope.$apply(function(){
				    		$scope.active = false;
				    	})
				    }

				};

				function slideLeft(){
					$scope.activeIndex--;
					if ($scope.activeIndex<0) $scope.activeIndex = $scope.items.length - 1;
					var itemsIndex = utils.indexBy($scope.items,"id");
					if (!itemsIndex[$scope.activeIndex]) $scope.activeIndex = 0;
					$scope.activeItem = itemsIndex[$scope.activeIndex];
				}
				function slideRight(){
					$scope.activeIndex++;
					var itemsIndex = utils.indexBy($scope.items,"id");
					if (!itemsIndex[$scope.activeIndex]) $scope.activeIndex = 0;
					$scope.activeItem = itemsIndex[$scope.activeIndex];
				}
			}

			function link(scope,element,attrs){
				scope.activeIndex = scope.activeIndex || 0;

			}
		}
})();