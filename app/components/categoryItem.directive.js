(function(){
	angular
		.module("app")
		.directive("categoryItem",categoryItem);

		categoryItem.$inject = [];

		function categoryItem(){

			var directive = {
				restrict: "EA",
				scope:{
					category: "="
				},
				link: link,
				templateUrl: "components/categoryItem.html",
				replace: true
			}
			return directive;

			function link(scope,element,attrs){

			}

		}
})();