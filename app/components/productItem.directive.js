//product box (whith title, image and color palette)

(function(){
	angular
		.module("app")
		.directive("productItem",productItem);

		productItem.$inject = [
			"config",
			"cartService"
		];

		function productItem(config,cartService){

			Controller.$inject = ["$scope","colorModel", "$q"];

			var directive = {
				restrict: "EA",
				scope:{
					product: "=",
					forceGalleryColor: "=" //display only a color (must be in SKU format -- i.e. EE144JSKI00 -- we have to convert it to the corresponding magento color id)
				},
				link: link,
				templateUrl: "components/productItem.html",
				replace: true,
				controller: Controller
			}
			return directive;

			function Controller($scope, colorModel, $q){

				$scope.activeImageIndex = 0;
				$scope.activeColor = null;
				$scope.changeColor = changeColor;
				$scope.cartService = cartService;
				$scope.openProductColor = openProductColor;

				initialize();

				function initialize(){
					$scope.$watch("product",function(newValue,oldValue){
						var color;

						//if forceGalleryColor is set, convert it to a magento color id
						if ($scope.forceGalleryColor) convertColorSkuToMagento($scope.forceGalleryColor)
							.then(function(color){
								changeColor(color);
							})

						else if ((newValue.color) && (newValue.color.length)) {
							//take the first color available
							color = newValue.color[0];
							changeColor(color);	
						}
					})
				}

				function changeColor(color){
					if (color) {
						$scope.activeImageIndex = searchGalleryForColor(color);
						$scope.activeColor = color;
					}
				}

				function searchGalleryForColor(color){
					//color: color object || color string/number
					if (typeof color == "string") color = parseInt(color);
					var colorIndex = null;
					if (color){
						$scope.product.gallery.forEach(function(galleryItem,i){
							if (typeof colorIndex=="number") return;
							var filename = galleryItem.replace(/^.*\/|\.[^.]*$/g, '');
							var galleryColorId;
							if(isNumber(filename.slice(9,13))){ 
								galleryColorId = parseInt(filename.slice(9,13));
							} else {
								galleryColorId = parseInt(filename.slice(9,11));
							}
							var galleryColor;
							if(!isNumber(galleryColorId)){
								galleryColor = galleryColorId;
							}else {
								galleryColor = config.colors[galleryColorId];
							}

							if ((typeof color == "number") && (color==galleryColorId)) colorIndex = i; 
							else{
								var galleryColors = config.colors[galleryColorId];
								if (galleryColors) galleryColors.forEach(function(galleryColor){
									if (galleryColor.string==color.admin_label) colorIndex = i;
								})
							}
						})
					}
					return colorIndex || 0;
				}

				function isNumber (o) {
  					return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
				}


				function convertColorSkuToMagento(color){
					//color is a SKU color. Search colors from magento and get the magento ID
					var colorsObj = config.colors[parseInt(color)];

					if ((!colorsObj) || (!colorsObj.length)) return $q.resolve();

					return colorModel.findAll()
					.then(function(colors){
						var ret;
						colors.forEach(function(magentoColor){
							if (ret) return;
							colorsObj.forEach(function(colorObj){
								if (magentoColor.admin_label==colorObj.string) ret = magentoColor;
							})
						})

						return ret;
					})
				}

				function openProductColor(color){
					var color = ((color) && (typeof color=="object")) ? color.admin_label.toLowerCase() : color; //color = color name OR color id

					var url;
					if ($scope.product.urlKey) url = "/#!/products/"+$scope.product.urlKey;
					else url = "/#!/products/id/"+$scope.product.id;
					if (color)  url += "/"+color;	

					window.location.href = url;
				}
			}

			function link(scope,element,attrs){

			}
		}
})();
