(function(){
	angular
		.module("app")
		.directive("customVideo",customVideo);

		customVideo.$inject = [];

		function customVideo(){

			controller.$inject = ["$scope"];

			return {
				scope:{
					videosrc: "="
				},
				restrict: "EA",
				link: link,
				templateUrl: "components/customVideo.html",
				controller: controller
			};

			function controller($scope){
				$scope.muted = true;

			}

			function link(scope,element,attributes){
				var video = angular.element(element[0].querySelectorAll("video"));
				scope.toggleVolume = toggleVolume;
				video[0].muted = true;

				function toggleVolume(){
					var video = angular.element(element[0].querySelectorAll("video"));

					if (video.length){
						video[0].muted = !video[0].muted;
						scope.muted = video[0].muted;
					}

				}

	
			}
		}
})();