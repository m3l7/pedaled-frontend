(function(){
	angular
		.module('app')
		.directive('multiSlider',multiSlider);

		multiSlider.$inject = ["$timeout"];

		function multiSlider($timeout){

			Controller.$inject = ['$scope'];

			return {
				scope:{
					slides: '=', //slides data
					activeImage: '=',
					items: '@', //number of slides to show
					ratio: "@",
					height: "@",
					itemsToSlide: "@" //number of items to slide when right and left button are clicked
				},
				link: link,
				restrict: 'EA',
				replace: true,
				controller: Controller,
				templateUrl: "components/multislider.html"
			};

			function Controller($scope){
				$scope.slideLeft = slideLeft;
				function slideLeft(){

				}
			}

			function link(scope,element,attrs){
				scope.element = element;
				scope.items = parseInt(scope.items);
				scope.itemsToSlide = parseInt(scope.itemsToSlide);
				scope.ratio = parseInt(scope.ratio);

				scope.rightSlides = [];
				scope.leftSlides = [];
				scope.carouselPosition = 0;
				scope.activeIndex = 0;

				scope.getElementDimensions = getElementDimensions;
				scope.slideLeft = slideLeft;
				scope.slideRight = slideRight;

				initialize();

				function initialize(){
					initSlides();

					scope.$watch(scope.getElementDimensions, function (newValue, oldValue) {
						scope.slideLength = newValue.w/scope.items;
						scope.slideHeight = newValue.w/scope.items/scope.ratio;
						scope.carouselHeight = (scope.ratio) ? newValue.w/scope.items/scope.ratio : scope.height;
					}, true);

					scope.$watch("slides",function(newValue,oldValue){
						console.log("slides: "+scope.slides.length)
						if (newValue!=oldValue) initSlides();
					})

				}

				function initSlides(){	
					var index;
					scope.rightSlides = [];
					scope.leftSlides = [];
					scope.carouselPosition = 0;
					scope.activeIndex = 0;
					if (!scope.slides.length) return false;

					//Add images to gallery
					for (index=0;index<(scope.items);index++) addSlide(index,false);	

					//Add left and right buffer to gallery
					for (index=scope.items;index<(scope.itemsToSlide+scope.items);index++) {
						addSlide(index,false);	
					}
					for (index=1;index<=scope.itemsToSlide;index++) {
						addSlide(-index,true);	
					}
				}

				function addSlide(index,left){
					if (!index) index = 0;

					while (index>=scope.slides.length) index-=scope.slides.length;
					while (index<0) index+=scope.slides.length;

					if (left){
						//slides images array
						for (var i=(scope.leftSlides.length-1);i>=0;i--){
							scope.leftSlides[i+1] = scope.leftSlides[i];
						}							
						scope.leftSlides[0] = {
							image: scope.slides[index],
							index: index
						}
					}
					else scope.rightSlides.push({
						image: scope.slides[index],
						index: index
					});
				}

				function addNextSlide(left){
					var index = (left) ? scope.leftSlides[0].index : scope.rightSlides[scope.rightSlides.length-1].index;
					if (!index) index = 0;
					index = (left) ? (index-1) : (index+1);

					addSlide(index,left);
				}

				function slideRight(){
					scope.activeIndex += parseInt(scope.itemsToSlide);
					for (var i=0;i<scope.itemsToSlide;i++) addNextSlide(false);
					scope.carouselPosition -= (scope.slideLength*scope.itemsToSlide);
					scope.activeImage.url = activeImageFromIndex(scope.activeIndex).image;
				}
				function slideLeft(){
					scope.activeIndex -= parseInt(scope.itemsToSlide);
					for (var i=0;i<scope.itemsToSlide;i++) addNextSlide(true);
					scope.carouselPosition += (scope.slideLength*scope.itemsToSlide);
					scope.activeImage.url = activeImageFromIndex(scope.activeIndex).image;
				}

				function activeImageFromIndex(index){
					if (!index) index = 0;

					var imageIndex = index + parseInt(scope.items) -2;
					return (imageIndex>=0) ? (scope.rightSlides[imageIndex]) : (scope.leftSlides[scope.leftSlides.length+imageIndex]);
				}


				function getElementDimensions() {
					return { 'h': element[0].offsetHeight, 'w': element[0].offsetWidth};
				};


			}
		}
})();