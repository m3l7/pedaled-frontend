// Adapt element height/max-height to window:    window.height*offsetMult - offset

(function(){
	angular
		.module("app")
		.directive("adaptToWindow",adaptToWindow);

		adaptToWindow.$inject = ["$window"];

		function adaptToWindow($window){
			return {
				scope:{
					maxHeight: "@",
					offset: "@",
					offsetMult: "@"
				},
				restrict: "EA",
				link: link
			}

			function link(scope,element,attrs){
				var offset = parseInt(scope.offset || 0);
				var offsetMult = parseFloat(scope.offsetMult || 0.8);
				var attribute = scope.maxHeight ? "max-height" : "height";

				element.css(attribute,($window.innerHeight*offsetMult-offset)+"px");
				scope.getWindowDimension = getWindowDimension;

				scope.$watch(scope.getWindowDimension, function (newValue, oldValue) {
					if (newValue!=oldValue) element.css(attribute,(newValue.h*offsetMult-offset)+"px");
				}, true);

				function getWindowDimension() {
				    return { 'h': $window.innerHeight, 'w': $window.innerWidth };
				};
			}
		}
})();