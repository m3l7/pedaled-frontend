(function(){
	angular
		.module('app')
		.directive('alertCart',alertCart);

		alertCart.$inject = ['$timeout', "alertService", "cartService"];

		function alertCart($timeout, alertService, cartService){
			var directive = {
				link: link,
				restrict: 'EA',
				scope:{
					message: '='
				},
				replace: 'true',
				templateUrl: "components/alertCart.html"
			};

			return directive;

			function link(scope,element,attrs){
				scope.cartService = cartService;
				scope.$watch('message.name',function(newm,oldm){
					if (newm){
						var timeout = scope.message.timeout || 5000;
						$timeout(function(){
							delete scope.message.name;
						},timeout)
					}
				})
			}
		}

})();