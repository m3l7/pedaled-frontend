//watch a <select> click event and pass the value of the <option> to the callback function
(function(){
	angular
		.module("app")
		.directive("onchange",onchange);

		onchange.$inject = [];

		function onchange(){
			return{
				scope:{
					onchange: "&"
				},
				restrict: "A",
				link: link
			};

			function link(scope,element,attrs){
				if (typeof element[0].onchange == "function") element[0].onchange = function(event){
					if (typeof scope.onchange()=="function") scope.onchange()(event.target.value);
				}
			}
		}
})();