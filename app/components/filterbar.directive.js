(function(){
	angular
		.module("app")
		.directive("filterbar",filterbar);

		filterbar.$inject = [];

		function filterbar(){
			Controller.$inject = ["$scope"];

			return {
				restrict: "EA",
				replace: true,
				templateUrl: "components/filterbar.html",
				scope:{
					colors: "=",
					sizes: "=",
					filters: "=",
					searchtext: "="
				},
				controller: Controller
			}


			function Controller($scope){
				$scope.toggleFilterMenu = toggleFilterMenu;
				$scope.activeMenu = null;
				$scope.resetFilter = resetFilter;

				function resetFilter(filter){
					if (filter) delete $scope.filters[filter];
					else {
						$scope.filters.color = {};
						$scope.filters.size = {};
						$scope.searchtext = null;
					}
				}

				function toggleFilterMenu(menu){
					if (!menu) $scope.activeMenu = null;
					else if ($scope.activeMenu==menu) $scope.activeMenu = null;
					else $scope.activeMenu = menu;
				}
			}
		}
})();