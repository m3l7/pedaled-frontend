(function(){
	angular
		.module("app")
		.directive("breadcrumb",breadcrumb);

		breadcrumb.$inject = [];

		function breadcrumb(){

			var template = '<div ng-show="breadcrumb.length" class="breadcrumb">'+
			   '<div ng-repeat="item in breadcrumb" class="breadcrumb-item">'+
			 	  '<span ng-show="$index&gt;0" class="breadcrumb-separator">></span>'+
			 	  '<a ng-href="{{item.link}}">{{item.text}}</a>'+
			   '</div>'+
			 '</div>';

			return {
				restrict: "EA",
				scope:{
					breadcrumb: "="
				},
				template: template,
				replace: false
			};
		}
})();