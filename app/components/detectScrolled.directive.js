(function(){
    angular
        .module('app')
        .directive('detectScrolled',detectScrolled);

        detectScrolled.$inject = ['$window','$timeout']

        function detectScrolled($window,$timeout){

            var directive = {
                link: link,
                restrict: 'EA',
                scope: {
                    top: '@',
                    shrinked: "="
                },
            }

            return directive;

            function link(scope,el,attrs){

                init();
                scope.$watch("shrinked",function(newv,oldv){
                    if (newv!=oldv) init();
                })

                $timeout(function(){
                },2000)

                function init(){
                    if (!scope.shrinked) {
                        window.addEventListener("scroll",detect);
                        detect();
                    }
                    else{
                        window.removeEventListener("scroll",detect);
                        el.addClass("shrinked");
                    }
                }

                function detect(){
                    var distanceY = window.pageYOffset || document.documentElement.scrollTop;
                    if (distanceY > scope.top) {
                        el.addClass('shrinked');
                    }
                    else{
                        el.removeClass('shrinked');
                    }
                }
            }
        }
})();
