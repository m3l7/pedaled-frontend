(function(){
	angular
		.module("app")
		.directive("gallery",gallery);

		gallery.$inject = [];

		function gallery(){

			Controller.$inject = ["$scope", "utils", "$document"];

			return {
				restrict: "EA",
				link: link,
				templateUrl: "components/gallery.html",
				transclude: true,
				replace: true,
				scope: {
					items: "=",
					// activeItem: "=",
				},
				controller: Controller
			}

			function Controller($scope, utils, $document){
				$scope.slideLeft = slideLeft;
				$scope.slideRight = slideRight;

				document.onkeydown = function(evt) {
				    evt = evt || window.event;
				    if (evt.keyCode == 27) {
				    	$scope.$apply(function(){
				    		$scope.active = false;
				    	})
				    }

				};

				function slideLeft(){
					$scope.activeIndex--;
					if ($scope.activeIndex<0) $scope.activeIndex = $scope.items.length - 1;
					var itemsIndex = utils.indexBy($scope.items,"id");
					if (!itemsIndex[$scope.activeIndex]) $scope.activeIndex = 0;
					$scope.activeItem = itemsIndex[$scope.activeIndex];
				}
				function slideRight(){
					$scope.activeIndex++;
					var itemsIndex = utils.indexBy($scope.items,"id");
					if (!itemsIndex[$scope.activeIndex]) $scope.activeIndex = 0;
					$scope.activeItem = itemsIndex[$scope.activeIndex];
				}
			}

			function link(scope,element,attrs){

				scope.active = false;
				scope.activeIndex = 0;
				scope.activeItem = {};

				var galleryItems = angular.element(element[0].querySelectorAll(".gallery-item"));

				angular.forEach(galleryItems,function(galleryItem){
					angular.element(galleryItem).unbind("click");
					angular.element(galleryItem).bind("click",function(){
						activateGallery(angular.element(galleryItem));
					})
				})

				function activateGallery(item){

					var activeIndex = 0;
					var activeItem = {};
					if (item){
						//ugly shit -- fix me when real backend is ready
						var style = item.find("div").attr("style");
						var bg = style.split("url(")[1].split(")")[0];
						bg = bg.slice(1,-1);	

						//compute gallery index
						scope.items.forEach(function(item){
							if (item.small==bg) {
								activeIndex = item.id;
								activeItem = item;
							}	
						})
					}
					scope.$apply(function(){
						scope.active = true;
						scope.activeIndex = activeIndex;
						scope.activeItem = activeItem;
					})

				}
			}
		}
})();