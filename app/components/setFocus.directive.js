//set focus when the variable becomes true

(function(){
	angular
		.module("app")
		.directive("setFocus",setFocus);

		setFocus.$inject = [];

		function setFocus(){
			return {
				scope:{
					setFocus: "="
				},
				restrict: "A",
				link: link
			}

			function link(scope,elem,attrs){
				scope.$watch("setFocus", function(newV,oldV){
					if ((newV!=oldV) && (newV)) elem[0].focus();
				})
			}
		}
})();