(function(){
	angular
		.module("app")
		.directive("popover",popover);

		popover.$inject = [];

		function popover(){

			Controller.$inject = ["$scope", "NewsletterService", "$timeout"];

			return {
				scope:{
					closed: "="
				},
				link: link,
				restrict: "EA",
				replace: true,
				templateUrl: "components/popover.html",
				controller: Controller
			};

			function Controller($scope, NewsletterService, $timeout){
				$scope.email = null;
				$scope.subscribe = subscribe;
				$scope.subscribing = false;
				$scope.subscribed = false;

				function subscribe(){
				    if ($scope.email) {
				        $scope.subscribing = true;
				        $scope.alertMessage = null;
				        NewsletterService.subscribe($scope.email)
				        .then(function(response){
				            $scope.subscribing = false;
				            $scope.alertMessage = "you have been subscribed to the newsletter";
				            $scope.subscribed = true;

				            $timeout(function(){
				            	$scope.closed = true;
				            },2000)
				        })
				        .catch(function(err){
				            $scope.subscribing = false;
				            $scope.alertMessage = err;
				        })
				    }

				}

			}

			function link(scope,element,attrs){

			}
		}
})();