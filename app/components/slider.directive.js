(function(){
	angular
		.module('app')
		.directive('slider',slider);

		slider.$inject = ['$window',"$interval","config","utilsUI","commonData"];

		function slider($window,$interval,config,utilsUI, commonData){

			Controller.$inject = ["$scope"];

			return {
				scope:{
					slides: "=",
					activeImages: '='
				},
				link: link,
				restrict: 'EA',
				replace: true,
				templateUrl: "components/slider.html",
				controller: Controller
			};

			function Controller($scope){

				$scope.openLink = openLink;

				function openLink(link){
				    window.location.href = link;
				}

			}

			function link(scope,element,attrs){
				scope.element = element;
				scope.slidesPages = [];
				scope.slideLeft = slideLeft;
				scope.slideRight = slideRight;
				scope.getElementDimensions = getElementDimensions;
				scope.getWindowDimension = getWindowDimension;
				scope.changePage = changePage;
				scope.changePageAnimated = changePageAnimated;
				scope.scrollDown = scrollDown;
				scope.isMobile = false;

				initialize();

				function initialize(){
					scope.isMobile = isMobile.any;

					scope.items = 1;
					var heightOffset = (commonData.viewPort=="XS") ? 90 : 30;
					scope.height = $window.innerHeight- heightOffset;

					scope.$watch(scope.getWindowDimension, function (newValue, oldValue) {
						if (newValue!=oldValue) scope.height = newValue.h- heightOffset;
					}, true);

					buildVideoControls();

					changePage(0);
				}

				function scrollDown(){
					utilsUI.scrollWindowTo($window.innerHeight/100*80,200);
				}

				function slideLeft(){
					if (scope.slides.length>1){
						scope.element.removeClass('right');
						scope.element.addClass('left');
						changePage(scope.currentPage-1);
					}
				}
				function slideRight(){
					if (scope.slides.length>1){
						scope.element.removeClass('left');
						scope.element.addClass('right');
						changePage(scope.currentPage+1);
					}
				}

				function changePageAnimated(page){

					if (scope.currentPage<page){
						scope.element.removeClass('left');
						scope.element.addClass('right');
						changePage(page);
					}
					else if (scope.currentPage>page){
						scope.element.removeClass('right');
						scope.element.addClass('left');
						changePage(page);
					}
				}

				function setInterval(){
					scope.interval = $interval(function(){
						slideRight();
					},config.carousel.interval);	
				}

				function changePage(page){

					var items = parseInt(scope.items);
					if (typeof page == 'number'){
						while (page>=scope.slides.length) page -= scope.slides.length;
						while (page<0) page+=scope.slides.length;
						scope.currentPage = page;

						var firstSlide = page * items;

						//build slides to be included to the DOM
						scope.slidesPages = [[scope.slides[page]]];

						// stop the interval if the slide is a video, otherwise start it
						if (scope.slides[scope.currentPage].video) $interval.cancel(scope.interval);
						else if (!scope.interval) setInterval();
					}
				}

				function getElementDimensions() {
				    return { 'h': element.height(), 'w': element.width() };
				};
				function getWindowDimension() {
				    return { 'h': $window.innerHeight, 'w': $window.innerWidth };
				};

				function openLink(link){
				    window.location.href = link;
				}

				function buildVideoControls(){
					// var video = document.getElementById("video");
					var video = angular.element(element[0].querySelectorAll(".video"));

					// debugger


				}

			}
		}
})();