(function(){
	angular
		.module("app")
		.filter("escape",escape);

		escape.$inject = [];

		function escape(){
			return window.encodeURIComponent;
		}
})();
