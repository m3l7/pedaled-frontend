(function(){
	angular
		.module("app")
		.filter("productText",productText);

		function productText(){
			return function(items,filter){
				var out = [];

				if ((!filter) || (!items)) return items;
				else return items.filter(function(item){
					if (item.description.toLowerCase().indexOf(filter.toLowerCase())!=-1) return true;
					if (item.name.toLowerCase().indexOf(filter.toLowerCase())!=-1) return true;
					else return false;
				})
			}
		}
})();