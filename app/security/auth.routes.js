(function(){
    angular
        .module('app')
        .run(authRoutes);

        authRoutes.$inject = ['$rootScope','$location','security'];

        function authRoutes($rootScope, $location,security){
            $rootScope.$on('$routeChangeStart', function (event, next) {
                if ((!!next) && (!!next.data) && (!!next.data.roles) && (next.data.roles.length)){
                    var roles = next.data.roles;
                    if (!security.isAuthorized(roles)) {
                      event.preventDefault();
                      if (security.isAuthenticated) {
                        // user is not allowed
                        $location.path('/forbidden');
                      } else {
                        // user is not logged in
                        $location.path('/login');
                      }
                    }
                    else if (next.originalPath=='/admin') {
                        $location.path('/admin/video');
                    }
                }

            });
        }
})();