(function(){
  angular
    .module('app')
    .factory('security',Security);

    Security.$inject = ['$http','$timeout','$location','config',"$q"];

    function Security($http,$timeout,$location,config,$q){

      var service = {
        logout: logout,
        login: login,
        fetchAccount: fetchAccount,
        isAuthenticated: isAuthenticated,
        user: null
      };

      return service;

      function logout(){
        return $http.get(config.API.basePath+'account/logout',{withCredentials:true})
        .then(function(response){
            if ((response.status!=200) || (!response.data.success)) return $q.reject("Can't logout");
            else{
              service.user = null;
              return true;
            }
        })
        .catch(function(err){
          console.warn("can't logout: "+err);
          return $q.reject(err);
        })
      }

      function login(email,pass){
        return $http.post(config.API.basePath+'account/login?email='+email+'&password='+pass,{},{withCredentials:true})
        .then(function(response){
            var data = response.data;
            if (response.status==401) return $q.reject("Wrong email or password");
            else if (response.status!=200) return $q.reject();
            else if (!data.success) return $q.reject("Wrong email or password");
            else if (data.model){
              service.user = data.model;
              return service.user;
            }
            else return $q.reject("Invalid response from server");
        })
        .catch(function(err){
          err = err || "Could not login. Server Error";
          console.warn(err);
          return $q.reject(err);
        })
      }

      function fetchAccount(options){
        options = options || {};
        if ((!options.bypassCache) && (service.user)) return $q(function(resolve){resolve(service.user)});
        else return $http.get(config.API.basePath+"account",{withCredentials:true})
          .then(function(data){
            if ((data) && (data.data) && (data.data.model) && (data.data.model.email)) service.user = data.data.model;
            else service.user = null;

            return service.user;
          })
          .catch(function(err){
            console.warning(err);
            return $q.reject(err);
          })
      }

      function isAuthenticated(){
        return (service.user) ? true : false;
      }
      
    }

})();