// Intercept unauthorized response from backend and redirect to login

(function(){
    angular
        .module('app')
            .factory('authHttpInterceptor',authHttpInterceptor);
        angular
            .module('app')
            .config(configInterceptor);

        configInterceptor.$inject = ["$httpProvider"];
        authHttpInterceptor.$inject = ["$location","$q"];

        function authHttpInterceptor($location,$q){
            return {
                responseError: function(response){
                    if ([401,403,419,440].indexOf(response.status)!=-1){
                        //redirect to login page
                        $location.path('/login');
                        return response;
                    }
                    else return response;
                }
            };
        }

        function configInterceptor($httpProvider){
            $httpProvider.interceptors.push('authHttpInterceptor');
        }
})();