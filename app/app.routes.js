(function(){
  angular
    .module('app')
    .config(route);

    route.$inject = ["$routeProvider"];

    function route($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'pages/home.html',
          controller: 'Home',
        })
        .when('/checkout/cart', {
          templateUrl: 'pages/checkout/cart.html',
          controller: 'Cart',
        })
        .when('/checkout/signin', {
          templateUrl: 'pages/checkout/signin.html',
          controller: 'Signin',
        })
        .when('/checkout/billing/:param', {
          templateUrl: 'pages/checkout/billing.html',
          controller: 'Billing',
        })
        .when('/checkout/billing', {
          templateUrl: 'pages/checkout/billing.html',
          controller: 'Billing',
        })
        .when('/checkout/shipping', {
          templateUrl: 'pages/checkout/shipping.html',
          controller: 'Shipping',
        })
        .when('/checkout/payment', {
          templateUrl: 'pages/checkout/payment.html',
          controller: 'Payment',
        })
        .when('/checkout/review', {
          templateUrl: 'pages/checkout/review.html',
          controller: 'Review',
        })
        .when('/checkout/payment', {
          templateUrl: 'pages/checkout/review.html',
          controller: 'Review',
        })
        .when('/checkout/paypal', { //return callback from paypal
          templateUrl: 'pages/checkout/paypalCallback.html',
          controller: 'PaypalCallback',
        })
        .when('/checkout/complete', {
          templateUrl: 'pages/checkout/complete.html',
          controller: 'Complete',
        })
        .when('/products/s', { //products, search mode
          templateUrl: 'pages/products/products.html',
          controller: 'Products',
          resolve:{
            searchMode: function(){return true;}
          }
        })

        //PRODUCT DETAIL (BY ID/NAME/URLKEY)
        .when('/products/id/:id/:colorId', {
          templateUrl: 'pages/products/product.html',
          controller: 'Product',
        })
        .when('/products/id/:id', {
          templateUrl: 'pages/products/product.html',
          controller: 'Product',
        })
        .when('/products/name/:name/:colorId', {
          templateUrl: 'pages/products/product.html',
          controller: 'Product',
        })
        .when('/products/name/:name', {
          templateUrl: 'pages/products/product.html',
          controller: 'Product',
        })
        .when('/products/:urlkey/:colorId', {
          templateUrl: 'pages/products/product.html',
          controller: 'Product',
        })
        .when('/products/:urlkey', {
          templateUrl: 'pages/products/product.html',
          controller: 'Product',
        })


        .when('/category/:catName', {
          templateUrl: 'pages/products/category.html',
          controller: 'Category',
        })
        .when('/products', {
          templateUrl: 'pages/products/products.html',
          controller: 'Products',
          resolve:{
            searchMode: function(){return false;}
          }
        })
        .when('/newarrivals', {
          templateUrl: 'pages/products/landingPage.html',
          controller: 'LandingPage',
          resolve:{
            shopId: function(){return 1;}
          }
        })
        .when('/limited', {
          templateUrl: 'pages/products/landingPage.html',
          controller: 'LandingPage',
          resolve:{
            shopId: function(){return 2;}
          }
        })
        .when('/bestseller', {
          templateUrl: 'pages/products/landingPage.html',
          controller: 'LandingPage',
          resolve:{
            shopId: function(){return 3;}
          }
        })
        .when('/shop/:shopId', {
          templateUrl: 'pages/products/landingPage.html',
          controller: 'LandingPage',
          resolve:{
            shopId: function(){return;}
          }
        })
        .when('/stories', {
          templateUrl: 'pages/stories.html',
          controller: 'Stories',
        })
        .when('/stories/:id', {
          templateUrl: 'pages/story.html',
          controller: 'Story',
        })
        .when('/faq', {
          templateUrl: 'pages/faq.html',
          controller: 'Faq',
        })
        .when('/work', {
          templateUrl: 'pages/work.html',
          controller: 'Work',
        })
        .when('/about', {
          templateUrl: 'pages/about.html',
          controller: 'About',
        })
        .when('/account', {
          templateUrl: 'pages/account.html',
          controller: 'Account',
        })
        .when('/contact', {
          templateUrl: 'pages/contact.html',
          controller: 'Contact',
        })
        .when('/instagram', {
          templateUrl: 'pages/instagram.html',
          controller: 'Instagram',
        })
        .when('/dressliveride', {
          templateUrl: 'pages/instagram.html',
          controller: 'Instagram',
        })
        .when('/storefinder', {
          templateUrl: 'pages/storefinder.html',
          controller: 'Storefinder',
        })
        .when('/fontello', {
          templateUrl: 'pages/fontello.html',
        })
        .otherwise({
          redirectTo: '/'
        });
    };  
})();
