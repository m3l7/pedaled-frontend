(function(){
	angular
		.module('app')
		.controller('Body',Body);

		Body.$inject = ['$scope','$location',"config", "alertService", "security","$timeout","utils"];

		function Body($scope,$location,config, alertService, security,$timeout,utils){
			$scope.getBodyClass = getBodyClass;
			$scope.popoverClosed = true;
			$scope.alertService = alertService;

			initialize();

			function initialize(){
				//check login status
				security.fetchAccount()
				.then(function(user){
					if (!user){
						var siteLoadCounter = parseInt(localStorage.getItem("siteLoadCounter"));
						siteLoadCounter = (siteLoadCounter) ? siteLoadCounter+1: 1;
						localStorage.setItem("siteLoadCounter",siteLoadCounter);

						if (siteLoadCounter<=config.popover.popoverTimes) {
							$timeout(function(){
								$scope.popoverClosed = false;
							},15000);
						}
					}
				})

			}

			function getBodyClass(){
				return $location.path().split('/')[1];
			}

		}
})();