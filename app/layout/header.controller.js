(function(){
    angular
        .module('app')
        .controller('Header',Header);

        Header.$inject = [
            '$scope',
            'commonData',
            "$location",
            "utils",
            "cartService",
            "security",
            "HeaderService",
            "colorModel",
            "sizeModel",
            "$timeout",
            "categoryModel",
            "NewsletterService"
        ];

        function Header($scope,commonData,$location,utils,cartService,security, HeaderService,colorModel,sizeModel, $timeout,categoryModel, NewsletterService){

            $scope.commonData = commonData;
            $scope.cartService = cartService;
            $scope.HeaderService = HeaderService;
            $scope.security = security;
            $scope.customMessage = commonData.customMessage;
            $scope.mobileMenuActive = commonData.mobileMenuActive;
            $scope.infoMenuActive = false;
            $scope.menuActive = commonData.menuActive;
            $scope.activeBlock = "login";
            $scope.registerError = null;
            $scope.login = {};
            $scope.stores = [];
            $scope.activeStore = {};
            $scope.loggingIn = false;
            $scope.registering = false;
            $scope.newsletter = {
                email: null,
                alertMessage: null,
                loading: false
            };
            $scope.gotoAccount = gotoAccount;

            //methods
            $scope.toggleMenu = HeaderService.toggleMenu;
            $scope.login = cartService.login;
            $scope.selectChange = selectChange;
            $scope.toggleInfoMenu = HeaderService.toggleInfoMenu;
            $scope.register = register;
            $scope.openLink = openLink;
            $scope.login = login;
            $scope.search = search;
            $scope.newsletterSubscribe = newsletterSubscribe;
            $scope.logout = logout;
            

            $scope.infoMenu = [ // move to the CMS in the future
                {
                    type: 'void',
                    class: "col-md-3"
                },
                {
                    type: "emailsubscribe",
                    class: "col-md-3"
                },
                {
                    type: "registration",
                    class: "col-md-3"
                },
                {
                    type: "login",
                    class: "col-md-3"
                },
            ]
            $scope.menu = [ // move to the CMS in the future
                {
                    backgroundColor: 'rgba(0,0,0,0.95)',
                    color: 'white',
                    name: 'Shop',
                    mainLink: "/#!/products",
                    submenu:[
                        {
                            type: 'categoryList',
                            name: '',
                            links:[
                            ]
                        },
                        {
                            type: 'categoryList',
                            name: 'New arrivals',
                            href: "/#!/newarrivals",
                            links:[
                                { name:'Kaido jersey long sleeve', href: '/#!/products/name/kaido jersey long'},
                                { name:'Kaze access vest', href: '/#!/products/name/kaze access vest'},
                                { name:'Daikann winter bibshort', href: '/#!/products/name/daikann winter bibshort'},
                                { name:'Ultralight baselayer', href: '/#!/products/name/ultralight baselayer'},
                                { name:'See All', href: '/#!/newarrivals'},
                            ]
                        },
                        {
                            type: 'categoryList',
                            name: 'Shop by categories ',
                            href: "/#!/products",
                            links:[
                                { name:'Jerseys & baselayers', href: '/#!/category/Jerseys & Baselayers'},
                                { name:'Bib shorts', href: '/#!/category/Bib%20Shorts'},
                                { name:'Jackets', href: '/#!/category/Jackets'},
                                { name:'Tops & T-shirts', href: '/#!/category/Tops%20&%20T-Shirts'},
                                { name:'Trousers & Shorts', href: '/#!/category/Trousers%20&%20Shorts'},
                                { name:'Accessories & Shoes ', href: '/#!/category/Accessories%20&%20Shoes'},
                            ]
                        },
                        {
                            type: 'categoryList',
                            name: 'Limited & special edition',
                            href: "/#!/limited",
                            links:[
                                { name:'Transcontinental', href: '/#!/products/name/transcontinetal cap 2015'},
                                { name:'Retro Wool Polo', href: '/#!/products/name/retro wool polo'},
                                // { name:'Retro Wool Polo', href: '/#!/products'},
                                // { name:'CMWC belt', href: '/#!/products'},
                                { name:'See All', href: '/#!/limited', class: "bold"},
                            ]
                        },
                        {
                            type: 'shopAll'
                        }
                    ]
                },
                {
                    backgroundColor: '#656725',
                    color: 'white',
                    name: 'What\'s new',
                    link: "/#!/newarrivals",
                    mobileLink: "/#!/newarrivals"
                },
                {
                    backgroundColor: '#656725',
                    color: 'white',
                    name: 'About',
                    link: "/#!/about",
                    mobileLink: "/#!/about"
                },
                {
                    // backgroundColor: '#656725',
                    backgroundColor: 'rgba(0,0,0,0.95)',
                    color: 'white',
                    name: 'Stories',
                    mainLink: "/#!/stories",
                    mobileLink: "/#!/stories",
                    submenu:[
                        {
                            type: 'description',
                            link: "",
                            text: '',
                        },
                        {
                            type: 'description',
                            link: "/#!/instagram",
                            text: '<h3>#dressliveride</h3><p>Discover the most inspiring way to dress, live and ride around the world and be part of this journey.</p>',
                        },
                        {
                            type: 'social'
                        },
                        {
                            type: 'categoryList',
                            name: 'Stories',
                            links:[
                                { name:'See All', href: '/#!/stories'},
                                { name:'Australia Great Ocean Road', href: '/#!/stories/Australia-Great-Ocean-Road'},
                                { name:'The Transcontinental Race 2015 Is Over', href: '/#!/stories/The-Transcontinental-Race-2015-is-over'},
                                { name:'The Transcontinental Race', href: '/#!/stories/Transcontinental-Race-2015'},
                                { name:'From Japan To The World', href: '/#!/stories/From-Japan-To-The-World'},
                                { name:'Artcrank', href: '/#!/stories/Artcrank'},
                                // { name:'Castaway On A Bicycle', href: '/#!/stories/3'},
                                // { name:'CMWC 2015', href: '/#!/stories/4'}
                            ]
                        },
                    ]
                },

            ];
            
        	initialize();

        	function initialize(){
                commonData.breadcrumb = [];
                $scope.registrationInfo = {};

                if(!security.isAuthenticated()) {
                    $scope.login.email = localStorage.getItem("email");
                    $scope.login.password = localStorage.getItem("password");
                }

                cartService.fetchBasket()
                .catch(console.error);

                security.fetchAccount()
                .then(function(user){
                    if (user) $scope.user = user;
                })

                colorModel.findAll();
                colorModel.bindAll({},$scope,"colors");
                sizeModel.findAll();
                sizeModel.bindAll({},$scope,"sizes");

                cartService.fetchStores()
                .then(function(stores){
                    $scope.stores = stores;
                })
        	}

            function register(){

                //remove additional billing_address parameters (they may be filled from previous sessions)
                cartService.billing_address.city = null;
                cartService.billing_address.company = null;
                cartService.billing_address.country_id = null;
                cartService.billing_address.middlename = null;
                cartService.billing_address.postcode = null;
                cartService.billing_address.prefix = null;
                cartService.billing_address.region = null;
                cartService.billing_address.region_id = null;
                cartService.billing_address.street = null;
                cartService.billing_address.suffix = null;
                cartService.billing_address.taxvat = null;
                cartService.billing_address.telephone = null;
                cartService.default_billing_address = null;
                cartService.billing_address.email = $scope.registrationInfo.email;
                cartService.billing_address.password = $scope.registrationInfo.password;
                cartService.billing_address.confirmPassword = $scope.registrationInfo.confirmPassword;
                cartService.billing_address.firstname = $scope.registrationInfo.firstname;
                cartService.billing_address.lastname = $scope.registrationInfo.lastname;

                

                if (!utils.validateEmail(cartService.billing_address.email)) setValidationError("Please enter a valid email address","registerError");
                else if (cartService.billing_address.password!=cartService.billing_address.confirmPassword) setValidationError("Passwords don't match","registerError");
                else if (!cartService.billing_address.password) setValidationError("Please insert a password","registerError");
                else {
                    $scope.registering = true;
                    cartService.saveBillingAddress(true)
                        .then(function(){
                            $scope.registering = false;
                            $scope.registrationInfo = {};
                        })
                        .then(function(){
                            return cartService.saveBillingAddress();
                        })
                        .catch(function(err){
                            $scope.registering = false;
                            setValidationError(err,"registerError");
                        })
                }

            }

            function selectChange(link){
                window.location.href = link;
            }
            function openLink(link){
                window.location.href = link;
            }

            function login(){
                $scope.loggingIn = true;
                security.login($scope.login.email,$scope.login.password)
                .then(function(){
                    return cartService.fetchState({bypassCache:true});
                })
                .then(function(){
                    cartService.populateCart();
                })
                .then(function(){
                    localStorage.setItem("email", $scope.login.email);
                    localStorage.setItem("password", $scope.login.password);
                    $scope.loggingIn = false;
                    $scope.login = {};
                })
                .catch(function(err){
                    $scope.loggingIn = false;
                    $scope.login.error = err;
                })
            }

            function setValidationError(err,scopeObj){
                if ((err) && (scopeObj)){
                    $scope[scopeObj] = err;
                    $timeout(function(){
                        $scope[scopeObj] = null;
                    },5000);
                }
            }

            function search(searchText){
                // redirect to products page with text filter
                if (searchText){
                    commonData.productsSearchText = searchText;
                    window.location.href = "/#!/products/s";
                }
            }

            function newsletterSubscribe(){
                if ($scope.newsletter.email) {
                    $scope.newsletter.loading = true;
                    $scope.newsletter.alertMessage = null;
                    NewsletterService.subscribe($scope.newsletter.email)
                    .then(function(response){
                        $scope.newsletter.loading = false;
                        $scope.newsletter.alertMessage = "you have been subscribed to the newsletter";
                    })
                    .catch(function(err){
                        $scope.newsletter.loading = false;
                        $scope.newsletter.alertMessage = err;
                    })
                }

            }

            function gotoAccount(){
                $scope.HeaderService.infoMenuVisible = false;
                window.location.href = "/#!/account";
            }

            function logout(){
                $scope.security.logout().then(function(){
                    cartService.fetchState({bypassCache : true});
                    $scope.login.email = localStorage.getItem("email");
                    $scope.login.password = localStorage.getItem("password");
                })     
            }

        }
})();