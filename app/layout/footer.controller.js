(function(){
    angular
        .module('app')
        .controller('Footer',Footer);

        Footer.$inject = ['$scope'];

        function Footer($scope){

        	$scope.footerColumnClasses = "col-md-2 col-xs-12 col-sm-fraction-1-5";
        	$scope.footerColumns = getFooterColumns();
        	$scope.selectChange = selectChange;

        	function selectChange(link){
        		if (link) window.location.href = link;
        	}

        	function getFooterColumns(){
        		return [
					{
						title: "by category",
						items:[
							{ name:'Jerseys & baselayers', href: '/#!/category/Jerseys & Baselayers'},
							{ name:'Bib shorts', href: '/#!/category/Bib%20Shorts'},
							{ name:'Jackets', href: '/#!/category/Jackets'},
							{ name:'Tops & T-shirts', href: '/#!/category/Tops%20&%20T-Shirts'},
							{ name:'Trousers & Shorts', href: '/#!/category/Trousers%20&%20Shorts'},
							{ name:'Accessories & Shoes ', href: '/#!/category/Accessories%20&%20Shoes'},
						]

					},	
					{	
						title: "customer service",
						items:[
							{	
								name: "Ordering & Payment",
								href:"/#!/faq#payment",
							},
							{		
								name: "Shipping",
								href:"/#!/faq#shipping",
							},
							{		
								name: "Returns",
								href:"/#!/faq#returns",
							},
							{		
								name: "Refund & Exchange",
								href:"/#!/faq",
							},
							{		
								name: "FAQ",
								href:"/#!/faq",
							}
						]
					},
					{	
						title: "PEdALED",
						items:[
							{
								name: "About us",
								href: "/#!/about"
							},
							{
								name: "#dressliveride",
								href: "/#!/instagram"
							}
						]
					},	
					{	
						title: "Social Channels",
						items:[
							{	
								name: "Facebook",
								href:"https://www.facebook.com/PEDALED",
								target: "_blank"
							},
							{
								name: "Instagram",
								href: "https://instagram.com/pedaledjapan/",
								target: "_blank"
							},
							{		
								name: "Twitter",
								href:"https://twitter.com/PEdALEDjapan",
								target: "_blank"
							},
							{		
								name: "Pinterest",
								href:"https://it.pinterest.com/pedaled/",
								target: "_blank"
							},
							{		
								name: "Tumblr",
								href:"http://pedaled-japan.tumblr.com/",
								target: "_blank"
							}
						]
					},	
        		]
        	}  //footer columns end


        }
})();


