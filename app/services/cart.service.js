(function(){
	angular
		.module("app")
		.factory("cartService",cartService);

		cartService.$inject = [
			"commonData",
			"$http",
			"config",
			"$q",
			"productModel",
			"security"
		];

		function cartService(commonData,$http,config,$q,productModel,security){
			var service = {
				items: [], //basket items
				itemsPopulated: false, //true if items have been populated with colors,size,parent models
				subTotal: 0,
				totals: {},
				billing_address: {},
				default_billing_address: {},
				default_shipping_address: {},
				shipping_address: {},
				shipping_address_validation: null,
				shipping_methods: [],
				payment_methods: [],
				payment_method: null,
				payment: {},
				creditCardTypes: getCreditCardTypes(),
				activeStore: {},
				activeStoreId: null,
				defaultCurrency: {},
				stores: [],

				fetchState: fetchState,
				setActiveStore: setActiveStore,
				setActiveStoreId: setActiveStoreId,
				addToCart: addToCart,
				fetchBasket: fetchBasket,
				removeItem: removeItem,
				saveBillingAddress: saveBillingAddress,
				saveShippingAddress: saveShippingAddress,
				saveShippingMethod: saveShippingMethod,
				savePaymentMethod: savePaymentMethod,
				sendOrder: sendOrder,
				fetchStores: fetchStores,
				populateCart: populateCart,
				paypalRedirect: paypalRedirect,
				authorizePaypalPayment: authorizePaypalPayment,
				getOrderHistory: getOrderHistory,
				editCustomerInfo: editCustomerInfo,
				getRegionsByCountry: getRegionsByCountry
			};

			initialize();

			return service;

			function fetchBasket(){
				return $http.get(config.API.basePath+"/basket?storeId="+service.activeStoreId,{withCredentials:true})
					.then(function(data){
						if ((!data) || (!data.data)){
							service.items = [];
							service.subTotal = 0;
						}
						else{
							service.items = data.data.items || [];
							service.subTotal = data.data.subTotal || 0;
						}
					});
				
			}

			function addToCart(productId,quantity){
				if ((!productId) || (!quantity)) return $q.reject("missing productId or quantity");

				return $http.get(config.API.basePath+"/basket/add/"+productId+"/"+quantity+"?storeId="+service.activeStoreId,{withCredentials:true})
					.then(fetchBasket)
			}

			function populateCart(){
				return populateProducts(service.items)
				.then(function(items){
					service.items = items;
					service.itemsPopulated = true;
					return buildState();
				})
			}

			function populateProducts(products){
				if (products){
					var promises = [];
					products.forEach(function(product){
						promises.push(populateProduct(product));
					});
					return $q.all(promises);
				}
			}

			function saveBillingAddress(register, isDefaultBilling){
				console.log("isDefaultBilling="+isDefaultBilling);

				if (typeof service.billing_address == "object"){
					//save return and cancel url callbacks. For paypal
					service.billing_address.returnUrl = "porcodio";
					console.warn("madonna!")
				}

				//register: boolean
				if (register) return $http.post(config.API.basePath+"/account/register",{customer: service.billing_address},{withCredentials:true}).then(cb);
				else {
					var params;
					if(service.billing_address=="default"){
						params = {address:service.default_billing_address,isDefaultBilling:isDefaultBilling,storeId:service.activeStoreId};
					}else {
						params = {address:service.billing_address,isDefaultBilling:isDefaultBilling,storeId:service.activeStoreId};
					}
					return $http.post(config.API.basePath+"/checkout/setBillingAddress",params,{withCredentials:true}).then(cb);
					
				}

				function cb(data){
					if (register){
						if (!data.data.success) return $q.reject(data.data.error);
						else{
							security.user = data.data.model;
							return security.user;
						}
					}
					else{
						if ((data) && (data.data)) {
							if ((data.data.billing_address_validation) && (data.data.billing_address_validation.length)) return $q.reject(data.data.billing_address_validation);
							else service.billing_address = data.data.billing_address;
							if (!data.data.billing_address) return $q.reject(data.data.error);
						}
						else service.billing_address = {};

						return service.billing_address;
					}
				}
			}

			function saveShippingAddress(isDefaultShipping){

				var params;
				if(service.shipping_address=="default"){
					params = {address:service.default_shipping_address,isDefaultShipping:isDefaultShipping,storeId:service.activeStoreId};
				} else {
					params = {address:service.shipping_address,isDefaultShipping:isDefaultShipping,storeId:service.activeStoreId};
				}
				return $http.post(config.API.basePath+"/checkout/setShippingAddress",params,{withCredentials:true})
					.then(function(data){
						if ((data) && (data.data)) return saveStateFromData(data.data);
						else return $q.reject("unknown error");
					})
					.then(function(){
						if (typeof service.shipping_address_validation=="string") return $q.reject(service.shipping_address_validation);
						else if ((service.shipping_address_validation) && (service.shipping_address_validation.length)) return $q.reject(service.shipping_address_validation);
						else return buildState();
					})
					.catch(function(err){
						console.warn("Error saving shipping address: "+err);
						return $q.reject(err);
					})
			}

			function populateProduct(product){
				//populate cart item simple product, with parentObj and productObj
				if (product){
					var promise;
					if (product.parentId) promise = $http.get(config.API.basePath+"/products/"+product.parentId+"?storeId="+service.activeStoreId,{withCredentials:true});
					else promise = $http.get(config.API.basePath+"/products/"+product.id+"?storeId="+service.activeStoreId,{withCredentials:true});

					return promise
					.then(function(productObj){
						if (productObj.data) productObj = productObj.data;
						else return $q.reject("Can't fetch product");

						if (!product.parentId) product.productObj = productObj;
						else{
							//try to populate productObj from configurable parent
							product.parentObj = productObj;
							productObj.models.forEach(function(model){
								if (model.id==product.id) product.productObj = model;
							})
						}

						//inject into the datastore
						productModel.inject(product.productObj);
						product.productObj = productModel.get(product.id);
						if (product.parentId){
							productModel.inject(product.parentObj);
							product.parentObj = productModel.get(product.parentId);
						}

						//load attributes
						var promises = [product.productObj.loadAttributes()];
						if (product.parentId) promises.push(product.parentObj.loadAttributes());

						return $q.all(promises);
					})
					.then(function(products){
						product.productObj = products[0];
						product.parentObj = products[1];
						return product;
					})
				}
			}

			function fetchState(options){
				options = options || {};
				if ((!options.bypassCache) && (service.stateLoaded)) return $q(function(resolve){resolve(buildState())});
				else return $http.get(config.API.basePath+"checkout/state?storeId="+service.activeStoreId,{withCredentials:true})
					.then(function(state){
						initialize();
						if ((state) && (state.data)) return saveStateFromData(state.data);
					})
					.then(function(){
						return buildState();
					})
					.catch(function(err){
						console.warn("Error fetching state: "+err);
						return $q.reject(err);
					})

			}

			function saveShippingMethod(){
				if ((!service.shipping_method) && (service.shipping_methods.length)) service.shipping_method = service.shipping_methods[0].id;

				if (!service.shipping_method) {
					console.warn("invalid id");
					return $q.reject("Can't set shipping method: invalid id");
				}
				else{
					return $http.post(config.API.basePath+"checkout/setShippingMethod",{code:service.shipping_method,storeId:service.activeStoreId},{withCredentials:true})
						.then(function(data){
							if (data.data) return saveStateFromData(data.data);
							else return $q.reject("unknown error");
						})
						.catch(function(err){
							console.warn("Can't set shipping method: "+err);
							return $q.reject(err);
						})
				}
			}

			function savePaymentMethod(){
				return $http.post(config.API.basePath+"checkout/setPaymentMethod",{payment:service.payment,storeId:service.activeStoreId},{withCredentials:true})
					.then(function(data){
						if ((data) && (data.data)) {
							if (!data.data.success) return $q.reject(data.data.message);
							return saveStateFromData(data.data);
						}
					})
					.then(function(){
						return buildState();
					})
					.catch(function(err){
						console.warn("Can't set payment method: "+err);
						return $q.reject(err);
					})
			}

			function sendOrder(){
				var email = (typeof service.billing_address=="object") ? service.billing_address.email : null;
				return $http.post(config.API.basePath+"checkout/send",{payment:service.payment,storeId:service.activeStoreId, email:email},{withCredentials:true})
					.then(function(status){
						if (!status.data.success) return $q.reject(status.data.message);
						else service.fetchState();
					})
					.catch(function(err){
						console.warn("Can't send order: "+err);
						return $q.reject(err);
					});
			}

			function saveStateFromData(data){
				service.stateLoaded = true;
				return $q(function(resolve,reject){
					if (data){
						service.billing_address = data.billing_address;
						service.default_billing_address = data.default_billing_address;
						service.default_shipping_address = data.default_shipping_address;
						service.shipping_address = data.shipping_address;
						service.payment_methods = data.payment_methods;
						service.payment_method = data.payment_method;
						service.shipping_methods = data.shipping_methods;
						service.shipping_method = data.shipping_method;
						if(data.cart){ 
							service.subTotal = data.cart.grandTotal;
							service.items = data.cart.items;
						}
						service.totals = data.totals;
						service.shipping_address_validation = data.shipping_address_validation;
						service.itemsPopulated = false;

						resolve(buildState());
					}
					else reject("data missing");					
				})
			}

			function buildState(){
				return {
					default_billing_address: service.default_billing_address,
					default_shipping_address: service.default_shipping_address,
					billing_address: service.billing_address,
					shipping_address: service.shipping_address,
					payment_methods: service.payment_methods,
					payment_method: service.payment_method,
					totals: service.totals,
					cart:{
						items: service.items,
						subTotal: service.subTotal
					}
				}
			}

			function getCreditCardTypes(){
				return [
					{
						id: "MC",
						name: "Mastercard"
					},
					{
						id: "VI",
						name: "Visa"
					},
					{
						id: "AE",
						name: "American Express"
					}
				];
			}

			function removeItem(id){
				if (id) return $http.get(config.API.basePath+"basket/remove/"+id+"?storeId="+service.activeStoreId,{withCredentials:true})
					.then(function(data){
						if ((data.data) && (data.data.models)){
							service.grandTotal = data.data.models.grandTotal;
							service.totals.grand_total.value = data.data.models.grandTotal;
							updateItemsQuantities(data.data.models.items);
							service.subTotal = data.data.models.subTotal;
							service.totals.subtotal.value = data.data.models.subTotal;
						}
					})
					.catch(function(err){
						console.warn("can't remove cart item: "+err);
						return $q.reject(err);
					})
			}

			function updateItemsQuantities(itemsNew){
				//update service items quantities from itemsNew, and remove not existent items
				//use this to avoid repopulating service.items.
				service.items = service.items.filter(function(item){
					var toDelete = true;
					itemsNew.forEach(function(itemNew){
						if (item.id==itemNew.id) {
							toDelete = false;
							item.quantity = itemNew.quantity;
						}
					})
					return !toDelete;
				})
			}

			function fetchStores(){
				return $http.get(config.API.basePath+"/stores",{withCredentials:true})
					.then(function(data){
						if ((data.data) && (data.data.length)) {
							service.stores = data.data;
							if ((!service.activeStore) || (!service.activeStore.id)) data.data.forEach(function(store){
								if ((!service.activeStoreId) && (store.default)) service.setActiveStore(store); //we don't have a preferred store, set the magento default as our default
								else if (service.activeStoreId==store.id) service.setActiveStore(store);	
							})
							return service.stores;
						}
						else return $q.reject("Can't fetch stores");
					})
					.catch(function(err){
						console.warn(err);
						return $q.reject(err);
					})							
			}

			function setActiveStore(store,reload){
				if (store){
					service.activeStore = store;
					service.activeStoreId = store.id;
					try{
						localStorage.setItem("store",store.id);
					}catch(error){
						console.log("Error trying to do localStorage.setItem: "+error);
					}

					if (reload) location.reload();

					//set default currency of the store
					service.defaultCurrency = {
						symbol: store.symbol,
						name: store.currency
					}
				}
			}

			function setActiveStoreId(storeId){
				if (storeId) {
					var store = null;
					service.stores.forEach(function(s){
						if (s.id==storeId) store = s
					});
					if (store) service.setActiveStore(store,true);
				}
			}

			function initialize(){
				service.items = [];
				service.subTotal = 0;
				service.billing_address = {};
				service.shipping_address = {};
				service.totals = {};

				service.payment = {method:"authnetcim"};
				// service.payment = {method:"ccsave"};

				//fetch active store from previous session and fetch stores from backend
				try{
					service.activeStoreId = localStorage.getItem('store');
				}catch(error){
					console.log("Error trying to do localStorage.setItem: "+error);
				}
				service.fetchStores();
			}

			function paypalRedirect(){
				$http.get(config.API.basePath+"paypal/start?storeId="+service.activeStoreId, {withCredentials:true})
				.then(function(data){
					if ((!data) || (!data.data) || (typeof data.data!="string")) return $q.reject("Can't redirect to paypal");
					else window.location.href = data.data;
				})
			}

			function authorizePaypalPayment(token,payerId){
				return $http.get(config.API.basePath+"paypal/authorize?storeId="+service.activeStoreId+"&token="+token+"&PayerID="+payerId, {withCredentials:true})
				.then(function(data){
					if ((data.data) && (data.data.success)) return;
					else{
						if (data.data) return $q.reject(data.data.message);
						else return $q.reject();
					}
				})
			}

			function getOrderHistory(){
				return $http.get(config.API.basePath+"account/orderHistory", {withCredentials:true})
				.then(function(data){
					data.data.forEach(function(order){
						order.created_at = new Date(order.created_at);
						if(!order.coupon_code) order.coupon_code = "-"
					})
					return data.data;
				})
			}

			function editCustomerInfo(customerEdited){
				return $http.post(config.API.basePath+"account/editCustomerInfo", {"customerEdited":customerEdited},{withCredentials:true})
				.then(function(data){
					if(data.status==200){
						return true;
					}else if(data.error || data.status !=200){
						return false;
					}
					return false;
				})
			}

			function getRegionsByCountry(country){
				debugger;
				return $http.get(config.API.basePath+"checkout/regionsByCountry/"+country, {withCredentials:true})
				.then(function(data){
					debugger;
					if(data.status==200){
						return data.data;
					}else if(data.error || data.status !=200){
						return null;
					}
					return null;
				})
			}
		}
})();