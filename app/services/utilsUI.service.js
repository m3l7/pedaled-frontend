(function(){
	angular
		.module("app")
		.factory("utilsUI",utilsUI);

		utilsUI.$inject = [];

		function utilsUI(){
			return {
				scrollWindowTo: scrollWindowTo
			};

			function scrollWindowTo(to, duration) {
				//animated scroll. duration is in ms
			    if (duration < 0) return;
			    var difference = to - window.pageYOffset;
			    var perTick = difference / duration * 10;

			    var interval = setInterval(function(){
			    	window.scrollTo(0,window.pageYOffset + perTick);
			    	if (window.pageYOffset>= to) clearInterval(interval);
			    },10)
			}
		}
})();