(function(){
	angular
		.module("app")
		.factory("alertService",alertService);

		alertService.$inject = [];

		function alertService(){
			return {
				lastMessage: {
					text: "",
					type: null
				}
			}
		}
})();