(function(){
	angular
		.module("app")
		.factory("NewsletterService",NewsletterService);

		NewsletterService.$inject = [
			"$q",
			"$http",
			"config",
			"cartService"
		];

		function NewsletterService($q, $http, config, cartService){
			var service = {
				subscribe: subscribe
			};

			return service;

			function subscribe(email){
				if (!email) return $q.reject("Missing email");
				else return $http.post(config.API.basePath+"/newsletter/subscribe",{email:email, storeId: cartService.activeStoreId})
					.then(function(data){
						if ((data) && (data.data)){
							if (data.data.success) return data.data;
							else return $q.reject(data.data.message);
						}
					})
			}
		}
})();