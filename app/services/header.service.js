(function(){
	angular
		.module("app")
		.factory("HeaderService",HeaderService);

		HeaderService.$inject = [];

		function HeaderService(){
			var service = {
				menuActive: {},
				menuVisible: false,
				infoMenuVisible: false,
				activeBlock: "login",
				searchMenuActive: false,
				toggleMenu: toggleMenu,
				toggleInfoMenu: toggleInfoMenu
			};

			return service;

			function toggleMenu(menu,hover){
				//if (!menu) -> close menu
			    if (menu){
			       var visible = ((hover) || (service.menuActive.name!=menu.name) || (!service.menuVisible));
			       service.menuActive = menu;
			       service.menuVisible = visible;
			    }
			    else {
			        service.menuVisible = false;
			        service.searchMenuActive = false;
			    }
			}

			function toggleInfoMenu(){
				//toggle info menu and close main menu
				service.infoMenuVisible = !service.infoMenuVisible;
				service.toggleMenu();
			}

		}
})();