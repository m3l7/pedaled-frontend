(function(){
	angular
		.module("app")
		.factory("utils",utils);

		utils.$inject = [
			"$location",
			"$rootScope"
		];

		function utils($location,$rootScope){
			var service = {
				indexBy: indexBy,
				normalizeUrl: normalizeUrl,
				isPageActive: isPageActive,
				has: has,
				validateEmail: validateEmail,
				setSEOMetaTitle: setSEOMetaTitle,
				setSEOMetaDescription: setSEOMetaDescription,
				setSEOMetaKeywords: setSEOMetaKeywords,
				setSEOMetaContentLanguage: setSEOMetaContentLanguage
			}

			return service;

			function indexBy(arr,key){
				var ret = {};
				arr.forEach(function(item){
					// if (item[key]) ret[item[key]] = item;
					if (has(item,key)) ret[item[key]] = item;
				})
				return ret;
			}
			function normalizeUrl(url){
				//convert href link to $location.path link
				if (url){
					if (url.indexOf("/#!/")!=-1) return url.split("/#!")[1];
					else if (url.indexOf("/#/")!=-1) return url.split("/#")[1];
					else return url;
				}
			}
			function isPageActive(path,fixed) {
			  path = normalizeUrl(path);

			  if (fixed==true) return path === $location.path();
			  else if ($location.path().substr(0, path.length) == path) return true;
			  else return false;
			};

			function has(object, key) {
				return object ? hasOwnProperty.call(object, key) : false;
			};

			function validateEmail(email) {
			    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			    return re.test(email);
			};


			function setSEOMetaTitle(seoMetaTitle){
				$rootScope.seoMeta.title = seoMetaTitle;
			}

			function setSEOMetaDescription(seoMetaDescription){
				$rootScope.seoMeta.description = seoMetaDescription;
			}
			function setSEOMetaKeywords(seoMetaKeywords){
				$rootScope.seoMeta.keywords = seoMetaKeywords;
			}
			function setSEOMetaContentLanguage(seoMetaContentLanguage){
				$rootScope.seoMeta.contentLanguage = seoMetaContentLanguage;
			}

		}
})();