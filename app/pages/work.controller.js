(function(){
	angular
		.module('app')
		.controller('Work',Work);

		Work.$inject = ['$scope',"commonData","utils"];

		function Work($scope,commonData,utils){
			utils.setSEOMetaTitle("");
			utils.setSEOMetaDescription("");
			utils.setSEOMetaKeywords("");
			utils.setSEOMetaContentLanguage("");

			commonData.header.breadcrumb = [];	
			commonData.header.filterBarVisible = false;
			commonData.header.shrinked = true;
		}
})();