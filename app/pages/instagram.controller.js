(function(){
	angular
		.module('app')
		.controller('Instagram',Instagram);

		Instagram.$inject = ['$scope',"commonData", "instagramModel","utils"];

		function Instagram($scope,commonData, instagramModel,utils){

			$scope.activePhoto = {};

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;
				commonData.header.breadcrumbVisible = false;

				instagramModel.findAll();
				instagramModel.bindAll({},$scope,"photos");
			}


			// $scope.photos = [
			// 	{
			// 		id: 1,
			// 		image: "/content/images_api/instagram/1.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 2,
			// 		image: "/content/images_api/instagram/2.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 3,
			// 		image: "/content/images_api/instagram/3.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 4,
			// 		image: "/content/images_api/instagram/4.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 5,
			// 		image: "/content/images_api/instagram/5.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 6,
			// 		image: "/content/images_api/instagram/6.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 7,
			// 		image: "/content/images_api/instagram/7.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 8,
			// 		image: "/content/images_api/instagram/1.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 9,
			// 		image: "/content/images_api/instagram/2.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 10,
			// 		image: "/content/images_api/instagram/3.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 11,
			// 		image: "/content/images_api/instagram/4.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 12,
			// 		image: "/content/images_api/instagram/5.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 13,
			// 		image: "/content/images_api/instagram/6.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 14,
			// 		image: "/content/images_api/instagram/7.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 15,
			// 		image: "/content/images_api/instagram/1.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// 	{
			// 		id: 16,
			// 		image: "/content/images_api/instagram/2.jpg",
			// 		description: "<p>#dressliveride is more than an Hashtag</p><p>This place shows how we and our friends interpretate this idea</p>"
			// 	},
			// ]
		}
})();