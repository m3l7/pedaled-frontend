(function(){
	angular
		.module('app')
		.controller('Contact',Contact);

		Contact.$inject = ['$scope',"commonData","utils"];

		function Contact($scope,commonData,utils){
			utils.setSEOMetaTitle("");
			utils.setSEOMetaDescription("");
			utils.setSEOMetaKeywords("");
			utils.setSEOMetaContentLanguage("");
			
			commonData.header.shrinked = true;
			commonData.header.filterBarVisible = false;
			commonData.header.breadcrumb = [
				{
					text: "Home",
					link:"/#!/"
				},
				{
					text: "contact",
					link: "/"
				}
			];
		}
})();