(function(){
	angular
		.module("app")
		.controller("About",About);

		About.$inject = [
			"$scope",
			"commonData",
			"utils"
		];

		function About($scope,commonData,utils){

			$scope.pages = getPages();

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.breadcrumb = [];
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;
			}


			function getPages(){
				return [
					{
						image: "/content/images_api/about/3.jpg",
						title: "“We make the garments. You decide how to Live and Ride with them.”"
					},
					{
						image: "/content/images_api/about/2.jpg",
						text: "",
						columns:[
							"",
							" ",
							"PEdAL ED began out of a love for cycling. <br> In 2007, Hideto Suzuki started creating beautiful and functional apparel to complement his cycling lifestyle in Tokyo. Since then, he has gone on to create a number of award-winning collections based on his belief that each feature of a garment can improve function and performance, not simply aesthetics. <br><br>At PEdAL ED, we design clothing for people who love cycling.<br>We want to make the most comfortable and long-lasting garments on the market. Garments you will love and recommend.",
							"Founded and handcrafted in Japan, PEdALED has grown internationally and recently also started a new collaboration in Italy, the heart of cycling apparel production.<br>Here Hideto’s creative talent has been able to develop new ideas, bringing to gether his unique understanding of materials and designs with the best of Italian manufacturing tradition."
						]
					},
					{
						image: "/content/images_api/about/1.jpg",
						text: "Every PEdAL ED product tells the story of our approach to Life and to Cycling. Made in Japan and Made in Italy stand for an attention to detail, and care for high quality products. The use of the finest manufacturing techniques guarantees that our garments are highly functional and durable.<br>We want cyclists to appreciate what we make as something inherently special and feel the difference in our garments. Like the people who have put their passion into designing and making them.",
						title: "“MADE IN JAPAN,<br>MADE IN ITALY”<br>日本製<br>イタリア製",
					},
					{
						image: "/content/images_api/about/2.jpg",
						text: "",
						title: "“GARMENTS DESIGNED FOR DEMANDING CYCLISTS”<br>サイクリストの<br>要求のために<br>設計された"
					},
				]
			}


		}
})();