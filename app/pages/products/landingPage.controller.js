(function(){
	angular
		.module('app')
		.controller('LandingPage',LandingPage);

		LandingPage.$inject = [
			'$scope',
			"$routeParams",
			'commonData',
			"productModel",
			"categoryModel",
		 	"$http",
			"config",
			"$timeout",
			"$location",
			"cartService",
			"$q",
			"shopId",
			"utils"
		];

		function LandingPage($scope,$routeParams, commonData, productModel,categoryModel,$http,config,$timeout,$location,cartService, $q, shopId,utils){

			$scope.openProduct = openProduct;
			$scope.filters = commonData.productsFilters = {};
			$scope.commonData = commonData;
			$scope.products = [];
			$scope.title = "";
			$scope.page = null;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.menuVisible = false;
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;
				commonData.header.breadcrumbVisible = false;

				var page = getLandingPage(shopId || $routeParams.shopId);
				$scope.title = page.name;
				$scope.page = page;

				if (page) productModel.findAll({storeId:cartService.activeStoreId})
				.then(function(products){

					var promises = [];

					page.items.forEach(function(sku){
						var skip = false;
						products.forEach(function(product){
							if (skip) return;
							if (sku.substring(0,9)==product.sku){
								skip = true;
								if (sku.length>9) product.forceGalleryColor = sku.substring(9);
								product.longSku = sku;
								promises.push(product.loadAttributes());
							}
						})

						if (!skip) console.log("missing sku: "+sku)
					})

					return $q.all(promises);
				})
				.then(function(products){
					$scope.products = products;
				})

			}

			function getLandingPage(page){
				var pages = {
					"1": {
						name: "New Arrivals",
						image: "/content/images_api/landing/header-newarrivals.jpg",
						items: ["EE16WJKWJ","EE16WJSKL","EE164JKVE","EE16SSPBB","EE16WSPBB","EE16WACLW","EE164JSUL","EE164ACWB"],
					},
					"2": {
						name: "Limited Edition",
						image: "/content/images_api/landing/header-newarrivals.jpg",
						items: ["EE144JKPL","EE154WTCR","EE144CATC","EE164JSRP"],
					},
					"3": {
						name: "Best Seller",
						image: "/content/images_api/landing/header-newarrivals.jpg",
						// items: ["EE144JSKI00","EE144JSKI02","EE154JSKI03","EE144JSOK00","EE144JSOK13","EE144JSWK02","EE144SPBB00","EE134JKGF03","EE154JKKA00","EE14WJSTA04","EE15WSOWS14"]
						items: ["EE144JSKI00","EE144JSKI02","EE154JSKI03","EE144JSOK00","EE144JSOK13","EE144JSWK02","EE144SPBB00","EE134JKGF03","EE154JKKA00","EE14WJSTA04"]
					}
				}
				if (page) return pages[page];
			}

			function openProduct(product){
				if (product){
					$location.path("/products/"+product[productModel.idAttribute]);
				}
			}


		}
})();