(function(){

	"use strict";

	angular
		.module('app')
		.controller('Product',Product);

		Product.$inject = [
			'$scope',
			'$routeParams',
			'commonData',
			"productModel",
			"colorModel",
			"sizeModel",
			"categoryModel",
			"config",
			"$http",
			"$location",
			"$anchorScroll",
			"$q",
			"cartService",
			"alertService",
			"utils"
		];

		function Product($scope,$routeParams, commonData,productModel,colorModel,sizeModel,categoryModel, config, $http,$location,$anchorScroll,$q,cartService, alertService,utils){

			$scope.product = {};
			$scope.colors = commonData.colors;
			$scope.currentModel = {};
			$scope.productOptions = {
				quantity: 1,
				otherItemsMenu: 0
			};
			$scope.carouselItemsNumber = 6;
			$scope.activeImage = {};
			$scope.addingToCart = false;
			$scope.commonData = commonData;
			$scope.cartService = cartService;
			$scope.sizechartActive = false;
			$scope.sizechartItems = [];
			$scope.currentRelatedField = "similar"; // active related prducts tab (similar/upsell/crosssell)
			$scope.loading = false;
			
			$scope.openProduct = openProduct;
			$scope.addToCart = addToCart;
			$scope.changeSize = changeSize;
			$scope.changeColor = changeColor;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				$scope.loading = true;

				$anchorScroll();

				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;

				colorModel.findAll();
				colorModel.bindAll({},$scope,"colors");
				sizeModel.findAll();
				sizeModel.bindAll({},$scope,"sizes");

				var action;
				if ($routeParams.id) action = productModel.findById($routeParams.id,cartService.activeStoreId);
				else if ($routeParams.name) action = productModel.findByName($routeParams.name,cartService.activeStoreId);
				else if ($routeParams.urlkey) action = productModel.findByUrlKey($routeParams.urlkey,cartService.activeStoreId);
				else{
					console.err("Invalid product id/name/urlkey");
					return;
				}

				action
				.then(function(product){
					if (!product) return $q.reject("Can't fetch product");
					else{
						if (!product.models.length) return $q.reject("No models in this product");
						else return product.loadAttributes();
					}
				})
				.then(fillModelsGalleries) //if there are too few images in a model, fill them from other models
				.then(function(product){

					//set model on scope
					$scope.product = product;
					product.computeAttributesCombinations();
					setCurrentModelByColor($routeParams.colorId); //try to set a color if colorId is set, else set the first model
					product.indexAttributes();
					$scope.loading = false;

					return product;
				})
				.then(function(product){
					return product.loadRelatedProducts(cartService.activeStoreId);
				})
				.then(function(){
					//load parent category info
					return categoryModel.findAll()
						.then(function(){
							return $scope.product.loadCategories();
						})
				})
				.then(function(){
					buildBreadcrumb();
				})
				.catch(function(err){
					$scope.loading = false;
					console.error(err);
				})

			}

			function fillModelsGalleries(product){
				//fill galleries of products if there are too few images
				//we have to fill all models... fill model i with images from model j
				// return product;

				product.models.forEach(function(model,i){
					var firstSizeModelIndex = getFirstSizeModel(product.models,model);
					var firstSizeModel = product.models[firstSizeModelIndex];
					model.gallery = angular.copy(firstSizeModel.gallery);
					if (!model.gallery) model.gallery = [];

					for (var imageIndex=0;imageIndex<=4;imageIndex++){
						//try to get second,third,fourth images from other models (skip the first, because they're all the same)
						if (model.gallery.length<6) product.models.forEach(function(modelFill,j){
							if ((model.gallery.length>=6) || (firstSizeModelIndex==j)) return;
							else if (!modelFill.gallery[imageIndex]) return;
							else if (model.gallery.indexOf(modelFill.gallery[imageIndex])==-1) {
								model.gallery.push(modelFill.gallery[imageIndex]);
							}
						})
					}
				})

				return product;
			}

			function getFirstSizeModel(models,model){
				//input: a product model. Search the first size of the same color.

				var firstModelIndex = null;
				models.forEach(function(item,i){
					if (typeof firstModelIndex=="number") return;
					if (item.color.id==model.color.id) firstModelIndex = i;
				})
				return firstModelIndex;
			}

			function addToCart(){
				$scope.addingToCart = true;
				cartService.addToCart($scope.currentModel.id,$scope.productOptions.quantity)
				.then(function(){
					$scope.addingToCart = false;
					alertService.lastMessage = {
						name: $scope.product.name,
						image: ($scope.product.gallery) ? $scope.product.gallery[0] : null,
						price: $scope.product.price*$scope.productOptions.quantity,
						subTotal: cartService.subTotal
					};
				})
				.catch(function(err){
					//TODO: manage error in a message box?
					$scope.addingToCart = false;
				})
			}

			function buildBreadcrumb(){
				commonData.header.breadcrumbVisible = true;
				commonData.header.breadcrumb = [
					{
						text: "Home",
						link:"/#!/"
					},
					{
						text: "Shop",
						link: "/#!/products/"
					}
				];
				if ($scope.product.categories.length) commonData.header.breadcrumb.push(
					{
						text: $scope.product.categories[0].name,
						link: "/#!/category/"+$scope.product.categories[0].name
					}
				);
				commonData.header.breadcrumb.push(
					{
						text: $scope.product.name,
						link: ""
					}
				);

			}

			function openProduct(product){
				if (product){
					$location.path("/products/"+product[productModel.idAttribute]);
				}
			}

			function changeColor(color){
				if (color){
					var success = false;
					$scope.product.models.forEach(function(model){
						if (success) return;
						if ((model.color.id==color.id) && (model.size.id==$scope.currentModel.size.id)){
							success = true;
							setCurrentModel(model);
						}
					})
					if (!success) $scope.product.models.forEach(function(model){
						if (success) return;
						if (model.color.id==color.id){
							setCurrentModel(model);
							success = true;
						}
					})
				}
			}
			function changeSize(size){
				if (size){
					var success = false;
					$scope.product.models.forEach(function(model){
						if (success) return;
						if ((model.size.id==size.id) && (model.color.id==$scope.currentModel.color.id)){
							success = true;
							setCurrentModel(model);
						}
					})
					if (!success) $scope.product.models.forEach(function(model){
						if (success) return;
						if (model.size.id==size.id){
							setCurrentModel(model);
							success = true;
						}
					})
				}
			}

			function setCurrentModel(model){
				if (model){
					$scope.currentModel = model;
					$scope.productOptions.quantity = (model.quantity) ? 1 : 0;
					$scope.activeImage.url = (model.gallery.length) ? model.gallery[0] : "";
				}
				else{
					//set a placeholder model if model is not present
					$scope.currentModel = {
						name: "-",
						description: "-",
					}
				}
			}

			function setCurrentModelByColor(colorId){
				//colorId can be an Id or a color Name
				if (!colorId) setCurrentModel($scope.product.models[0]);
				else{
					var currentModel;

					//colorId is a name
					if ((parseInt(colorId)!=0) && (!parseInt(colorId))){
						$scope.product.models.forEach(function(model){
							if (currentModel) return;
							if (model.color.admin_label.toLowerCase()==colorId.toLowerCase()) currentModel = model;
						})
					}

					//colorId is an id
					else $scope.product.models.forEach(function(model){
						if (currentModel) return;
						if (model.color.id==colorId) currentModel = model;
					})

					if (!currentModel) console.warn("Can't find a suitable color");
					else setCurrentModel(currentModel);
				}
			}

		}
})();