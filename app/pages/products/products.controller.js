(function(){
	angular
		.module('app')
		.controller('Products',Products);

		Products.$inject = [
			'$scope',
			'commonData',
			"productModel",
			"categoryModel",
		 	"$http",
			"config",
			"$timeout",
			"$location",
			"cartService",
			"$q",
			"searchMode",
			"utils"
		];

		function Products($scope,commonData, productModel,categoryModel,$http,config,$timeout,$location,cartService, $q, searchMode,utils){

			$scope.filters = commonData.productsFilters = {};
			$scope.commonData = commonData;
			$scope.categories = [];
			$scope.products = [];

			$scope.openProduct = openProduct;
			$scope.resetFilters = resetFilters;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.menuVisible = false;
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = true;
				commonData.header.breadcrumbVisible = true;
				commonData.header.breadcrumb = [
					{
						text: "Home",
						link:"/#!/"
					},
					{
						text: "Shop",
						link: "/"
					}
				];	

				//clear filters if not in search mode
				commonData.productsFilters = {};
				if (!searchMode) commonData.productsSearchText = null;

				categoryModel.bindAll({},$scope,"categories");

				productModel.findAll({storeId:cartService.activeStoreId})
				.then(function(products){
					return $q.all(products.map(function(product){
						return product.loadAttributes();
					}))
				})
				.then(function(products){
					$scope.products = products;
					return categoryModel.findAll();
				})
				.then(function(categories){
					categories.forEach(function(category){
						category.loadProducts($scope.products);
					})

				})

			}

			function openProduct(product){
				if (product){
					$location.path("/products/"+product[productModel.idAttribute]);
				}
			}

			function resetFilters(){
				commonData.productsFilters = {};
				commonData.productsSearchText = null;
			}

		}
})();