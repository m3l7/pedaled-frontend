(function(){
	angular
		.module('app')
		.controller('Category',Category);

		Category.$inject = [
			'$scope',
			'commonData',
			"productModel",
			"categoryModel",
		 	"$http",
			"config",
			"$location",
			"$routeParams",
			"$q",
			"cartService",
			"utils"
		];

		function Category($scope,commonData, productModel,categoryModel,$http,config,$location,$routeParams,$q,cartService,utils){

			$scope.colors = [
				{value: '#656725'},
				{value: '#186790'},
				{value: '#95481b'},
				{value: '#656725'},
				{value: '#186790'},
				{value: '#95481b'},
			];
			$scope.openProduct = openProduct;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = true;
				commonData.header.breadcrumbVisible = true;
				commonData.header.breadcrumb = [
					{
						text: "Home",
						link:"/#!/"
					},
					{
						text: "Shop",
						link: "/#!/products/"
					},
					{
						text: $routeParams.catName,
						link: ""
					}
				];	

				categoryModel.findAll()
				.then(function(category){
					$scope.category = categoryModel.filter({name:$routeParams.catName});
					if ($scope.category.length){
						$scope.category = $scope.category[0];
						return productModel.findAll({categories:$scope.category.id,storeId:cartService.activeStoreId});
					}
				})
				.then(function(products){
					return $q.all(products.map(function(product){
						return product.loadAttributes();
					}))
				})		
				.then(function(products){
					$scope.category.loadProducts(products);
				});

			}

			function openProduct(product){
				if (product){
					$location.path("/products/"+product[productModel.idAttribute]);
				}
			}
		}
})();