(function(){
	angular
		.module('app')
		.controller('Account',Account);

		Account.$inject = ['$scope',"commonData", "cartService", "utils", "security","$timeout","NewsletterService","$q"];

		function Account($scope,commonData,cartService,utils,security,$timeout,NewsletterService,$q){
			
			initialize();
			watchSecurity();

			function initialize() {
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");

				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;
				commonData.header.breadcrumb = [];
				$scope.orderHistory = {};
				$scope.customerInfo = {};
				$scope.messageValidation = "";
				$scope.messageShippingValidation = "";
				$scope.messageBillingValidation = "";
				$scope.wantToSubscribe = false;
				$scope.defaultBillingAddress = null;
				$scope.defaultShippingAddress = null;
				$scope.changingBillingAddress = false;
				$scope.changingShippingAddress = false;
				$scope.countryCodes = commonData.countryCodes;
				$scope.shippingSubmit = shippingSubmit;
				$scope.billingSubmit = billingSubmit;
				$scope.loadingUpdateAccount = false;
				$scope.loadingUpdateBilling = false;
				$scope.loadingUpdateShipping = false;
				$scope.allOperationDone = false;
				$scope.logout = logout;
				$scope.security = security;
				$scope.labelFiscalCodeBilling = "Tax/VAT Number";
				$scope.labelFiscalCodeShipping = "Tax/VAT Number";
				$scope.updateLabelFiscalCode = updateLabelFiscalCode;

				cartService.getOrderHistory().then(function(orderHistory){
					$scope.orderHistory = orderHistory;
					getCurrencySymbol();
				})
				.catch(function(error){
					console.warn(error);
				});
			

				security.fetchAccount({bypassCache : true}).then(function(customerInfo){
					if(customerInfo){
						if(customerInfo.dob){
							customerInfo.dob = new Date(customerInfo.dob);
							customerInfo.day = customerInfo.dob.getDate();
							customerInfo.month = customerInfo.dob.getMonth()+1;
							customerInfo.year = customerInfo.dob.getFullYear();
						}
						$scope.customerInfo = customerInfo;
					}else{
						// if user is not logged in
						window.location.href="/#!/home/";
					}
					$scope.allOperationDone = true;
				})
				.catch(function(error){
					console.warn(error);
					$scope.allOperationDone = true;
				});

				cartService.fetchState({bypassCache : true}).then(function(state){
					if(!state){
						$scope.defaultBillingAddress = {};
						$scope.defaultShippingAddress = {};
					}else{
						$scope.defaultBillingAddress = state.default_billing_address;
						$scope.defaultShippingAddress = state.default_shipping_address;
						if($scope.defaultShippingAddress.country_id == 'IT') $scope.labelFiscalCodeShipping = "Fiscal code";
						else $scope.labelFiscalCodeShipping = "Tax/VAT Number";
						debugger;
						if($scope.defaultBillingAddress.country_id == 'IT') $scope.labelFiscalCodeBilling = "Fiscal code";
						else $scope.labelFiscalCodeBilling = "Tax/VAT Number";
					}
				})
				.catch(function(error){
					console.warn(error);
				})

			}

			function watchSecurity() {
				$scope.$watch("security.isAuthenticated()", function(newV,oldV){
				if(newV != oldV){
					initialize();
				}
				},true)
			}
			
			$scope.updateCustomer = function (){
				$scope.loadingUpdateAccount = true;
				$scope.customerInfo.Dob = "";
				delete $scope.customerInfo.dob;

				if(!$scope.customerInfo.email){
					setValidationMessage("messageValidation","Error! Email field is required");
					return;
				} else if(!$scope.customerInfo.firstname){
					setValidationMessage("messageValidation","Error! First name field is required");
					return;
				} else if(!$scope.customerInfo.lastname){
					setValidationMessage("messageValidation","Error! Last name field is required");
					return;
				} else

				$scope.customerInfo.Dob = new Date($scope.customerInfo.year+"-"+$scope.customerInfo.month+"-"+$scope.customerInfo.day);

				if(!$scope.customerInfo.Dob.getDate()){
					if($scope.customerInfo.year || $scope.customerInfo.month || $scope.customerInfo.day){
						setValidationMessage("messageValidation","Error! Wrong date");
						$scope.customerInfo.year = "";
						$scope.customerInfo.month = "";
						$scope.customerInfo.day = "";
						return;
					}
				}

				var arrayOperations = [ cartService.editCustomerInfo($scope.customerInfo) ];
				if($scope.wantToSubscribe) arrayOperations.push(NewsletterService.subscribe($scope.customerInfo.email));

				$q.all(
					arrayOperations
				)
				.then(function(results) {
					$scope.loadingUpdateAccount = false;
					if(results.size==2){
						if(results[0] && results[1].success){
							setValidationMessage("messageValidation","Account information updated correctly!");
						}else if(!results[0]){
							setValidationMessage("messageValidation","Error! Wrong password");
						} else if(!results[1].success){
							setValidationMessage("messageValidation","Error subscribing to newsletter");
						}
					}else {
						if(results[0]){
							setValidationMessage("messageValidation","Account information updated correctly!");
						}else {
							setValidationMessage("messageValidation","Error! Wrong password");
						}
					}

					
					delete $scope.customerInfo.oldPassword;
					delete $scope.customerInfo.newPassword;
					delete $scope.customerInfo.newPassword2;
				})
				.catch(function(error) {
                    setValidationMessage("messageValidation",error);
				});
			}

			function setValidationMessage(StringVarValidation,stringMessage){
                if (stringMessage){
                    $scope[StringVarValidation] = stringMessage;
                    $timeout(function(){
                        $scope[StringVarValidation] = null;
                    },5000);
                }
            }


			function getCurrencySymbol() {
				cartService.fetchStores().then(function(stores){
					var storesIndex = utils.indexBy(stores,"currency");
					if($scope.orderHistory.length){
						$scope.orderHistory.forEach(function(singleOrderH){
							singleOrderH.symbol = storesIndex[singleOrderH.order_currency_code].symbol;
						})
					}
				})
				.catch(function(error){
					console.warn(error);
				});
			}
			


			function shippingSubmit() {
				$scope.loadingUpdateShipping = true;
				cartService.default_shipping_address = $scope.defaultShippingAddress;
				cartService.shipping_address = $scope.defaultShippingAddress;
				cartService.saveShippingAddress(true).then(function(result){
					if(result) {
						$scope.loadingUpdateShipping = false;
						setValidationMessage("messageShippingValidation","Shipping address updated correctly");
						cartService.default_shipping_address = angular.copy(cartService.shipping_address);
						cartService.shipping_address = "default";
					}
				})
				.catch(function(error){
					$scope.loadingUpdateShipping = false;
					if(result) setValidationMessage("messageShippingValidation","Error!");
					console.error(error);
				});
			}

			function billingSubmit() {
				$scope.loadingUpdateBilling = true;
				cartService.default_billing_address = $scope.defaultBillingAddress;
				cartService.billing_address = $scope.defaultBillingAddress;
				cartService.saveBillingAddress(false, true).then(function(result){
					if(result){ 
						$scope.loadingUpdateBilling = false;
						setValidationMessage("messageBillingValidation","Billing address updated correctly");
						cartService.default_billing_address = angular.copy(cartService.billing_address);
						cartService.billing_address = "default";
					}

				})
				.catch(function(error){
					$scope.loadingUpdateBilling = false;
					if(error) setValidationMessage("messageBillingValidation","Error!");
					console.error(error);
				});
			}


			function logout(){
				security.logout()
				.then(function(result){
					if(result) window.location.href="/#!/home/";
				})
			}

			function updateLabelFiscalCode(country_id){
				if($scope.defaultShippingAddress.country_id == 'IT') $scope.labelFiscalCodeShipping = "Fiscal code";
				else $scope.labelFiscalCodeShipping = "Tax/VAT Number";
				if($scope.defaultBillingAddress.country_id == 'IT') $scope.labelFiscalCodeBilling = "Fiscal code";
				else $scope.labelFiscalCodeBilling = "Tax/VAT Number";
			}
			
		}

})();