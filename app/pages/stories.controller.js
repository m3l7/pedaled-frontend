(function(){
	angular
		.module('app')
		.controller('Stories',Stories);

		Stories.$inject = ['$scope','commonData','utils'];

		function Stories($scope,commonDatam,utils){
			commonData.header.shrinked = true;
			$scope.stories = commonData.stories;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.menuVisible = false;
				commonData.header.filterBarVisible = false;
				commonData.header.breadcrumb = [
					{
						text: "Home",
						link:"/#!/"
					},
					{
						text: "Stories",
						link: "/"
					}
				];	
			}



		}
})();