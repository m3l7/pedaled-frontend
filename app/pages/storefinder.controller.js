(function(){
	angular
		.module('app')
		.controller('Storefinder',Storefinder);

		Storefinder.$inject = ['$scope',"commonData","utils"];

		function Storefinder($scope,commonData,utils){

			utils.setSEOMetaTitle("");
			utils.setSEOMetaDescription("");
			utils.setSEOMetaKeywords("");
			utils.setSEOMetaContentLanguage("");

			commonData.header.shrinked = true;
			commonData.breadcrumb = [
				{
					text: "Home",
					link:"/#!/"
				},
				{
					text: "Store finder",
					link: "/"
				}
			];
			$scope.shops = [
				{
					name: 'Bellerose',
					address: "Rijshout 3 (zone maalbek)",
					city: "1702 Groot-Bijgaarden",
					id:1,
					latitude: 40.1451,
					longitude: -99.6680
				},
				{
					name: 'CoffeeBar & Store O-Ne',
					address: "Broekstraat 1",
					city: "3080 Tervuren",
					id:2,
					latitude: 41.1451,
					longitude: -99.6680
				},
				{
					name: 'Bellerose',	
					address: "Rijshout 3 (zone maalbek)",
					city: "1702 Groot-Bijgaarden",
					id:3,
					latitude: 42.1451,
					longitude: -99.6680
				},
				{
					name: 'Bellerose',
					address: "Rijshout 3 (zone maalbek)",
					city: "1702 Groot-Bijgaarden",
					id:4,
					latitude: 41.1451,
					longitude: -98.6680
				},
				{
					name: 'Bellerose',
					address: "Rijshout 3 (zone maalbek)",
					city: "1702 Groot-Bijgaarden",
					id:5,
					latitude: 41.1451,
					longitude: -97.6680
				},
			];
			$scope.shopsFilter = $scope.shops;
			$scope.map = { center: { latitude: 44.1, longitude: -98 }, zoom: 5 };

			$scope.$watch('filter',function(newv,oldv){
				if (newv!=oldv){
					var filtered = [];
					$scope.shops.forEach(function(shop){
						if (!newv) filtered.push(shop);
						else{
							if (shop.name.toUpperCase().indexOf(newv.toUpperCase())!=-1) filtered.push(shop);
							else if (shop.address.toUpperCase().indexOf(newv.toUpperCase())!=-1) filtered.push(shop);
							else if (shop.city.toUpperCase().indexOf(newv.toUpperCase())!=-1) filtered.push(shop);
						}
					});
					$scope.shopsFilter = filtered;
				}
			})
		}
})();