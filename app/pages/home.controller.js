(function(){
    angular
        .module('app')
        .controller('Home',Home);

        Home.$inject = ["$scope","commonData", "NewsletterService","utils"];

        function Home($scope,commonData, NewsletterService,utils){

            commonData.header.breadcrumb = [];
            commonData.header.filterBarVisible = false;
            commonData.header.shrinked = false;
            $scope.viewPort = commonData.viewPort;
            $scope.slides = getSlides();
            $scope.newsletter = {
                email: null,
                alertMessage: null,
                loading: false,
                label: null,
                mobileLabel: null
            };

            //methods
            $scope.newsletterSubscribe = newsletterSubscribe;

            initialize();

        	$scope.homeContent = [
        		{
        			position: 0,
                    hasImages: true,
                    height:{
                        MD: "350px",
                        LG: "350px",
                        XS: "350px"
                    },
                    items:[
                        {
                            sizeMD: 6,
                            sizeXS: 12,
                            background: '/content/images_api/homeContent/new/new_arrivals.jpg',
                            content:'<p>NEW ARRIVALS</p><h3><strong>check out what\'s new</strong></h3>',
                            link: '#!/newarrivals'
                        },
                        {
                            sizeMD: 6,
                            sizeXS: 12,
                            background: '/content/images_api/homeContent/new/bestseller.jpg',
                            content:'<p>BEST SELLER</p><h3><strong>unique cycling collection</strong></h3>',
                            link: '#!/bestseller',
                        },
                    ]

                },
                {
                    position: 1,
                    items:[
                        {
                            sizeMD: 12,
                            sizeXS: 12,
                            type: "text",
                            content:'<p><strong>design for cycling</strong></p><p>through innovative design, pioneering materials and sartorial experience we create unique cycling products for your everyday life.',
                        },
                    ]

                },
                {
                    position: 2,
                    height:{
                        MD: "350px",
                        LG: "350px",
                        XS: "200px"
                    },
					hasImages: true,
        			items:[
        				{
        					sizeMD: 3,
        					sizeXS: 6,
        					background: '/content/images_api/homeContent/new/ambassador.jpg',
        					content:'<p>TRANSCONTINENTAL RACE</p><h3><strong>an unsupported competition</strong></h3>',
        					link: '#!/stories/Transcontinental-Race-2015',
        				},
        				{
        					sizeMD: 3,
        					sizeXS: 6,
        					background: '/content/images_api/homeContent/new/dresstoliveride.jpg',
        					content:'<p>#DRESSLIVERIDE</p><h3><strong>dress to live and ride</strong></h3>',
        					link: '#!/instagram'
        				},
        				{
        					sizeMD: 3,
        					sizeXS: 6,
        					background: '/content/images_api/homeContent/new/stories.jpg',
        					content:'<p>STORIES</p><h3><strong>and adventures</strong></h3>',
        					link: '/#!/stories',
        				},
        				{
        					sizeMD: 3,
        					sizeXS: 6,
        					background: '/content/images_api/homeContent/new/madeinitaly.jpg',
        					content:'<p>DESIGNED IN JAPAN</p><h3><strong>made in italy</strong></h3>',
        					link: '#!/stories/From-Japan-To-The-World'
        				},
        			]

        		}
        	];

            function getSlides(){
                var slides = [
                    {
                        video: '/content/images_api/homeSlides/PEdAL ED Great Ocean Road-HD_Audio.mp4',
                        link: '/#!/stories/Australia-Great-Ocean-Road/',
                        caption:{
                            backgrund: "transparent",
                            color: "white",
                            title: 'THE GREAT OCEAN ROAD',
                            text: 'Discover how we rode through Australia natural wonders, exploring mountains and coast lines...',
                            top: '190px',
                            horizontalAlign: true,
                            width: '990px'
                        }
                    },
                    {
                        image: '/content/images_api/homeSlides/tcr.jpg',
                        link: '/#!/stories/Transcontinental-Race-2015',
                        caption:{
                            backgrund: "transparent",
                            color: "white",
                            title: '15 DAYS - 13 HOURS - 14 MINUTES',
                            text: 'The Transcontinental race 2015 is now finished. A special thanks to our brave guys for testing our gear to the extreme and giving us the joy to follow them along...',
                            top: '340px',
                            horizontalAlign: true,
                            width: '990px',
                            readMore: true
                        }
                    },
                    {
                        image: '/content/images_api/homeSlides/newcollection.jpg',
                        link: '/#!/newarrivals',
                        caption:{
                            backgrund: "transparent",
                            color: "white",
                            title: 'NEW ARRIVALS',
                            text: 'Our latest designs, colors and fabrics are now ready to get you on the road',
                            top: '190px',
                            horizontalAlign: true,
                            width: '990px'
                        }
                    }
                ];

                return slides.filter(function(slide){
                    if ((slide.video) && (isMobile.any)) return false;
                    else return true;
                })
            }

            function initialize(){
                utils.setSEOMetaTitle("");
                utils.setSEOMetaDescription("");
                utils.setSEOMetaKeywords("");
                utils.setSEOMetaContentLanguage("");

                $scope.newsletter.label = "";
                // $scope.newsletter.label = "Keep in touch with PEdAL ED";
            }

            function newsletterSubscribe(){
                if ($scope.newsletter.email) {
                    $scope.newsletter.loading = true;
                    $scope.newsletter.label = null;
                    $scope.newsletter.mobileLabel = null;
                    NewsletterService.subscribe($scope.newsletter.email)
                    .then(function(response){
                        $scope.newsletter.loading = false;
                        $scope.newsletter.label = "You have been subscribed to the newsletter";
                        $scope.newsletter.mobileLabel = "You have been subscribed to the newsletter";
                    })
                    .catch(function(err){
                        $scope.newsletter.loading = false;
                        $scope.newsletter.label = err;
                        $scope.newsletter.mobileLabel = err;
                    })
                }

            }
            
        }
})();