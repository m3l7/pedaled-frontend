(function(){
	angular
		.module('app')
		.controller('Story',Story);

		Story.$inject = ['$scope','commonData','$routeParams','utils',"$anchorScroll","$sce", "config"];

		function Story($scope,commonData,$routeParams,utils,$anchorScroll, $sce, config){

			$scope.openShareLink = openShareLink;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				$anchorScroll();
				var id = $routeParams.id;
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;

				var stories = commonData.stories;
				var storiesObj = utils.indexBy(stories,'id');
				var story = storiesObj[id];

				story.text = "<gallery items='story.gallery'>"+story.text+"</gallery>";
				story.shareUrl = window.encodeURIComponent(config.baseUrl+"#!/stories/"+story.id);
				story.shareImage = window.encodeURIComponent(config.baseUrl+story.image);
				story.shareDescription = window.encodeURIComponent(story.description);
				story.shareTitle = window.encodeURIComponent(story.title);

				$scope.$root.ogMeta = {};
				$scope.$root.ogMeta.title = story.title;
				$scope.$root.ogMeta.description = story.description;
				$scope.$root.ogMeta.image = config.baseUrl+story.image;
				$scope.$root.ogMeta.url = config.baseUrl+"#!/stories/"+story.id;

				$scope.story = story;
				$scope.story.textSafe = $sce.trustAsHtml($scope.story.text);


				commonData.header.breadcrumbVisible = true;	
				commonData.header.breadcrumb = [
					{
						text: "Home",
						link:"/#!/"
					},
					{
						text: "Stories",
						link:"/#!/stories"
					},
					{
						text: $scope.story.title,
						link:""
					}
				]
			}

			function openShareLink(link){
				if (link) {
					window.open(link, 'titolo', 'width=500, height=400, resizable, status, scrollbars=1, location');
				}
			}
		}
})();