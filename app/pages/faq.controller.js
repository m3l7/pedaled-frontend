(function(){
	angular
		.module('app')
		.controller('Faq',Faq);

		Faq.$inject = ['$scope',"commonData","$anchorScroll","utils"];

		function Faq($scope,commonData,$anchorScroll,utils){

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.breadcrumb = [];
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;

				$anchorScroll.yOffset = 50;
				$anchorScroll();
			}

			$scope.faqs = [
				{
					title: "Shipping",
					class: "shipping",
					texts:[
						"All our orders (except for promotions) are *shipped to all Europe Countries (Switzerland included) for free.",
						"All product orders placed before 0:00 p.m. GMT+1 on business days, Monday thru Friday (excluding federal holidays) will ship in 7 working days.<br>Most of the time we are able to ship in less days but we take a safe amount of time.",
						"Should you wish to check the status of your shipment, feel free to write us here and we will send you the tracking number once shipping has been arranged.",
					]
				},
				{
					title: "Payment & Security",
					class: "payment",
					texts:[
						"Your credit card will be charged in Euro, as the shipping occours from Brooks England srl in Italy.",
						"Payments of online orders will be processed through Paypal and Authorize.Net, a secure online payment gateway. We suggest that you print out a copy of your online order, as it contains useful information you may need in case of communication with BROOKS ENGLAND srl.",
						"Your credit card will be charged only once your purchase has been shipped."
					]
				},
				{
					title: "Returns & Exchange Policy",
					class: "returns",
					texts:[
						"At PEdAL ED, we create products that help you live and ride better.<br>As we want you to be as passionate about your product purchased at our e-store, if one of our garment fails to satisfy you -- for any reason -- please return your order within 15 days of the shipping date for a full refund.<br>The only restrictions are the following:",
						"- the product has not been used, manipulated or damaged<br>-the product must be sent back with its original packaging",
						"We will refund you the cost of the product, excluding all shipping costs and duty. Before making a return, please provide written notification explaining the reason for your return by sending an email to sales@pedaled.com",
						"Your return will be processed within 10-15 business days of receipt at our warehouse.<br>Your refund will be applied to your original method of payment and will show on your next statement, depending on the issuing bank and/or billing cycle.",
						"PEdALED garments are made from natural materials, therefore color and appearance may vary slightly. We do not accept these slight natural variations as return claims.",
					]
				},
				{
					title: "Payment & Security",
					class: "payment2",
					image: "/content/images/faq/payments.jpg",
					texts:[
						"Your credit card will be charged in Euro, as the shipping occours from Brooks England srl in Italy.",
						"Payments of online orders will be processed through Paypal and Authorize.Net, a secure online payment gateway. We suggest that you print out a copy of your online order, as it contains useful information you may need in case of communication with BROOKS ENGLAND srl.",
						"Your credit card will be charged only once your purchase has been shipped."
					]
				},
				{
					title: "Sale Items",
					class: "sale",
				},
				{
					title: "Personal Information & Privacy",
					class: "personal",
					texts:[
						"By submitting the above information, you agree to let BROOKS ENGLAND srl store your data and to use the information for statistical analysis and occasional promotional mailings in respect of privacy laws applicable in Italy and in the UK, where the website is hosted.",
						"Your details will not be shared with any party, other than BROOKS ENGLAND srl, and its parent companies Brooks England LTS and Selle Royal S.p.A. Your credit card information will only be used to process the order and will not be stored.",
					]
				},
			]
		}
})();