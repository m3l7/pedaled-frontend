(function(){
	angular
		.module("app")
		.controller("Billing",Billing);

		Billing.$inject = [
			"$scope",
			"commonData",
			"config",
			"cartService",
			"$routeParams",
			"security",
			"$q",
			"NewsletterService",
			"utils"
		];

		function Billing($scope,commonData,config,cartService,$routeParams, security, $q, NewsletterService,utils){

			$scope.cartService = cartService;
			$scope.countryCodes = commonData.countryCodes;
			$scope.register = false;
			$scope.useBillingForShipping = false;
			$scope.defaultBilling = false;
			$scope.billing_address = null;	
			$scope.loading = false;
			$scope.newsletterSubscribe = false;
			$scope.regions = null;

			//methods
			$scope.isAuthenticated = security.isAuthenticated;
			$scope.formSubmit = formSubmit;
			$scope.getRegions = getRegions;
			$scope.labelFiscalCode = "Tax/VAT Number";

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;
				cartService.fetchState()
				.then(function(state){
					if (!cartService.items.length) window.location.href = "/#!/checkout/cart";
					if (state.shipping_address=="use_billing") $scope.useBillingForShipping = true;
					$scope.billing_address = (state.billing_address=="default") ? angular.copy(state.default_billing_address): state.billing_address;
					$scope.getRegions();
				})

				if ($routeParams.param=="register") $scope.register = true;
			}

			function formSubmit(defaultBilling){

				if (defaultBilling) cartService.billing_address = $scope.billing_address = "default";
				if (defaultBilling) {
					// $scope.billing_address = cartService.default_billing_address;
					$scope.useBillingForShipping = false;
				}

				if ($scope.useBillingForShipping) cartService.shipping_address = "use_billing";
				else cartService.shipping_address = {};

				$scope.loading = true;

				$q(function(r){r()})
				.then(function(){
					//make VAT required for IT country
					if ((cartService.billing_address.country_id=="IT") && (!cartService.billing_address.vat_id)) return $q.reject("Tax/VAT number is required");
				})
				.then(function(){
					if ($scope.register) {
						cartService.billing_address.is_default_billing = true;
						cartService.billing_address.is_default_shipping = true;
						return cartService.saveBillingAddress(true);
					}
					else return true;
				})
				.then(function(){
					cartService.billing_address = $scope.billing_address;
					return cartService.saveBillingAddress();	
				})
				// .then(cartService.saveShippingAddress)
				.then(function(){
					// if ($scope.useBillingForShipping) console.log("use billing for shipping")
					if ($scope.useBillingForShipping) return cartService.saveShippingAddress();
					else return true;
				})
				.then(function(){
					if ($scope.useBillingForShipping) console.log("use billing for shipping")
					if ($scope.useBillingForShipping) return cartService.saveShippingMethod();
					else return true;
				})
				.then(function(){
					if ($scope.newsletterSubscribe){
						if (!cartService.billing_address.email) console.warn("Can't subscribe to the newsletter: missing email");
						else return NewsletterService.subscribe(cartService.billing_address.email)
							.catch(function(err){
								console.warn("can't subscribe to the newsletter: "+err);
								return true;
							})
					}
					else return true;
				})
				.then(function(){
					if ($scope.useBillingForShipping) window.location.href = "#!/checkout/payment";
					else window.location.href = "#!/checkout/shipping";
				})
				.catch(function(err){
					if ((typeof err=="object") && (err.length)) err = err[0];

					$scope.validationError = err;

					$scope.loading = false;
				})
			}


			function getRegions(){
				$scope.regions = null;
				if($scope.billing_address.country_id == 'IT') $scope.labelFiscalCode = "Fiscal code";
				else $scope.labelFiscalCode = "Tax/VAT Number";
				
				cartService.getRegionsByCountry($scope.billing_address.country_id)
				.then(function(regions){
					debugger;
					if(regions) $scope.regions = regions;
					else $scope.regions = null;
				})
			}

		}
})();


