(function(){
	angular
		.module("app")
		.controller("PaypalCallback",PaypalCallback);

		PaypalCallback.$inject = ["$scope","cartService", "$location", "commonData","utils"];

		function PaypalCallback($scope,cartService, $location, commonData,utils){

			$scope.error = false;
			$scope.errorMessage = null;
			$scope.loading = true;

			$scope.changePage = changePage;

			initialize();

			function initialize(){

				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");

				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;

				var token = $location.search().token;
				var PayerID = $location.search().PayerID;

				if ((!token) || (!PayerID)){
					$scope.loading = false;
					$scope.error = true;
					$scope.errorMessage = "Paypal authorization error";
				}
				else{
					cartService.authorizePaypalPayment(token,PayerID)
					.then(function(){
						$scope.loading = false;
						window.location.href = "#!/checkout/complete";
					})
					.catch(function(err){
						$scope.loading = false;
						$scope.errorMessage = "Paypal authorization error: "+err;
					})

				}
			}

			function changePage(url){
				if (url) window.location.href = url;
			}

		}
})();