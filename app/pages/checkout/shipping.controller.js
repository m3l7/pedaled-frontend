(function(){
	angular
		.module("app")
		.controller("Shipping",Shipping);

		Shipping.$inject = [
			"$scope",
			"commonData",
			"config",
			"cartService",
			"security",
			"utils"
		];

		function Shipping($scope,commonData,config,cartService,security,utils){

			$scope.cartService = cartService;
			$scope.formSubmit = formSubmit;
			$scope.countryCodes = commonData.countryCodes;
			$scope.defaultShipping = false;
			$scope.loading = false;
			$scope.regions = null;
			$scope.getRegions = getRegions;

			//methods
			$scope.isAuthenticated = security.isAuthenticated;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;
				cartService.fetchState({bypassCache:true})
				.then(function(){
					if (!cartService.items.length) window.location.href = "/#!/checkout/cart";
					if (cartService.shipping_address=="use_billing") cartService.shipping_address = angular.copy(cartService.billing_address);
					else $scope.shipping_address = (cartService.shipping_address=="default") ? angular.copy(cartService.default_shipping_address): cartService.shipping_address;
					debugger;
					$scope.getRegions();
				})
			}

			function formSubmit(defaultShipping){

				$scope.loading = true;

				if (defaultShipping) cartService.shipping_address = "default";

				if (cartService.shipping_methods.length) cartService.shipping_method = cartService.shipping_methods[0].id;

				cartService.saveShippingAddress()
				.then(cartService.saveShippingMethod)
				.then(function(){
					window.location.href = "#!/checkout/payment";
				})
				.catch(function(err){
					if (err.length) err = err[0];

					$scope.validationError = err;

					$scope.loading = false;
				})
			}

			function getRegions(){
				$scope.regions = {};
				var shipping_address = {};
				if(cartService.shipping_address == "default"){
					shipping_address = angular.copy(cartService.default_shipping_address);
				}else if(cartService.shipping_address == "use_billing"){
					shipping_address = angular.copy(cartService.billing_address);
				}else {
					shipping_address = angular.copy(cartService.shipping_address);
				}
				cartService.getRegionsByCountry(shipping_address.country_id)
				.then(function(regions){
					if(regions) $scope.regions = regions;
					else $scope.regions = null;
				})
			}

		}
})();