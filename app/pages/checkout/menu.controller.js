(function(){
	angular
		.module("app")
		.controller("CheckoutMenu",CheckoutMenu);

		CheckoutMenu.$inject = [
			"$scope",
			"utils",
			"security"
		];

		function CheckoutMenu($scope,utils,security){

			utils.setSEOMetaTitle("");
			utils.setSEOMetaDescription("");
			utils.setSEOMetaKeywords("");
			utils.setSEOMetaContentLanguage("");

			$scope.isPageActive = utils.isPageActive;
			$scope.security = security;
			$scope.changePage = changePage;
			$scope.pages = getPages();

			function changePage(page){
				if (page){
					var currentPage = getCurrentPage();
					if (page.id<currentPage.id) window.location.href = page.url;
				}
			}

			function getPages(){
				return [
					{
						id: 1,
						name: "Shopping Cart",
						url: "/#!/checkout/cart"
					},
					{
						id: 2,
						name: "Sign in",
						url: "/#!/checkout/signin",
						skipIfLogged: true
					},
					{
						id: 3,
						name: "Billing",
						url: "/#!/checkout/billing"
					},
					{
						id: 4,
						name: "Shipping",
						url: "/#!/checkout/shipping"
					},
					{
						id: 5,
						name: "Payment",
						url: "/#!/checkout/payment"
					}
				];
			}

			function getCurrentPage(){
				var pages = getPages();
				var currentPage;
				pages.forEach(function(page){
					if (utils.isPageActive(page.url)) currentPage = page;
				})
				return currentPage;
			}

		}

})();