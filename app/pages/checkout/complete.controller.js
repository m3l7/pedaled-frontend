(function(){
	angular
		.module("app")
		.controller("Complete",Complete);

		Complete.$inject = [
			"commonData",
			"cartService",
			"utils"
		];

		function Complete(commonData,cartService,utils){
			utils.setSEOMetaTitle("");
			utils.setSEOMetaDescription("");
			utils.setSEOMetaKeywords("");
			utils.setSEOMetaContentLanguage("");
			
			commonData.header.shrinked = true;

			cartService.fetchState({bypassCache:true});
		}
})();