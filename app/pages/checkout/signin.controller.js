(function(){
	angular
		.module("app")
		.controller("Signin",Signin);

		Signin.$inject = [
			"$scope",
			"commonData",
			"security",
			"cartService",
			"utils"
		];

		function Signin($scope,commonData,security,cartService,utils){

			$scope.security = security;
			$scope.loginParams = {username:"",password:""};
			$scope.loginError = null;
			$scope.guestError = null;
			$scope.signinMethod = "guest";
			$scope.countryCodes = commonData.countryCodes;
			$scope.loading = false;

			//methods
			$scope.proceed = proceed;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;

				security.fetchAccount()
				.then(function(user){
					if (user) $scope.user = user;
				})

				cartService.fetchState()
				.then(function(){
					if (!cartService.items.length) window.location.href = "/#!/checkout/cart";
				})
			}

			function login(email,password){
				if (email && password) security.login(email,password)
					.then(function(user){
						if (user) {
							$scope.user = user;
							$scope.loginParams = {username:"",password:""};
						}
					})
					.catch(function(err){
						$scope.loginError = err;
					})
			}

			function guestRegisterCheckout(){
				if ($scope.signinMethod=="guest") window.location.href = "#!/checkout/billing";
				else if ($scope.signinMethod=="register") window.location.href = "#!/checkout/billing/register";
			}

			function openNextPage(){
				window.location.href = "#!/checkout/billing";
			}

			function proceed(method){
				$scope.loading = true;
				$scope.signinMethod = method || $scope.signinMethod;

				if (method=="login"){
					security.login($scope.loginParams.email,$scope.loginParams.password)
					.then(function(){
						return cartService.fetchState({bypassCache:true});
					})
					.then(function(){
						window.location.href = "#!/checkout/billing";
					})
					.catch(function(err){
						$scope.loading = false;
						$scope.loginError = err;
					})
				}
				else if (method=="logged"){
					window.location.href = "#!/checkout/billing";
				}
				else security.logout()
					.then(function(){
						if ($scope.signinMethod=="register") window.location.href = "#!/checkout/billing/register";
						else if ($scope.signinMethod=="guest") window.location.href = "#!/checkout/billing";
					})
					.catch(function(err){
						$scope.loading = false;
						$scope.guestError = err;
					})
			}

		}
})();