(function(){
	angular
		.module("app")
		.controller("Cart",Cart);

		Cart.$inject = [
			"$scope",
			"commonData",
			"cartService",
			"security",
			"$q",
			"utils"
		];

		function Cart($scope,commonData,cartService,security,$q,utils){

			$scope.cartService = cartService;
			$scope.total = {price:0};
			$scope.loading = false;
			$scope.error = null;

			//methods
			$scope.decQuantity = decQuantity;
			$scope.incQuantity = incQuantity;
			$scope.quantityChanged = quantityChanged;
			$scope.openNextPage = openNextPage;
			$scope.changePage = changePage;
			$scope.removeItem = removeItem;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;
				commonData.header.filterBarVisible = false;
				commonData.header.breadcrumbVisible = false;

				cartService.itemsPopulated = false;
				cartService.fetchState({bypassCache:true})
				.then(function(){
					return cartService.populateCart();
				})
				.then(function(){
					$scope.totals = cartService.totals;
				})

			}

			function decQuantity(item){
				if (item){
					if (item.quantity<=0) item.quantity=0;
					else item.quantity--;
				}
				computeManualTotals();
				quantityChanged(item);
			}
			function incQuantity(item){
				if (item) item.quantity++;
				computeManualTotals();
				quantityChanged(item);
			}

			function quantityChanged(item){
				if (item) {
					item.changed = true;
					computeManualTotals();
				}
			}

			function computeManualTotals(){
				//bypass magento and compute manual totals...
				//it's not a good idea, but delete/readd products every time was too slow.
				var total = 0;
				cartService.items.forEach(function(item){
					total+= item.price*item.quantity;
				});
				cartService.grandTotal = total;
				$scope.totals.grand_total = {value: cartService.grandTotal};
				$scope.totals.subtotal = {value: cartService.grandTotal};
			}

			function removeItem(itemId){
				if (itemId){
					cartService.itemsPopulated = false;
					cartService.removeItem(itemId)
					.then(function(){
						cartService.itemsPopulated = true;
						$scope.totals.grand_total = {value: service.grandTotal};
						$scope.totals.subtotal = {value: service.grandTotal};
					})
				}
			}

			function openNextPage(){

				$scope.loading = true;

				//check if some items are changed, and save them (magento requires to delete and readd the products)
				var itemsToSave = angular.copy(cartService.items).filter(function(item){
						if (item.changed) return true;
						else return false;
					});

				$q.all(itemsToSave.map(function(item){
					return cartService.removeItem(item.itemId);
				}))
				.then(function(){
					return $q.all(itemsToSave.map(function(item){
						return cartService.addToCart(item.id,item.quantity);
					}))
				})
				.then(function(){
					window.location.href = (security.isAuthenticated()) ? "#!/checkout/billing" : "#!/checkout/signin";
				})
				.catch(function(err){
					$scope.error = err;
				})


			}
			function changePage(page){
				if (page) window.location.href = page;
			}

		}
})();