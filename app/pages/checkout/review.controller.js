(function(){
	angular
		.module("app")
		.controller("Review",Review);

		Review.$inject = [
			"$scope",
			"cartService",
			"commonData",
			"$q",
			"utils"
		];

		function Review($scope,cartService,commonData,$q,utils){

			$scope.cartService = cartService;
			$scope.creditCardTypes = cartService.creditCardTypes;
			$scope.sendError = "";
			$scope.billing_address = null;
			$scope.shipping_address = null;
			$scope.loading = false;

			$scope.sendOrder = sendOrder;
			$scope.paypalOrder = paypalOrder;

			initialize();

			function initialize(){
				utils.setSEOMetaTitle("");
				utils.setSEOMetaDescription("");
				utils.setSEOMetaKeywords("");
				utils.setSEOMetaContentLanguage("");
				
				commonData.header.shrinked = true;
				cartService.fetchState({bypassCache:true})
				.then(function(){
					if (!cartService.items.length) window.location.href = "/#!/checkout/cart";

					$scope.billing_address = (cartService.billing_address=="default") ? cartService.default_billing_address : cartService.billing_address;
					if (cartService.shipping_address=="use_billing") $scope.shipping_address = $scope.billing_address;
					else if (cartService.shipping_address=="default") $scope.shipping_address = cartService.default_shipping_address;
					else $scope.shipping_address = cartService.shipping_address;

					return cartService.populateCart();
				})
				.then(function(){
						$scope.items = cartService.items;
				})
			}

			function sendOrder(){
				$scope.loading = true;

				$q(function(r){r();})
				.then(function(){
					if (!checkDateValidity()) return $q.reject("please enter a valid expiration date");
				})
				.then(cartService.savePaymentMethod)
				.then(cartService.sendOrder)
				.then(function(){
					window.location.href = "#!/checkout/complete";
				})
				.catch(function(err){
					$scope.sendError = err;
					$scope.loading = false;
				})
			}

			function checkDateValidity(){
				var month = parseInt(cartService.payment.cc_exp_month);
				var year = parseInt(cartService.payment.cc_exp_year);
				if ((month<1) || (month>12)) return false;
				else return true;
			}

			function paypalOrder(){
				$scope.loading = true;
				cartService.payment = {method:"paypal_express"};
				cartService.savePaymentMethod()
				.then(cartService.paypalRedirect)
				.catch(function(err){
					$scope.sendError = err;
					$scope.loading = false;
				})
			}
		}
})();