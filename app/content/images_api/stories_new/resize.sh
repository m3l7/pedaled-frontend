#!/bin/bash

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for f in */
do
  for big in "$f"big/*.jpg
  do
    if [ -f "$big" ]; then
       convert -geometry 1920x -quality 50 $big "$big"_big.jpg
       convert -geometry 900x -quality 50 $big "$big"_small.jpg
       rm $big
    fi
  done
  for bigg in "$f"big/*.jpeg
  do
    if [ -f "$bigg" ]; then
       convert -geometry 1920x -quality 50 $bigg "$bigg"_big.jpeg
       convert -geometry 900x -quality 50 $bigg "$bigg"_small.jpeg
       rm $bigg
    fi
  done
  for gallery in "$f"gallery/*.jpg
  do
    if [ -f "$gallery" ]; then
       convert -geometry 1920x -quality 50 $gallery "$gallery"_big.jpg
       convert -geometry 400x -quality 50 $gallery "$gallery"_small.jpg
       rm $gallery
    fi
  done
  for galleryy in "$f"gallery/*.jpeg
  do
    if [ -f "$galleryy" ]; then
       convert -geometry 1920x -quality 50 $galleryy "$galleryy"_big.jpeg
       convert -geometry 400x -quality 50 $galleryy "$galleryy"_small.jpeg
       rm $galleryy
    fi
  done
done
IFS=$SAVEIFS
