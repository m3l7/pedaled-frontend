#!/bin/bash -x

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
  for big in *.jpg
  do
    if [ -f "$big" ]; then
       convert -geometry 900x -quality 50 $big ../small/"$big"
    fi
  done
IFS=$SAVEIFS
