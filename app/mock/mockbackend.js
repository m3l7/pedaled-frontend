// Backend mocking with fake data

(function(){
    angular
        .module('app')
        .run(mockBackend);

    mockBackend.$inject = ['$httpBackend','testData','config',"utils"];

    function mockBackend($httpBackend,testData,config,utils){
        // PRODUCTS AND CATEGORIES
            // var productRegexp = new RegExp(/.*/);
            var productRegexp = new RegExp(/api\/products\/([0-9]+)/);
           $httpBackend.whenGET(productRegexp).respond(function(method,url,data){
                var id = url.match(productRegexp)[1];
                var products = testData.products;
               return [200,utils.indexBy(products,"id")[id],{}];
           })
           $httpBackend.whenGET(/api\/products/).respond(function(method,url,data){
           // $httpBackend.whenGET(/rest\/products/).respond(function(method,url,data){
               return [200,testData.products,{}];
           })
           $httpBackend.whenGET(/api\/categories/).respond(function(method,url,data){
               return [200,testData.categories,{}];
           })
        // DEFAULT
            $httpBackend.whenGET(/.*/).passThrough();
            $httpBackend.whenPOST(/.*/).passThrough();
            $httpBackend.whenPUT(/.*/).passThrough();
            $httpBackend.whenDELETE(/.*/).passThrough();
    }

})();