// Backend mocking with fake data

(function(){
    angular
        .module('app')
        .run(mockBackend);

    mockBackend.$inject = ['$httpBackend'];

    function mockBackend($httpBackend){
        
        // DEFAULT
            $httpBackend.whenGET(/.*/).passThrough();
            $httpBackend.whenPOST(/.*/).passThrough();
            $httpBackend.whenPUT(/.*/).passThrough();
            $httpBackend.whenDELETE(/.*/).passThrough();

    }

})();