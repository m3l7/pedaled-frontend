(function(){

    angular
        .module('app')
        .factory('testData',testData);
    
    testData.$inject = ["utils"];

	function testData(utils){
	    return {
	        products: getProducts(),
	        categories: getCategories()
	    }

	    function getCategories(){

	    	var productsIndex = utils.indexBy(getProducts(),"id");

	    	var categories = [
	    		{
	    			id: "1",
	    			name: "Jacket",
	    			meta_title: "Jackets",
	    			meta_description: "Cycling specific product",
	    			description: "Our jersey collection satisfies the needs of every type of ride. From winter training to the hottest.",
	    			image: "/content/images_api/categories/jacket.jpg",
	    			position: 1,
	    		},
	    		{
	    			id: "2",
	    			name: "Top",
	    			meta_title: "Tops",
	    			meta_description: "Cycling specific product",
	    			description: "Our jersey collection satisfies the needs of every type of ride. From winter training to the hottest.",
	    			image: "/content/images_api/categories/top.jpg",
	    			position: 3,
	    		},
	    		{
	    			id: "3",
	    			name: "Bottoms",
	    			meta_title: "Bottoms",
	    			meta_description: "Cycling specific product",
	    			description: "Our jersey collection satisfies the needs of every type of ride. From winter training to the hottest.",
	    			image: "/content/images_api/categories/bottom.jpg",
	    			position: 2,
	    		},
	    		{
	    			id: "4",
	    			name: "Shoes",
	    			meta_title: "Shoes",
	    			meta_description: "Cycling specific product",
	    			description: "Our jersey collection satisfies the needs of every type of ride. From winter training to the hottest.",
	    			image: "/content/images_api/categories/shoes.jpg",
	    			position: 1,
	    		}
	    	];

	    	// categories[0].products = [productsIndex[3]];
	    	// categories[1].products = [productsIndex[12]];
	    	// categories[2].products = [productsIndex[12]];
	    	// categories[3].products = [productsIndex[3]];

	    	return categories;
	    }

	    function getProducts(){
	    	var products = [
			    {
			        "id": "30",
			        "sku": "S12T-kan-RS",
			        "name": "Transcontinental Cap",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 50,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 19,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/cap/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            4
			        ]
			    },
			    {
			        "id": "29",
			        "sku": "S12T-kan-RS",
			        "name": "Mido Riding Boots",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 50,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 250,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/mido/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            4
			        ]
			    },
			    {
			        "id": "28",
			        "sku": "S12T-kan-RS",
			        "name": "Winter Boots",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 50,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 250,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/winterboots/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            4
			        ]
			    },
			    {
			        "id": "27",
			        "sku": "S12T-kan-RS",
			        "name": "CMWC 2015 Elastica Belt",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 50,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 79,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/cmwc/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            3
			        ]
			    },
			    {
			        "id": "26",
			        "sku": "S12T-kan-RS",
			        "name": "Padded Boxers",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 50,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 50,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/padded/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            3
			        ]
			    },
			    {
			        "id": "25",
			        "sku": "S12T-kan-RS",
			        "name": "Reflective Denim Trousers",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 120,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 140,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/reflectivejeans/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            3
			        ]
			    },
			    {
			        "id": "24",
			        "sku": "S12T-kan-RS",
			        "name": "Okabe Jersey",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 120,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 110,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/okabe/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            2
			        ]
			    },
			    {
			        "id": "23",
			        "sku": "S12T-kan-RS",
			        "name": "Retro Wool Polo",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 120,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 140,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/retro/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            2
			        ]
			    },
			    {
			        "id": "22",
			        "sku": "S12T-kan-RS",
			        "name": "Saddle Packable Jacket",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 120,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 200,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/saddle/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            1
			        ]
			    },
			    {
			        "id": "21",
			        "sku": "S12T-kan-RS",
			        "name": "Gufo Jacket",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 120,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 215,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/gufo/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            1
			        ]
			    },
			    {
			        "id": "20",
			        "sku": "S12T-kan-RS",
			        "name": "Kanaya Jacket",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 150,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 215,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/kanaya/05.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            1
			        ]
			    },
			    {
			        "id": "3",
			        "sku": "S12T-Gec-RS",
			        "name": "Kaido Jersey",
			        "type": "simple",
			        "visibility": "4",
			        "quantity": 150,
			        "friendUrl": "http://localhost:90/index.php/sendfriend/product/send/id/3/",
			        "weight": "30.0000",
			        "price": 110,
			        "colour": 0,
			        "manufacturer": 0,
			        "description": "<p>long <strong>description</strong></p>",
			        "short_description": "Waterproof Jacket",
			        "largeImage": "/content/images_api/products/kaido/01.png",
			        "gallery": [],
			        "products": [],
			        "models": [],
			        "similar": [],
			        "upsell": [],
			        "crosssell": [],
			        "meta_keywords": "keyword1,keyword2",
			        "meta_description": "meta description",
			        "meta_title": "Meta title",
			        "categories": [
			            2
			        ]
			    },
		    ];

		    products.forEach(function(product){
		    	product.gallery = product.gallery.concat([
		            "/content/images_api/products/new/1.jpg",
		            "/content/images_api/products/new/2.jpg",
		            "/content/images_api/products/new/3.jpg",
		            // "/content/images_api/products/Base-Foto-Crispy.jpg",
		            // "/content/images_api/products/Base-Foto-Crispy (1).jpg",
		            // "/content/images_api/products/wide/pedaled_wide3.jpeg",
		            // "/content/images_api/products/wide/pedaled_wide.jpeg",
		            // "/content/images_api/products/wide/pedaled_wide2.jpeg",
			    ]);

			    product.description = "Our Gufo Jacket is a packable, lightweight cycling windbreaker with a regular or snug fit, flexible for sport or casual use. Featuring a longer cut back, with a larger rear pocketa";
		    })

		    return products;
		}
	}


})();
