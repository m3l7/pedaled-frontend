(function(){
	angular
		.module("app")
		.run(Run);

		Run.$inject = ["config","commonData"];

		function Run(config,commonData){
			if (config.screenSize.xs>=window.screen.width) commonData.viewPort = "XS";
			else if (config.screenSize.md>=window.screen.width) commonData.viewPort = "MD";
			else if (config.screenSize.mdlg>=window.screen.width) commonData.viewPort = "MDLG";
			else commonData.viewPort = "LG";
		}
})();