(function(){
    angular
        .module('app')
        .factory('instagramModel',instagramModel);

        instagramModel.$inject = ['DS'];

        function instagramModel(DS){

            return DS.defineResource({
                name: 'instagram',
                idAttribute: "image_id",
                endpoint: "instagram/photos"
            });
        }
})();