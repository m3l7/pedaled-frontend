(function(){
    angular
        .module('app')
        .factory('colorModel',colorModel);

        colorModel.$inject = ['DS'];

        function colorModel(DS){

            return DS.defineResource({
                name: 'color',
                endpoint: "attributes/color",
                idAttribute: "id"
            });
        }
})();