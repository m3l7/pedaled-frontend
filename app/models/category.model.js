(function(){
    angular
        .module('app')
        .factory('categoryModel',categoryModel);

        categoryModel.$inject = ['DS',"productModel","cartService","$q"];

        function categoryModel(DS,productModel,cartService,$q){

            return DS.defineResource({
                name: 'category',
                endpoint: "categories",
                idAttribute: "id",
                relations:{
                    hasMany:{
                        product:{
                            localField: "products",
                            foreignKey: "categoryId"
                        }
                    }
                },
                beforeInject: function(resource,attrs){
                    //TODO: 
                    //INJECT ADDITIONAL PROPERTIES. TO REMOVE WHEN REAL BACKEND IS COMPLETE
                    attrs.forEach(function(category){
                        // if (!category.position) category.position = Math.trunc(Math.random()*3);
                        if (!category.position) category.position = 0;
                    })
                },
                methods:{
                    loadProducts: function(products){
                        //populate products
                        var category = this;
                        var categoryProducts = [];

                        var promise = (products) ? $q.all(products) : DS.findAll("product",{storeId:cartService.activeStoreId});
                        
                        promise
                        .then(function(products){
                            products.forEach(function(product){
                                if (product.categories) product.categories.forEach(function(cat){
                                    if ((typeof cat == "object") && (cat.id==category.id)) categoryProducts.push(product);
                                    else if (cat==category.id) categoryProducts.push(product);
                                })
                            })
                            category.products = categoryProducts;
                        })
                    }
                }
            });
        }
})();