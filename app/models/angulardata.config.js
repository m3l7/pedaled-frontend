(function(){
	angular
		.module("app")
		.config(dataConfig);

		dataConfig.$inject = ["DSProvider","config"];

		function dataConfig(DSProvider,config){
                  angular.extend(DSProvider.defaults,config.API);

                  DSProvider.defaults.___deserialize = function(resourceConfig,data){
                  	if ((data.data) && (!data.data.entity_id)){
                  		var dataOut = [];
                  		for (var i in data.data){
                  			dataOut.push(data.data[i]);
                  		}
                  		data.data = dataOut;
                  	}

                  	//change relations key from objects
                  	if ((data.data) && (data.data.length)) data.data.forEach(normalizeCategoryKey);
                  	else if (typeof data=="object") normalizeCategoryKey(data.data);

                  	function normalizeCategoryKey(item){
                  		if (resourceConfig.name=="product"){
                  			if ((item.categoryId) && (typeof item.categoryId=="object")) {
                  				item.category = item.categoryId;
                  				delete item.categoryId;
                  			}
                  			if ((item.category) && (typeof item.category=="number")) {
                  				item.categoryId = item.category;
                  				delete item.category;
                  			}
                  		}
                  	}

                  	return data.data;
                  }
		}
})();