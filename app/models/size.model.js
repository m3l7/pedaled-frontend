(function(){
    angular
        .module('app')
        .factory('sizeModel',sizeModel);

        sizeModel.$inject = ['DS'];

        function sizeModel(DS){

            return DS.defineResource({
                name: 'size',
                endpoint: "attributes/size",
                idAttribute: "id"
            });
        }
})();