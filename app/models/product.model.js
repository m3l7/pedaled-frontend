(function(){
    angular
        .module('app')
        .factory('productModel',productModel);

        productModel.$inject = ['DS',"$q","$http","colorModel","sizeModel","utils", "config"];

        function productModel(DS,$q,$http, colorModel,sizeModel,utils, config){

            return DS.defineResource({
                name: 'product',
                endpoint: "products",
                idAttribute: "id",
                beforeInject: function(resource,instances){
                    //make materials an array (splitted by '-'), split features
                    if ((instances) && (instances.length)) instances.forEach(function(product){
                        product = sanitizeProduct(product);
                    })
                    else if (instances) instances = sanitizeProduct(instances);

                    function sanitizeProduct(product){
                        if (product.materials) product.materials = product.materials.split("-");
                        if (product.short_description) product.features = product.short_description.split(", ");
                        if (product.sizeChart) product.sizeCharts = [product.sizeChart];
                        if (product.urlKey=="undefined") delete product.urlKey;
                        product.inStock = product.inStock=="1" ? true : false;
                        product.models.forEach(function(model){
                            model.inStock = model.inStock=="1" ? true : false;
                        })

                        return product;
                    }
                },
                findByNameOrId: function(nameId,store){
                    //redirect to findByName or Id depending on nameId type
                    var findByName = isNaN(nameId); //id is a string, find by name instead of by id
                    if (findByName) return this.findByName(nameId,store);
                    else return this.findById(nameId,store);
                },
                findByName:function(name,store){
                    var storeString = (store) ? "?storeId="+store : "";
                    if (!name) return $q.reject("Missing name parameter");
                    else return $http.get(DS.defaults.basePath+this.endpoint+"/name/"+name+storeString)
                        .then(function(data){
                            if ((data) && (data.data) && (data.data.id)) {
                                DS.inject("product",data.data);
                                return DS.get("product",data.data.id);
                            }
                        })
                },
                findByUrlKey:function(urlkey,store){
                    var storeString = (store) ? "?storeId="+store : "";
                    if (!urlkey) return $q.reject("Missing urlkey parameter");
                    else return $http.get(DS.defaults.basePath+this.endpoint+"/urlkey/"+urlkey+storeString)
                        .then(function(data){
                            if ((data) && (data.data) && (data.data.id)) {
                                DS.inject("product",data.data);
                                return DS.get("product",data.data.id);
                            }
                        })
                },
                findById:function(id,store){
                    var storeString = (store) ? "?storeId="+store : "";
                    if (!id) return $q.reject("Missing id parameter");
                    else return $http.get(DS.defaults.basePath+this.endpoint+"/"+id+storeString)
                        .then(function(data){
                            if ((data) && (data.data) && (data.data.id)) {
                                DS.inject("product",data.data);
                                return DS.get("product",data.data.id);
                            }
                        })
                },
                methods:{
                    loadCategories: function(){
                        var product = this;
                        var categories = DS.getAll("category");
                        var productCategories = [];
                        categories.forEach(function(category){
                            product.categories.forEach(function(productCategoryId){
                                if (productCategoryId==parseInt(category.id)) productCategories.push(category);
                            })
                        })
                        this.categories = productCategories;
                    },
                    loadRelatedProducts: function(store){
                        //populate related, upsells and crosssells products
                        var product = this;
                        var fields = ["similar","upsell"]; //fields to populate
                        var storeOption = store ? {store: store} : null;
                        return DS.findAll("product",storeOption)
                            .then(function(products){
                                var productsIndex = utils.indexBy(products,"id");

                                // fields.forEach(function(field){
                                return $q.all(fields.map(function(field){
                                    if (product[field]) {

                                        //fill min(4,products.length) random products if there are less than 4 products
                                        var n = (products.length>4) ? 4 : products.length;
                                        while (product[field].length<n){
                                            var index = Math.floor(Math.random()*products.length);
                                            var id = products[index].id;
                                            if (product[field].indexOf(id)==-1) product[field].push(id);
                                        }

                                        //populate products and populate colors/sizes
                                        return $q.all(product[field].map(function(item){
                                            var relatedItem = DS.get("product",item);
                                            return relatedItem.loadAttributes();
                                        }))
                                    }
                                }))
                            })
                            .then(function(relatedGroups){
                                fields.forEach(function(field,i){
                                    product[field] = relatedGroups[i];
                                })

                                return product;
                            })
                    },
                    loadAttributes: function(){
                       var product = this;
                        return $q.all([
                            DS.findAll("color"),
                            DS.findAll("size"),
                            angular.copy(product)
                        ])
                        .then(function(data){
                            var product = data[2];
                            var colorsIndex = utils.indexBy(data[0],"id");
                            var sizesIndex = utils.indexBy(data[1],"id");

                            product.models.forEach(function(model){
                                model.color = colorsIndex[model.color];
                                model.size = sizesIndex[model.size];
                            })

                            if (typeof product.color == "object"){
                                var colors = [];
                                if (product.color.length) product.color.forEach(function(colorItem){
                                    if (typeof colorItem == "number") colors.push(colorsIndex[colorItem]);
                                })
                                if (colors.length) product.color = colors;                            

                            }
                            else if (typeof product.color == "number") product.color = colorsIndex[product.color];

                            if (typeof product.size == "object"){
                                var sizes = [];
                                if (product.size.length) product.size.forEach(function(sizeItem){
                                    if (typeof sizeItem=="number") sizes.push(sizesIndex[sizeItem]);
                                })
                                if (sizes.length) product.size = sizes;                            

                            }
                            else if (typeof product.size == "number") product.size = sizesIndex[product.size];

                            product = orderColors(product);

                            return product;
                        })

                        function orderColors(product){
                            //sort colors based on square (configurable) images order
                            var colors = [];
                            var colorsIndex = [];

                            if ((!product.color) || (!product.color.forEach)) return product;

                            product.gallery.forEach(function(image){
                                var filename = image.replace(/^.*\/|\.[^.]*$/g, '');
                                var galleryColorId = parseInt(filename.slice(9,11));
                                var galleryColorName = config.colors[galleryColorId];
                                if (galleryColorName) galleryColorName = galleryColorName.string;
                                else return;

                                product.color.forEach(function(color){
                                    if ((color.admin_label==galleryColorName) && (colorsIndex.indexOf(color.id)==-1)){
                                        colors.push(color);
                                        colorsIndex.push(color.id);
                                    }
                                })
                            })

                            //fill orphan colors (those which don't have square images)
                            product.color = colors.concat(product.color.filter(function(color){
                                if (colorsIndex.indexOf(color.id)==-1) return true;
                                else return false;
                            }))

                            return product;
                        }

                    },
                    computeAttributesCombinations: function(){
                        //compute size/color combination availability
                        var product = this;

                        if ((this.models) && (this.models.length)) this.models.forEach(function(model){
                            var size = model.size;
                            var color = model.color;
                            model.colorsAvailable = [];
                            model.sizesAvailable = [];

                            product.models.forEach(function(model2){
                                //search for models which have the same size/color
                                if (model2.size.id==model.size.id) model.colorsAvailable.push(model2.color);
                                if (model2.color.id==model.color.id) model.sizesAvailable.push(model2.size);
                            })

                            model.colorsAvailableIndex = utils.indexBy(model.colorsAvailable,"id");
                            model.sizesAvailableIndex = utils.indexBy(model.sizesAvailable,"id");

                        })
                    },
                    indexAttributes: function(){
                        var product = this;

                        var colors = [];
                        var sizes = [];
                        if (this.models.length) this.models.forEach(function(model){
                            if (colors.indexOf(model.color)==-1) colors.push(model.color);
                            if (sizes.indexOf(model.size)==-1) sizes.push(model.size);
                        })
                        this.colors = colors;
                        this.sizes = sizes;
                        this.colorsIndex = utils.indexBy(colors,"id");
                        this.sizesIndex = utils.indexBy(sizes,"id");

                    }
                }
            });
        }
})();


