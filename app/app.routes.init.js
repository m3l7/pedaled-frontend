(function(){
	angular
	.module("app")
	.run(routesInit);

	routesInit.$inject = ["$rootScope","commonData","$anchorScroll","HeaderService"];

	function routesInit($rootScope,commonData,$anchorScroll,HeaderService){
		$rootScope.$on('$routeChangeStart', function (event, next) {
			if (next) {
				commonData.mobileMenuActive = false;
				HeaderService.toggleMenu();
				$anchorScroll();
				$rootScope.ogMeta = {}; // reset og meta
				$rootScope.seoMeta = {}; // reset seo meta

				document.onkeydown = undefined;
			}
		});
	}
})();