var express = require('express')
    , http = require('http')
    , path = require('path')
    , app = express()
    , compression = require("compression")
    , jade = require("jade")
    , _ = require("lodash")
    ,fs = require("fs");

// var baseUrl = "http://localhost:15000/"; //used for escaped fragment 
var baseUrl = "http://hideto.pedaled.com/"; //used for escaped fragment 

// all environments
app.set('port', process.env.PORT || 15002);
app.use(compression({
	level: 9
}));

if (app.get('env')=='dist') appDir = "dist";
else if (app.get('env')=='prod') appDir = "prod";
else appDir = "prod";

var conv = {
    '/instagram': '/#!/instagram',
    '/stories': '/#!/stories',
    '/about-pedal-ed': '/#!/about',
    '/category/Jackets': '/#!/category/Jackets',
    '/category/all': '/#!/products',
    '/category/sale': '/#!/products',
    '/category/Red_Collection': '/#!/products',
    '/category/jackets?uri=%2Fcategory%2Fjackets&page=2':'/#!/category/Jackets',
    '/category/jackets?uri=%2Fcategory%2Fjackets&page=3':'/#!/category/Jackets',
    '/category/tops': '/#!/category/Jerseys%20&%20Baselayers',
    '/category/tops?uri=%2Fcategory%2Ftops&page=2': '/#!/category/Jerseys%20&%20Baselayers',
    '/category/bottoms': '/#!/category/Bib%20Shorts',
    '/category/bottoms?uri=%2Fcategory%2Fbottoms&page=2': '/#!/category/Bib%20Shorts',
    '/category/shoes-accessories': '/#!/category/Accessories%20&%20Shoes',
    '/category/shoes-accessories?uri=%2Fcategory%2Fshoes-accessories&page=2': '/#!/category/Accessories%20&%20Shoes',
    '/category/shoes-accessories?uri=%2Fcategory%2Fshoes-accessories&page=3': '/#!/category/Accessories%20&%20Shoes',
    '/category/shoes-accessories?uri=%2Fcategory%2Fshoes-accessories&page=4': '/#!/category/Accessories%20&%20Shoes',
    '/category/hats-caps': '/#!/category/Accessories%20&%20Shoes',
    '/product/view/1317': '/#!/products/attakai-wool-jacket/black',
    '/product/view/1317#': '/#!/products/attakai-wool-jacket/black',
    '/product/view/kaze-access-vest': '/#!/products/kaze-access-vest/black',
    '/product/view/kaze-access-vest#': '/#!/products/kaze-access-vest/black',
    '/product/view/Kanaya-Jacket': '/#!/products/kanaya-jacket/black',
    '/product/view/Kanaya-Jacket#': '/#!/products/kanaya-jacket/black',
    '/product/Gufo-Wind-Jacket': '/#!/products/gufo-jacket/white',
    '/product/Gufo-Wind-Jacket#': '/#!/products/gufo-jacket/white',
    '/product/view/Saddle-Packable-Jacket': '/#!/products/saddle-packable-jacket/bordeaux',
    '/product/view/Saddle-Packable-Jacket#': '/#!/products/saddle-packable-jacket/bordeaux',
    '/product/view/Discovery-Shell-Jacket': '/#!/products/discovery-shell/black',
    '/product/view/Discovery-Shell-Jacket#': '/#!/products/discovery-shell/black',
    '/product/view/Adventure-Jacket': '/#!/products/adventure-jacket/lightgreymelange',
    '/product/view/Adventure-Jacket#': '/#!/products/adventure-jacket/lightgreymelange',
    '/product/view/Denim-Adventure-Jacket': '/#!/products/denim-adventure-jacket/indigo',
    '/product/view/Denim-Adventure-Jacket#': '/#!/products/denim-adventure-jacket/indigo',
    '/product/view/Waterproof-Adventure-Jacket': '/#!/category/Jackets',
    '/product/view/Waterproof-Adventure-Jacket#': '/#!/category/Jackets',
    '/product/view/Urban_Jacket': '/#!/category/Jackets',
    '/product/view/Urban_Jacket#': '/#!/category/Jackets',
    '/product/view/PEdALED-LAB-Jacket': '/#!/category/Jackets',
    '/product/view/PEdALED-LAB-Jacket#': '/#!/category/Jackets',
    '/product/view/Hacking-Jacket': '/#!/category/Jackets',
    '/product/view/Hacking-Jacket#': '/#!/category/Jackets',
    '/product/view/Rakkasan-Jacket': '/#!/category/Jackets',
    '/product/view/Rakkasan-Jacket#': '/#!/category/Jackets',
    '/product/view/Rainfrog-Jacket': '/#!/category/Jackets',
    '/product/view/Rainfrog-Jacket#': '/#!/category/Jackets',
    '/product/view/Retro-Wool-Polo-Italy-Edition': '/#!/products/undefined/black',
    '/product/view/Retro-Wool-Polo-Italy-Edition#': '/#!/products/undefined/black',
    '/product/view/Retro_Wool_Polo_limited_edition': '/#!/products/undefined/black',
    '/product/view/Retro_Wool_Polo_limited_edition#': '/#!/products/undefined/black',
    '/product/view/kaze-access-vest': '/#!/products/kaze-access-vest/black',
    '/product/view/kaze-access-vest#': '/#!/products/kaze-access-vest/black',
    '/product/view/kaido-jersey-long-sleeve': '/#!/products/kaido-jersey-long-sleeve/black',
    '/product/view/kaido-jersey-long-sleeve#': '/#!/products/kaido-jersey-long-sleeve/black',
    '/product/view/Kaido-Jersey': '/#!/products/kaido-jersey/black',
    '/product/view/Kaido-Jersey#': '/#!/products/kaido-jersey/black',
    '/product/view/Cycling-Jersey-Okabe': '/#!/products/okabe-jersey/black',
    '/product/view/Cycling-Jersey-Okabe#': '/#!/products/okabe-jersey/black',
    '/product/transcontinental-race-2015-logo-tee': '/#!/products/logo-tee-transcontinental-race/white',
    '/product/transcontinental-race-2015-logo-tee#': '/#!/products/logo-tee-transcontinental-race/white',
    '/product/view/Wiki-Baselayer': '/#!/products/wiki-baselayer/white',
    '/product/view/Wiki-Baselayer#': '/#!/products/wiki-baselayer/white',
    '/product/view/Logo-Tee': '/#!/products/logo-tee/black',
    '/product/view/Logo-Tee#': '/#!/products/logo-tee/black',
    '/product/view/Garage-Shirt': '/#!/products/garage-shirt/black',
    '/product/view/Garage-Shirt#': '/#!/products/garage-shirt/black',
    '/product/view/Tagi-Merino-Pullover': '/#!/products/tagi-merino-pullover/black',
    '/product/view/Tagi-Merino-Pullover#': '/#!/products/tagi-merino-pullover/black',
    '/product/view/Red-Hook-Criterium-2014-Riding-Shirt': '/#!/category/Tops%20&%20T-Shirts',
    '/product/view/Red-Hook-Criterium-2014-Riding-Shirt#': '/#!/category/Tops%20&%20T-Shirts',
    '/product/view/1387': '/#!/products/leg-warmers/black',
    '/product/view/1387#': '/#!/products/leg-warmers/black',
    '/product/view/daikan-winter-bib-shorts': '/#!/products/daikann-winter-bibshort/black',
    '/product/view/daikan-winter-bib-shorts#': '/#!/products/daikann-winter-bibshort/black',
    '/product/view/Raku-Bib-Short-Black': '/#!/products/raku-bibshort/black',
    '/product/view/Raku-Bib-Short-Black#': '/#!/products/raku-bibshort/black',
    '/product/view/Raku-Bib-Short-Brown': '/#!/products/raku-bibshort/black',
    '/product/view/Raku-Bib-Short-Brown#': '/#!/products/raku-bibshort/black',
    '/product/view/Discovery-Shorts': '/#!/products/discovery-shorts/black',
    '/product/view/Discovery-Shorts#': '/#!/products/discovery-shorts/black',
    '/product/view/Padded-Cycling-Boxers': '/#!/products/padded-boxer/black',
    '/product/view/Padded-Cycling-Boxers#': '/#!/products/padded-boxer/black',
    '/product/view/Workman-Trousers': '/#!/products/cycling-chinos/beige',
    '/product/view/Workman-Trousers#': '/#!/products/cycling-chinos/beige',
    '/product/view/Reflective-Denim-Trousers':'/#!/products/reflective-denim/indigo',
    '/product/view/Reflective-Denim-Trousers#':'/#!/products/reflective-denim/indigo',
    '/product/view/1393': '/#!/products/kubi-neck-warmer/bordeaux',
    '/product/view/1393#': '/#!/products/kubi-neck-warmer/bordeaux',
    '/product/view/Kaido-Arm-Warmers': '/#!/products/arm-warmers/black',
    '/product/view/Kaido-Arm-Warmers#': '/#!/products/arm-warmers/black',
    '/product/view/Winter-Cycling-Hat': '/#!/products/winter-hat/black',
    '/product/view/Winter-Cycling-Hat#': '/#!/products/winter-hat/black',
    '/product/view/mizu-water-bottle':'/#!/products/mizu-water-bottle/black',
    '/product/view/mizu-water-bottle#':'/#!/products/mizu-water-bottle/black',
    '/product/view/dario-pegoretti-cycling-socks-dots-style': '/#!/products/dario-sock-dots/blue',
    '/product/view/dario-pegoretti-cycling-socks-dots-style#': '/#!/products/dario-sock-dots/blue',
    '/product/view/dario-pegoretti-cycling-socks-flag-style': '/#!/products/dario-socks-flag/black',
    '/product/view/dario-pegoretti-cycling-socks-flag-style#': '/#!/products/dario-socks-flag/black',
    '/product/view/dario-pegoretti-cycling-socks-solid-style': '/#!/products/dario-socks-solid/black',
    '/product/view/dario-pegoretti-cycling-socks-solid-style#': '/#!/products/dario-socks-solid/black',
    '/product/view/Mido-Riding-Boots': '/#!/products/mido-riding-boots/black',
    '/product/view/Mido-Riding-Boots#': '/#!/products/mido-riding-boots/black',
    '/product/view/Winter-Boots': '/#!/products/mido-riding-boots/black',
    '/product/view/Winter-Boots#': '/#!/products/mido-riding-boots/black',
    '/blog/PEdAL_ED-mido_riding_boots_treatment': '/#!/products/mido-riding-boots/black',
    '/product/transcontinental-race-2015-cycling-cap': '/#!/products/id/6986/white',
    '/product/transcontinental-race-2015-cycling-cap#': '/#!/products/id/6986/white',
    '/product/transcontinental-race-2015-logo-tee': '/#!/products/logo-tee-transcontinental-race/white',
    '/product/transcontinental-race-2015-logo-tee#': '/#!/products/logo-tee-transcontinental-race/white',
    '/product/transcontinental-race-2015-musette': '/#!/products/transcontinental-musette-2015/black',
    '/product/transcontinental-race-2015-musette#': '/#!/products/transcontinental-musette-2015/black',
    '/product/view/Elastica-Belt': '/#!/products/elastica-belt/black',
    '/product/view/Elastica-Belt#': '/#!/products/elastica-belt/black',
    '/product/view/CMWC_2015_special_edition_elastica_belt': '/#!/category/Accessories%20&%20Shoes',
    '/product/view/CMWC_2015_special_edition_elastica_belt#': '/#!/category/Accessories%20&%20Shoes',
    '/product/view/Summer-Cycling-Cap': '/#!/products/summer-cap/blue',
    '/product/view/Summer-Cycling-Cap#': '/#!/products/summer-cap/blue',
    '/product/view/merino-wool-socks': '/#!/products/merino-long-socks/black',
    '/product/view/merino-wool-socks#': '/#!/products/merino-long-socks/black',
    '/product/view/Transcontinental-2014-Cycling-Cap': '/#!/products/id/6986/white',
    '/product/view/Transcontinental-2014-Cycling-Cap#': '/#!/products/id/6986/white',
    '/product/view/Transcontinental-2014-Cycling-Musette': '/#!/products/transcontinental-musette-2015/black',
    '/product/view/Transcontinental-2014-Cycling-Musette#': '/#!/products/transcontinental-musette-2015/black',
    '/blog/pedal-ed-supports-the-transcontinental-race-2015': '/#!/stories/Transcontinental-Race-2015',
    '/blog/Transcontinentalrace-2014': '/#!/stories/Transcontinental-Race-2015',
    '/category/new-arrivals': '/#!/newarrivals',
    '/terms-and-conditions': '/#!/faq#payment',
    '/shipping-and-returns': '/#!/faq#payment',
    '/contact-us': '/#!/faq#payment'
};

app.use(function(req, res, next){
    var rootIndex = _.keys(conv).indexOf(req.originalUrl);
    if(rootIndex > -1) {
        res.redirect(conv[req.originalUrl]);
    } else {
        next();
    }
});

app.get('/blog/:all',function(req,res){
    res.redirect('/#!/stories');
});

app.get('/search/:all',function(req,res){
    res.redirect('/#!/products');
});

app.use(serveSnapshots);
app.use(express.static(path.join(__dirname, appDir),{maxage: 86400000, extensions: ['html']}));

http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port %d in %s mode", app.get('port'), appDir);
});



function serveSnapshots(req,res,next){
    //manage escaped_fragment for google and facebook bots
    //serve snapshots generated with "grunt snapshots"
	if (req.originalUrl.indexOf('_escaped_fragment_') > -1) {
        var escapedUrl = decodeURIComponent(req.originalUrl);

        var split = req.originalUrl.split("?_escaped_fragment_=");
        var page = split[split.length-1];
        var prefix = split[0];

        //redirect to index if page is /
        if ((!page) || (page=="/")) page = "/index";

        //redirect: /products/.../color --> /products/...
        if ((m=/\/products\/name\/([^\/]*)/.exec(page)) !== null) page = m[0];
        else if ((m=/\/products\/id\/([^\/]*)/.exec(page)) !== null) page = m[0];
        else if ((m=/\/products\/([^\/]*)/.exec(page)) !== null) page = m[0];

        var filename = __dirname + prefix + "snapshots" + page + ".html";
        console.log(new Date()+ " Serving snapshot: "+filename);
        var rstream = fs.createReadStream(filename);
        rstream.on("open",function(){
            rstream.pipe(res);
        })
        rstream.on("error",function(err){
            res.status(500).send(err);
        })

	}
	else next();
}


function getStories(){
    var items = [
        {
            id: 'The-Transcontinental-Race-2015-is-over',
            title: 'The Transcontinental Race 2015 is over',
            description: 'Our friends Niccolò and Jacopo are back to their normal life, they closed the race in 15 days, 13 hours and ...',
            image: '/content/images_api/stories_new/TCR2015/big/TCR3-250715-075.jpg',
            // authors: "Words by Kristof Allegaert<br>Photos by Matthias Wjst",
            text: '<p>Our friends Niccolò and Jacopo are back to their normal life, they closed the race in 15 days, 13 hours and 14 minutes. We share with you a brief interview we made. We are just waiting for the next edition, see you there!</p><strong>One word to describe the TCR experience?</strong><p>N: Fear</p><p>J: Delirium (not in Vegas)</p><strong>How many kilometers did you ride in total?</strong><p>N: I would say from about 3.900 to 4.100 kilometers, actually in Albania the Garmin decided to party hard…</p><p>J: No Garmin No Rules, at a certain moment i turned it off, i trust much more Niccolò</p><strong>Which day was the most beautiful?</strong><p>N: For me the Albanian stage, from Kotor to Burrel, climbing up the Lovcen and escaping from zombie dogs</p><p>J: Defenitely the Assietta one, we had a group ride with many friends and then we climbed down to Torino chilling with some beers at the bar</p><strong>And what about the most strenuous one?</strong><p>N: Every day when i assumed it was an easy one</p><p>J: Well, the day after Torino, i had too many beers the night before</p><strong>Did you have technical and mechanical troubles during the race?</strong><p>N-J : Not having them during the TCR is like reading a book without turning pages</p><strong>And what about psychological and physical ones?</strong><p>N: One day, you realize you have tendons and knees and you discover the magical power of anti-inflammatory drug</p><p>J: The physical ones disappear, you have already the psychological ones if you decided to race the TCR</p><strong>Did you sometimes get lost?</strong><p>N: Yes sir! In Bosnia we took a wrong turn, that resulted in a 6 hour ride with 50 Km of gravel road and 3.000 meters of climbing</p><p>J: Bosnians have problems with cartography, it’s not our fault!</p><strong>Did you ever doubt you would ever reach Istanbul?</strong><p>N: Every single moment, even just before the finish line, i was scared to be crashed from a Taxi</p><p>J: Yes, till the end i was in doubt. The last 5 km i rode on my back rim, but i promised to myself to finish it even walking and carrying my bike</p><strong>What was your daily food?</strong><p>N: Totally random trash food, just looking for calories</p><p>J: Red Bull, Mars, Bread. Repeat 8 times a day for 15 days</p><strong>Did you train in some special way before the race?</strong><p>N: Absolutely not, i was sure that Assietta was in Tuscany , i am sure you can imagine the rest.</p><p>J: There is not training for this kind of race, i mean i have a life and for me there are more important things to do than training for the TCR. Kind of like Cavendish-style when he is overweight..</p><strong>What kind of weather conditions did you ride through?</strong><p>N – J: We had rainy days crossing every single country border and of course we rode headwind for 2.700 km…</p><strong>Did you meet other companions along the way?</strong><p>N: Yes, Jacopo</p><p>J: Yes, Niccolò</p><strong>What are you missing more after your return to normal life?</strong><p>N: When i sleep in my bed i miss the sky and the stars, but when i was sleeping under the sky, i was missing my bed</p><p>J: An E-Bike</p><strong>Describe your feelings once entering Istanbul.</strong><p>N: Hell yeah! Turkish coffee!</p><p>J: “ If you have tears, prepare to shed them now” that’s a Shakespeare verse taken from the Julius Cesar</p><strong>Who would you thank?</strong><p>J: My body, thanks body for keeping strong! And of course Niccolò, my buddy.</p><p>N: Those who know, they know, i want to hug them all!</p><strong>The Transcontinental Race 2015 is over</strong>',
            topImage: '/content/images_api/stories_new/TCR2015/TCR3-DAY5-290715-027_header.jpg',
            gallery:[]
        },
        {
            id: 'Transcontinental-Race-2015',
            title: 'Transcontinental Race 2015',
            description: 'The Tour de France has changed over the last 102 editions from a treacherous, long-distance race that relied...',
            image: '/content/images_api/stories_new/TCR2015/small/TCR3-DAY5-290715-020.jpg',
            // authors: "Words by Kristof Allegaert<br>Photos by Matthias Wjst",
            text: '<p><img src="/content/images_api/stories_new/TCR2015/big/TCR3-DAY5-290715-020.jpg"/></p><p>The Tour de France has changed over the last 102 editions from a treacherous, long-distance race that relied solely on the adventurous and resourceful spirits of its entrants, to the great corporate caravan it is today. It\'s still a great spectacle, but the original glory of the event no longer holds every rider in awe.</p><p>It is that authenticity that the Transcontinental hopes to provide to the modern age — an unsupported, single stage competition, in which the boundaries of the riders\' courage and preparation are truly tested. The 2015 event started on July 24th and finished with countless stories of bravery and endurance.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY4-034.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY4-032.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY4-149.jpg\');"></div></div></div><p>PEdALED were the main sponsors of the event for the second time, with riders Niccolò and Jacopo, who like the other racers, brought along only the minimum essentials for the 4,000km ride. Among these were included the Kaido Jersey, Kaze Access Vest, Raku Bib Short, Gufo Jacket, Kaido Arm Warmers and, of course, the standard issue Transcontinental Cap.</p><p><img src="/content/images_api/stories_new/TCR2015/big/TCR3-250715-075.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR3-270715-141.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY1-122.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY4-020.jpg\');"></div></div></div><p>This was the third Transcontinental Race, in which 175 racers set off from the Muur van Geraardsbergen in Belgium, finishing in the Bosphorus strait in Istanbul. There were four checkpoints: Mont Ventoux in Provence, the Strada dell’Assietta on the French-Italian border, Vukovar in Croatia, and finally Montenegro’s Mount Lovcen.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY4-005.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY5-015.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY8-019.jpg\');"></div></div></div><p>Completing the race within the competitive time frame meant riding up to 250 kilometres a day and sleeping only when absolutely necessary, in any location possible. Sometimes by the side of the road, other times in underground car parks.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY9-007.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR3-250715-088.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY9-070.jpg\');"></div></div></div><p>Its a long road with wind and rain and many days between, but PEdALED gear is made for real living. The Kaido Jersey, for example, is made from breathable Merino wool, which doesn\'t smell, even after being worn for many days and only washed by the rain.</p><p><img src="/content/images_api/stories_new/TCR2015/big/TCR3-270715-162.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY9-074.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY9-072.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY9-076.jpg\');"></div></div></div><p>Even after such hardship, Niccolò and Jacopo still managed to stay relatively comfortable, and as anyone who has completed such feats will testify, the less you have to think about your kit, the easier it is to manage the pain and keep your mind focused on reaching your goal.</p><p>Niccolò and Jacopo finished in 15 days, 13 hours and 14 minutes.</p><p>Read Niccolò and Jacopo\'s post-race interview here.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-059.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-060.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-065.jpg\');"></div></div></div><p><img src="/content/images_api/stories_new/TCR2015/big/TCR-DAY-ARRIVE-058.jpg"/></p><p>Special thanks to Giovanni Maria Pizzato for the exceptional photography</p><p>Special thanks to Adam Leddin, founder of Cycle EXIF, for writing this story</p>',
            topImage: '/content/images_api/stories_new/TCR2015/TCR3-DAY5-290715-027_header.jpg',
            gallery:[
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR3-250715-075.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR3-250715-075.jpg",
                    id: 1
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR3-250715-088 (1).jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR3-250715-088 (1).jpg",
                    id: 2
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR3-270715-141.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR3-270715-141.jpg",
                    id: 3
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR3-270715-162.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR3-270715-162.jpg",
                    id: 4
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR3-DAY5-290715-020.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR3-DAY5-290715-020.jpg",
                    id: 5
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY1-122.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY1-122.jpg",
                    id: 6
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY4-005.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY4-005.jpg",
                    id: 7
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY4-020.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY4-020.jpg",
                    id: 8
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY4-032.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY4-032.jpg",
                    id: 9
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY4-034.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY4-034.jpg",
                    id: 10
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY4-149.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY4-149.jpg",
                    id: 11
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY5-015.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY5-015.jpg",
                    id: 12
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY5-029.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY5-029.jpg",
                    id: 13
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY5-036.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY5-036.jpg",
                    id: 14
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY5-045.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY5-045.jpg",
                    id: 15
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY8-019.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY8-019.jpg",
                    id: 16
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY9-007.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY9-007.jpg",
                    id: 17
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY9-070.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY9-070.jpg",
                    id: 18
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY9-072.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY9-072.jpg",
                    id: 19
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY9-074.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY9-074.jpg",
                    id: 20
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY9-076.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY9-076.jpg",
                    id: 21
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-058.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY-ARRIVE-058.jpg",
                    id: 22
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-059.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY-ARRIVE-059.jpg",
                    id: 23
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-060.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY-ARRIVE-060.jpg",
                    id: 24
                },
                {
                    small:"/content/images_api/stories_new/TCR2015/small/TCR-DAY-ARRIVE-065.jpg",
                    big:"/content/images_api/stories_new/TCR2015/big/TCR-DAY-ARRIVE-065.jpg",
                    id: 25
                }

            ]
        },
        {
            id: 'Australia-Great-Ocean-Road',
            title: 'Australia Great Ocean Road',
            description: 'The week before was crazy with the CMWC and we were all happy but still so tired. Kit hosted Thomas, Nico...',
            image: '/content/images_api/stories_new/GOR/small/158C0817.jpg',
            authors: "",
            topImage: '/content/images_api/stories_new/GOR/top.jpg',
            text: '<p>The week before was crazy with the CMWC and we were all happy but still so tired. Kit hosted Thomas, Nico, and myself which made our departure much easier. Excited and happy, after a coffee around the corner at this famous place in Abbotsford, the Admiral Chen-go, we were off. First time for Thomas and Nico outside of the city. Bring on the Coast!</p><p><strong>Staurday 11th March<br/>Torquay - Skene’s Creek by bike 87km</strong></p><p><img src="/content/images_api/stories_new/GOR/big/158C1103.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C1161.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C1137.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0877.jpg\');"></div></div></div><p>On the road from Melbourne to Torquay, Nico realized that Torquay was the city with the beach where the legendary Point Break was filmed. Indeed Brodi aka Patrick Swayze, rode the last wave of his tragically short life on Bells Beach. Excited by the idea to walk on this beach, we started our journey with a stop here. Torquay is the start of the Great Ocean Road, or GOR, and to avoid some traffic, we drove here to start our ride.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0496.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0629.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0666.jpg\');"></div></div></div><p>Walking on the beach and making jokes about the film made us realize for the first time that we\'re making this bike trip, for real.  Kit had already ridden this road 4 or 5 times, but for Thomas and Nico, it was the first time, but even more, the first time in Australia. </p><p><img src="/content/images_api/stories_new/GOR/big/158C4804.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C4750.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C4971.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0908.jpg\');"></div></div></div><p>Immediately the landscape, turn after turn, impressed everyone with an continual succession of beach along coastline which instantly immersed us in the reality of the landscape of this part of the country. The State of Victoria is the greenest and humid part of Australia, so that means that on your left you have the Indian Ocean and on your right you have trees, forest or lowland. You won’t ever cross desert like in the middle of the country or on the west side.</p><p>Victoria is big like the United Kingdom but the smallest state of the country, and the 2nd most populated. Fun fact: you can fit 14 France’s in the entire country of Australia!</p><p><img src="/content/images_api/stories_new/GOR/big/158C0854.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5013.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5028.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5060.jpg\');"></div></div></div><p>Next Stop: Lorne Jetty. </p><p>Till that moment the weather was pretty nice but steadily the wind started to pick up. Time for a break, eat some fruits and have a walk on the pier. To be in front of the Indian Ocean is to have the sensation to be in the middle of nowhere. The guys had a chat with a local, but if you’re not Australian or english, you can’t even understand what one says! </p><p><img src="/content/images_api/stories_new/GOR/big/158C5126.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5117.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5197.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5164.jpg\');"></div></div></div><p>As is usual for this state, you can have 4 seasons in the same day… This first day was no different. Humid and sunny in the morning, then windy, sunny and rainy… After we left, some kilometers later the sun was back and it was super warm, so warm that Kit emptied his bidon on his head - each time the guys would stop to admire the landscape! This part of the road offered us a good view of the ocean and the beach, but not so much of the land to the east.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5446.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5488.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5500.jpg\');"></div></div></div><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5622.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5606.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5791.jpg\');"></div></div></div><p>This time of year, the sun goes down around 6. Due to the lots of stop we’ve made, we arrived just before nightfall at our first hotel in Skene’s Creek. After time to chill and shower, we went to the tiny city center. The main problem on this road was to find a good restaurant… not easy at all! Exhausted and starving, we ended up in a junk food place. Happy to be around a table together, jokes and funny stupid things happened.  Before going to sleep, we had a quick chat about the planning for the 2nd day: alarm clocks at 6am to catch the sunrise on the beach!</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5937.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5895.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C5900.jpg\');"></div></div></div><p><strong>Sunday 12th March<br/>Skenes Creek - Port Campbell -100km</strong></p><p>6.30am -  2nd day was the most impressive.  Absolutely no one is around, the road is our road. On foot at the beach and we understand what a great show we’re gonna have. Sand on our right, rocks on our left, ocean in front. some small waves stranded in between the rock and the hole that the saltwater created with time.It’s cold, as the wind from the ocean is still there, but we dont care. Nico walks away from us to enjoy the view. Kit doesn’t say a word and Thomas smiles facing the sunrise. Clouds hide a part of the sky.</p><p><img src="/content/images_api/stories_new/GOR/big/158C6029.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6078.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6134.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6155.jpg\');"></div></div></div><p>After few minutes we’re back on the road. Strange sensation to not have any cars around… it’s a real no man’s land!</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6271.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6291.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6338.jpg\');"></div></div></div><p>8am, time to get breakfast. Once again Nico impressed us by the quantity of food he can eat!  It’ll be a double breakfast for him: bread, eggs, jam, butter, apple kind…. and Red Bull of course.</p><p><img src="/content/images_api/stories_new/GOR/big/158C6440.jpg"/></p><p>Upon leaving, our friend the rain comes to annoy us for an hour. Along with the clouds, it gave a dramatic face to the landscape. After one turn, we could see a huge and empty beach where sky and sea were separated only by a line of rocks going far away into the ocean. the sand is super humid and sticks to our shoes. We really enjoy the feeling of the wind mixed with the salt and fresh air, like a deep breath which gives you positive energy. The rain stops at some point</p><p><img src="/content/images_api/stories_new/GOR/big/158C6499.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6651.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6511.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6515.jpg\');"></div></div></div><p>We enter then in Great Otway. Since a few kilometers the beach has given way to tropical forest. We heard that we could find waterfalls around. The first try was a fail, but we walked in the forest with the sensation to be in another country, maybe somewhere in Equador… lots of wild noise and bird vocals diving at us in this tropical atmosphere. After meeting a Koala (Kangaroo and Koala are pretty shy, so it was a great discovery) we made a stop to have a lunch. Probably the worst lunch we never had in our entire life! I dont how its even possible to serve food like that… </p><p><img src="/content/images_api/stories_new/GOR/big/158C6848.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6914.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6969.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C6941.jpg\');"></div></div></div><p><img src="/content/images_api/stories_new/GOR/big/158C7333.jpg"/></p><p>Anyway, let’s go back in the forest to find waterfalls. After a walk of about 1 hour we find one. One hour before we were on a lowland in England, and now in the middle of eucalyptus forest!</p><p>It’s so good to be so far from the city. Far from the cars, the noise, the sometimes stupid people. Surrounded by trees, animals and water, what else? Each of the guys took his time to admire the scenery… they know that they won’t have another opportunity like that again soon… SERENITY is the perfect word to describe the situation.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9068.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9057.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9106.jpg\');"></div></div></div><p>Back on the road quickly to be just in time at The 12 Apostles Bay. The 12 Apostles are 12 pieces of rock which are in the water separated from the land and the cliff. This day is neverending… each stop is the opportunity to see a landscape more beautiful than the one before! How is it possible? The sunset is perfect and the light is unbelievable. Guys can’t believe what they have in front of them and are smiling like kids.</p><p><img src="/content/images_api/stories_new/GOR/big/158C9416.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9409.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9436.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9526.jpg\');"></div></div></div><p>Port Campbell: Went to the grocery to get food and cook in this amazing apartment with an ocean view. And guess what, we get to watch Paris-Roubaix!</p><p><strong>Monday 13th March<br/>Port Campbell - Warrnambool by bike 80km</strong></p><p>Not enough satiated by sunrise the previous morning, will dedicated ourselves to catch the golden light. </p><p>Again, another straight road with nobody and no cars around. We met a kangaroo.  He crossed the road in front of the car. It’s super freezing and the guys shake a lot but they forget the temperature because of the light and the scenery.</p><p>Another stop along the ocean, on the top of the cliff. </p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9623.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9732.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C9749.jpg\');"></div></div></div><p><img src="/content/images_api/stories_new/GOR/big/158C9648.jpg"/></p><p>Then it’s back to the apartment to have a breakfast on the terrace in the morning sun.</p><p>Time for the guys to check the map and discuss about the last day on the GOR. </p><p><img src="/content/images_api/stories_new/GOR/big/158C0226.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0199.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0253.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0271.jpg\');"></div></div></div><p>Back on the road for the last part of the trip. On this part the roads are super straight for 10 or even 20km… surrounded by lowland. Cars and trucks are back because we’re close to the big city of the region, Warrnambool. Lots of farmland around with cow and cattle. Some dead fox are suspended on a barrier… the ground seems to melt when you look away from the road.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C4692.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C0909.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/GOR/small/158C4694.jpg\');"></div></div></div><p><img src="/content/images_api/stories_new/GOR/big/158C0817.jpg"/></p><p>10k before the city the main road become a sort of Highway. Kit and Thomas understand the danger but Nico doesn’t want to get off his bike. Head on the handlebar, he keep going.  The day winds down. We didn’t eat lunch today to be sure to be in time to the city. After food, its time to be back in Melbourne, which is 3 hours by car. Some of them fall asleep while others make jokes.  In one week, we’ll be together again to discover the streets of Melbourne!</p>',
            gallery:[
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0199.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0199.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0226.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0226.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0253.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0253.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0271.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0271.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0496.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0496.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0629.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0629.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0666.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0666.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0817.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0817.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0854.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0854.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0877.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0877.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0908.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0908.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C0909.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C0909.jpg",
                    id: 11
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C1103.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C1103.jpg",
                    id: 12
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C1137.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C1137.jpg",
                    id: 13
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C1161.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C1161.jpg",
                    id: 14
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C4692.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C4692.jpg",
                    id: 15
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C4694.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C4694.jpg",
                    id: 16
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C4750.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C4750.jpg",
                    id: 17
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C4804.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C4804.jpg",
                    id: 18
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C4971.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C4971.jpg",
                    id: 19
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5013.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5013.jpg",
                    id: 20
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5028.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5028.jpg",
                    id: 21
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5060.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5060.jpg",
                    id: 22
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5117.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5117.jpg",
                    id: 23
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5126.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5126.jpg",
                    id: 24
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5164.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5164.jpg",
                    id: 25
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5197.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5197.jpg",
                    id: 26
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5446.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5446.jpg",
                    id: 27
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5488.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5488.jpg",
                    id: 28
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5500.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5500.jpg",
                    id: 29
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5606.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5606.jpg",
                    id: 30
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5622.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5622.jpg",
                    id: 31
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5791.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5791.jpg",
                    id: 32
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5895.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5895.jpg",
                    id: 33
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5900.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5900.jpg",
                    id: 34
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C5937.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C5937.jpg",
                    id: 35
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6029.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6029.jpg",
                    id: 36
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6078.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6078.jpg",
                    id: 37
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6134.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6134.jpg",
                    id: 38
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6155.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6155.jpg",
                    id: 39
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6271.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6271.jpg",
                    id: 40
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6291.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6291.jpg",
                    id: 41
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6338.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6338.jpg",
                    id: 42
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6440.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6440.jpg",
                    id: 43
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6499.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6499.jpg",
                    id: 44
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6511.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6511.jpg",
                    id: 45
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6515.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6515.jpg",
                    id: 46
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6651.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6651.jpg",
                    id: 47
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6848.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6848.jpg",
                    id: 48
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6914.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6914.jpg",
                    id: 49
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6941.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6941.jpg",
                    id: 50
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C6969.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C6969.jpg",
                    id: 51
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C7333.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C7333.jpg",
                    id: 52
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9057.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9057.jpg",
                    id: 53
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9068.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9068.jpg",
                    id: 54
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9106.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9106.jpg",
                    id: 55
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9409 2.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9409 2.jpg",
                    id: 56
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9409.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9409.jpg",
                    id: 57
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9416.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9416.jpg",
                    id: 58
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9436.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9436.jpg",
                    id: 59
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9526.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9526.jpg",
                    id: 60
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9623.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9623.jpg",
                    id: 61
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9648.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9648.jpg",
                    id: 62
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9732.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9732.jpg",
                    id: 63
                },
                {
                    small: "/content/images_api/stories_new/GOR/small/158C9749.jpg",
                    big: "/content/images_api/stories_new/GOR/big/158C9749.jpg",
                    id: 64
                }
            ]
        },
        {
            id: 'From-Japan-To-The-World',
            title: 'From Japan To The World',
            description: 'In 2007, Hideto Suzuki left the high-paced Japanese fashion world to create beautiful and functional apparel...',
            image: '/content/images_api/stories_new/ABOUT HIDETO/big/HIDETO-B-001.jpg_small.jpg',
            authors: "Words by PEdAL ED<br>Photos by Ryotaraw Taniguchi  / Poci",
            text: '<p>In 2007, Hideto Suzuki left the high-paced Japanese fashion world to create beautiful and functional apparel to complement his cycling lifestyle in Tokyo.</p><p><img src="/content/images_api/stories_new/ABOUT HIDETO/big/HIDETO-B-001.jpg_small.jpg"/></p><p>Since then, he has gone on to create award-winning collections based on his belief that garment details can improve function and performance for cycling, without compromising aesthetics.</p><p><img src="/content/images_api/stories_new/ABOUT HIDETO/big/HIDETO-B-002.jpg_small.jpg"/></p><p>Hideto’s journey represents the people that have chosen the bicycle as an inseparable part of their everyday life.</p><p>“...the importance of making things is to create each object with a sincere attitude. Inspired by my daily cycling I designed my personal clothing to wear on the bike...”</p><p>PEdAL ED garments bring together Hideto’s belief in the aesthetic and function of cycle clothing through timeless and purposeful designs.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-003.jpg_small.jpg\');"></div></div></div><p>For transportation, recreation, sports or any riding PEdAL ED products are companions for life.</p>',
            topImage: '/content/images_api/stories_new/ABOUT HIDETO/big/top.jpg',
            gallery: [
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/ABOUT HIDETO/gallery/HIDETO-G-009.jpg_big.jpg",
                    id: 8
                }
            ]
        },
        {
            id: 'Artcrank',
            title: 'Artcrank',
            description: 'There are two ways in Sicily that can give you that castaway feeling, jump on a boat and let yourself go in ...',
            image: '/content/images_api/stories_new/Artcrank/big/ARTCRANK-B-001.jpeg_small.jpeg',
            authors: "Words by PEdAL ED<br>Photos by Artcrank",
            text: '<p>ARTCRANK was founded in 2007 as “A Poster Party For Bike People.”</p><p>PEdAL ED has teamed up with Brooks to celebrate five years ARTCRANK UK, the world’s leading bicycle poster show.</p><p><img src="/content/images_api/stories_new/Artcrank/big/ARTCRANK-B-001.jpeg_small.jpeg"/></p><p>The one-off retrospective event will be the first in a series of collaborations between ARTCRANK, PEdAL ED, and Brooks England, who have teamed up to host poster parties in London and Bristol for 2014.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-001.jpeg_small.jpeg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-002.jpeg_small.jpeg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-003.jpeg_small.jpeg\');"></div></div></div>',
            topImage: '/content/images_api/stories_new/Artcrank/big/top.jpg',
            gallery: [
                {
                    small: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-001.jpeg_small.jpeg",
                    big: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-001.jpeg_big.jpeg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-002.jpeg_small.jpeg",
                    big: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-002.jpeg_big.jpeg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-003.jpeg_small.jpeg",
                    big: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-003.jpeg_big.jpeg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-003.jpeg_small.jpeg",
                    big: "/content/images_api/stories_new/Artcrank/gallery/ARTCRANK-G-003.jpeg_big.jpeg",
                    id: 3
                }
            ]
        },
        {
            id: 'Castaway-On-A-Bicycle',
            title: 'Castaway On A Bicycle',
            description: 'There are two ways in Sicily that can give you that castaway feeling, jump on a boat and let yourself go in ...',
            image: '/content/images_api/stories_new/Castaway on a Bicycle/big/CASTAWAY-B-001.jpg_small.jpg',
            authors: "Words by Paolo Ciaberta<br>Photos by Paolo Ciaberta",
            text: '<p>There are two ways in Sicily that can give you that castaway feeling, jump on a boat and let yourself go in the sea or jump on a bike and lose yourself in the backcountry. I have a bike and not a boat, I suffer of seasickness and whenever I’m on a bike I fell happy. The choice has been simple.</p><p>Far from the wealthy coast, the backcountry is rougher, harder and austere, meanwhile sweet, welcoming, suspended in the light and shadow of an island set in the Mediterranean sea looking both ways towards Africa, Europe and Asia.</p><p><img src="/content/images_api/stories_new/Castaway on a Bicycle/big/CASTAWAY-B-001.jpg_small.jpg"/></p><p>The need for that castaway feeling came because I was looking for ample and unpopulated places, wild and far, where time seemed to stop, contest that I consider ideal to move legs and head. Usually when I get out of home in my bike, for my Sunday ride, I often find myself in places where I already know every single roughness of the road and I don’t think about anything else a part from riding and feeling like a child.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-003.jpg_small.jpg\');"></div></div></div><p>Cycle touring instead imposes organization and attention, you have to face most situations by trusting only your own instinct, your senses. You have to feel the noises, the air, follow the traces more like an animal than a child, thing that I find moving and gratifying.</p><p>I did almost 350 km, I know that if you don’t go at least around the world is not surprising anymore, but I believe that a journey is not just about distance, it is a matter of deepness, so I could say that they have been 350 km done with care.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-004.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-006.jpg_small.jpg\');"></div></div></div><p>State highways, country roads, unpaved roads, constantly brightened up by the colors of amazing valleys, corn fields, citrus groves and large grazing. I’ve ridden absorbed in the beauty of an intense light that shows you afar and reconciles you with your spirit. The silences of desolated lands, not yet oppressed by the July sun, makes you feel the freedom of listening just to yourself and your bike.</p><p><img src="/content/images_api/stories_new/Castaway on a Bicycle/big/CASTAWAY-B-002.jpg_small.jpg"/></p><p>On the background stunning little towns on top of mountains are looking gorgeous. They instantly shows how hard it will be to get there, but the desire of reaching them gives you unexpected motivations. May is already hot and sometimes tiredness kills every enthusiasm, but you can not have the wind in your favour every time.</p><p><img src="/content/images_api/stories_new/Castaway on a Bicycle/big/CASTAWAY-B-003.jpg_small.jpg"/></p><p>This is a journey that awake senses, a surprising journey , a journey where one can lose and find himself again.</p><p>In 1817 Goethe published “ Viaggio in Italia” and about Sicily he wrote: “ Italy without Sicily does not leave any image to the spirit. Here is the keyof everything. Purity of contour, suavity of ensemble, degrade of tones, harmony of the sky sea and heart…who has seen them once will never forget them.”</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-007.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-008.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-009.jpg_small.jpg\');"></div></div></div><p>Surely is not the island that Gohete described 200 years ago anymore during his journey in Italy, but there is no doubt that who crosses this land, even just one time, will never forget it for his entire life.</p>',
            topImage: '/content/images_api/stories_new/Castaway on a Bicycle/big/top.jpg',
            gallery: [
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0012.jpg_big.jpg",
                    id: 11
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0013.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0013.jpg_big.jpg",
                    id: 12
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0014.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0014.jpg_big.jpg",
                    id: 13
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0015.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0015.jpg_big.jpg",
                    id: 14
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0016.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0016.jpg_big.jpg",
                    id: 15
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0017.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0017.jpg_big.jpg",
                    id: 16
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0018.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0018.jpg_big.jpg",
                    id: 17
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0019.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0019.jpg_big.jpg",
                    id: 18
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0020.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0020.jpg_big.jpg",
                    id: 19
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0021.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0021.jpg_big.jpg",
                    id: 20
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0022.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0022.jpg_big.jpg",
                    id: 21
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0023.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0023.jpg_big.jpg",
                    id: 22
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0024.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0024.jpg_big.jpg",
                    id: 23
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0025.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0025.jpg_big.jpg",
                    id: 24
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0026.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0026.jpg_big.jpg",
                    id: 25
                },
                {
                    small: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0027.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Castaway on a Bicycle/gallery/CASTAWAY-G-0027.jpg_big.jpg",
                    id: 26
                },
            ]    
        },
        {
            id: 'CMWC-2015',
            title: 'CMWC 2015',
            description: 'The Cycle Messenger World Championship (CMWC) is the ultimate urban cycling event. Each year, the strongest,...',
            image: '/content/images_api/stories_new/CMWC 2015/big/CMWC-B-001.jpg_small.jpg',
            authors: "Words by Paolo Ciaberta<br>Photos by Paolo Ciaberta",
            text: '<p>The Cycle Messenger World Championship (CMWC) is the ultimate urban cycling event. Each year, the strongest, smartest and fastest bike messengers from around the globe converge to determine who is the best in a variety of disciplines designed to test riders’ physical and mental limits.</p><p><img src="/content/images_api/stories_new/CMWC 2015/big/CMWC-B-001.jpg_small.jpg"/></p><p>The CMWC is at once a world championship sporting event and a festival celebrating messenger culture, a perfect match for the PEdAL ED philosophy of #dressliveride. </p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-003.jpg_small.jpg\');"></div></div></div><p>The championships host a variety of related galas, shows, parties and vendor markets where spectators mingle with messengers to get a jump on the latest bike trends and urban utilitarian fashion. The side events showcase the artistic, photographic, musical and literary talents of the world’s hardest working professional athletes. While most of the competitors are bike messengers, the events are open to everyone with many non-messengers enjoying the challenge of the competition.</p><p><img src="/content/images_api/stories_new/CMWC 2015/big/CMWC-B-002.jpg_small.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-004.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-006.jpg_small.jpg\');"></div></div></div>',
            topImage: '/content/images_api/stories_new/CMWC 2015/big/top.jpg',
            gallery: [
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0012.jpg_big.jpg",
                    id: 11
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0013.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0013.jpg_big.jpg",
                    id: 12
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0014.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0014.jpg_big.jpg",
                    id: 13
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0015.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0015.jpg_big.jpg",
                    id: 14
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0016.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0016.jpg_big.jpg",
                    id: 15
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0017.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0017.jpg_big.jpg",
                    id: 16
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0018.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0018.jpg_big.jpg",
                    id: 17
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0019.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0019.jpg_big.jpg",
                    id: 18
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0020.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0020.jpg_big.jpg",
                    id: 19
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0021.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0021.jpg_big.jpg",
                    id: 20
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0022.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0022.jpg_big.jpg",
                    id: 21
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0023.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0023.jpg_big.jpg",
                    id: 22
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0024.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0024.jpg_big.jpg",
                    id: 23
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0025.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0025.jpg_big.jpg",
                    id: 24
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0026.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0026.jpg_big.jpg",
                    id: 25
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0027.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0027.jpg_big.jpg",
                    id: 26
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0028.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0028.jpg_big.jpg",
                    id: 27
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0029.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0029.jpg_big.jpg",
                    id: 28
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0030.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0030.jpg_big.jpg",
                    id: 29
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0031.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0031.jpg_big.jpg",
                    id: 30
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0032.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0032.jpg_big.jpg",
                    id: 31
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0033.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0033.jpg_big.jpg",
                    id: 32
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0034.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0034.jpg_big.jpg",
                    id: 33
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0035.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0035.jpg_big.jpg",
                    id: 34
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0036.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0036.jpg_big.jpg",
                    id: 35
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0037.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0037.jpg_big.jpg",
                    id: 36
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0038.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0038.jpg_big.jpg",
                    id: 37
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0039.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0039.jpg_big.jpg",
                    id: 38
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0040.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0040.jpg_big.jpg",
                    id: 39
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0041.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0041.jpg_big.jpg",
                    id: 40
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0042.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0042.jpg_big.jpg",
                    id: 41
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0043.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0043.jpg_big.jpg",
                    id: 42
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0044.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0044.jpg_big.jpg",
                    id: 43
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0045.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0045.jpg_big.jpg",
                    id: 44
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0046.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0046.jpg_big.jpg",
                    id: 45
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0047.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0047.jpg_big.jpg",
                    id: 46
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0048.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0048.jpg_big.jpg",
                    id: 47
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0049.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0049.jpg_big.jpg",
                    id: 48
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0050.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0050.jpg_big.jpg",
                    id: 49
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0051.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0051.jpg_big.jpg",
                    id: 50
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0052.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0052.jpg_big.jpg",
                    id: 51
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0053.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0053.jpg_big.jpg",
                    id: 52
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0054.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0054.jpg_big.jpg",
                    id: 53
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0055.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0055.jpg_big.jpg",
                    id: 54
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0056.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0056.jpg_big.jpg",
                    id: 55
                },
                {
                    small: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0057.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CMWC 2015/gallery/CMWC-G-0057.jpg_big.jpg",
                    id: 56
                },
            ]
        },
        {
            id: 'Cycle2sydney',
            title: 'Cycle2sydney',
            description: 'IT’S  not every day you get stopped by Kazakhstan police while relieving yourself on the side of a highway. ...',
            image: '/content/images_api/stories_new/CYCLE2SYDNEY/big/C2S-B-001.jpg_small.jpg',
            authors: "Words by Rebecca Isaacs<br>Photos by Kevin Downey",
            text: '<p>IT’S  not every day you get stopped by Kazakhstan police while relieving yourself on the side of a highway. But then, cycling 25,000km across the world isn’t exactly normal.</p><p>“Once we zipped up, the officers told us we would be fined,” 56-year-old Belgian adventurer Patrick Abrams said. “I gave them my Visa card.”</p><p>Abrams and his bike-mad pal, 28-year-old Irishman Kevin Downey, will roll into Sydney on September 9 to complete their epic ride from London, covering 175km per day.</p><p><img src="/content/images_api/stories_new/CYCLE2SYDNEY/big/C2S-B-001.jpg_small.jpg"/></p><p>It’s a perfect finish to an adventure that started in 2012, when Abrams was cycling to his mother’s house for a bowl of soup.</p><p>“I passed a young man riding with luggage and a guitar and asked, ‘Where are you going?’ He told me he wanted to travel the world so I invited him for lunch.”</p><p>The young man was Downey.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-003.jpg_small.jpg\');"></div></div></div><p>After years of planning, this unlikely pair have spent the past few months scoffing burgers, riding up Chinese mountains and battling the Aussie outback together.</p><p>The odds were stacked against Abrams completing the four-and-a-half-month journey according to plan, given his doctor told him a September finish would be unlikely.</p><p>But the reformed smoker and drinker said it all came down mindset.</p><p>“If we can both make jokes, laugh and whistle when the alarm goes off at 5.30am, I don’t think our bodies have a problem with the physical effort,” he said.</p><p><img src="/content/images_api/stories_new/CYCLE2SYDNEY/big/C2S-B-002.jpg_small.jpg"/></p><p><img src="/content/images_api/stories_new/CYCLE2SYDNEY/big/C2S-B-003.jpg_small.jpg"/></p><p>Downey is a seasoned adventurer. It was only in January this year that he finished a solo world tour by bike.</p><p>After crossing Europe, China, Laos, Thailand, Malaysia, Indonesia, and the Australian East Coast, both men agreed China offered “the best package for cyclists.”</p><p>“The people, the everchanging landscapes and the food are exactly what adventure travelling is about,” Abrams said.</p><p>“With 5,200km to complete within a 30 day visa, China was a race against time,” he said.</p><p><img src="/content/images_api/stories_new/CYCLE2SYDNEY/big/C2S-B-004.jpg_small.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-004.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-006.jpg_small.jpg\');"></div></div></div><p>What’s next? Abrams will travel around Australia with his wife. Downey plans to run the Dublin marathon in October, with permission from his other half of course.</p><p>“As far as more adventures go, I’ll have to ask the girlfriend. She is Mexican and deserves a medal for her patience so maybe a new life in Mexico awaits,” he said.</p><p><img src="/content/images_api/stories_new/CYCLE2SYDNEY/big/C2S-B-005.jpg_small.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-007.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-008.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-009.jpg_small.jpg\');"></div></div></div>',
            topImage: '/content/images_api/stories_new/CYCLE2SYDNEY/big/top.jpg',
            gallery:[
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0012.jpg_big.jpg",
                    id: 11
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0013.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0013.jpg_big.jpg",
                    id: 12
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0014.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0014.jpg_big.jpg",
                    id: 13
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0015.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0015.jpg_big.jpg",
                    id: 14
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0016.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0016.jpg_big.jpg",
                    id: 15
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0017.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0017.jpg_big.jpg",
                    id: 16
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0018.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0018.jpg_big.jpg",
                    id: 17
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0019.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0019.jpg_big.jpg",
                    id: 18
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0020.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0020.jpg_big.jpg",
                    id: 19
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0021.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0021.jpg_big.jpg",
                    id: 20
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0022.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0022.jpg_big.jpg",
                    id: 21
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0023.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0023.jpg_big.jpg",
                    id: 22
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0024.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0024.jpg_big.jpg",
                    id: 23
                },
                {
                    small: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0025.jpg_small.jpg",
                    big: "/content/images_api/stories_new/CYCLE2SYDNEY/gallery/C2S-G-0025.jpg_big.jpg",
                    id: 24
                },
            ]
        },
        {
            id: 'Dario-Pegoretti',
            title: 'Dario Pegoretti',
            description: 'Dario Pegoretti is one of the most colorful characters in the bicycle industry. His hand-painted frames (cou...',
            image: '/content/images_api/stories_new/Pegoretti/big/DARIO-B-001.jpg_small.jpg',
            authors: "Words by Adam Leddin<br>Photos by Luca Mathia",
            text: '<p>Dario Pegoretti is one of the most colorful characters in the bicycle industry. His hand-painted frames (coupled with his technical innovations and custom drawn Columbus and Deda tubes) are some of the most recognizable in the industry. Whether the paint schemes are to your taste or not, Dario injects a refreshing and typically Italian dash of color and panache to the world of the custom bicycle.<p><p><img src="/content/images_api/stories_new/Pegoretti/big/DARIO-B-001.jpg_small.jpg"/></p><p>Luigino Milani was your teacher when you did your apprenticeship. In regards to your painting, who has been your influences? Do you have a favorite artist? Regarding the paint, I have many influences. Not only from artists or paintings, my interest focusing around street art, informal from the 60’s but sometimes I have taken some ideas from women magazine or from advertising banner or anything what inspired me.<p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-003.jpg_small.jpg\');"></div></div></div><p>One of your frames, the Big Leg Emma, is named after a Frank Zappa song and another, the Marcelo Thelonius refers to the jazz great Thelonius Monk. Do you listen to music while you paint, and how necessary is music to inspiring you? Music is an important part of the shop, the first thing that we do in the morning when we arrive here is to push on the amplifier button.<p><p><img src="/content/images_api/stories_new/Pegoretti/big/DARIO-B-002.jpg_small.jpg"/></p><p>Have you always been a painter? When did you start painting your frames? No, I am not a painter, we open our paint shop in the 97, without any background, was a jump in the dark, the results are not too bad.<p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-004.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-006.jpg_small.jpg\');"></div></div></div><p>Do you only paint frames, do you paint canvasses as well, or keep a sketchbook? Sometimes I paint on canvas or paper but just for myself.<p><p>A technical question: what paint do you use — enamels or something similar? The paint sequence is primer, base coat, clear coat. But for many of my ‘WILD’ paint schemes I use piece of newspaper, powder, water color, coffee grain as i feel myself on that day.<p><p>A customer can request a particular color scheme when ordering a Pegoretti. Which is your favorite scheme to paint, and why? There is no favorite, I like all of them.<p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-007.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-008.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-009.jpg_small.jpg\');"></div></div></div><p>The Ayers Rock color scheme is obviously influenced by the dot painting of the Australian Aborigines. What is the significance, did you visit Australia and Uluru? I have never been in Australia yet but the aboriginal art touched me deeply because is clean and honest and deeply personal, the harmony of the colors that they use are amazing and they reflect the spirit of the soil.<p><p><img src="/content/images_api/stories_new/Pegoretti/big/DARIO-B-003.jpg_small.jpg"/></p><p>You could have become known as a great Italian frame builder on the strength of your frames alone. How has your art helped you define yourself? It is true that i have ‘used’ my paint schemes to give to my products a strong imprint but for the first I am a frame builder and the technical part of the job, the quality of the craftsmanship, the development of new solution or tube set is more important than the aesthetic part. At the beginning my paint scheme was painted just to try to change some conventional rules, at the end of the game a frame have a surface that I can use in many different way to express myself or my thoughts.<p>',
            topImage: '/content/images_api/stories_new/Pegoretti/big/top.jpg',
            gallery: [
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/Pegoretti/gallery/DARIO-G-0012.jpg_big.jpg",
                    id: 11
                }

            ]
        },
        {
            id: 'Red-Hook-Criterium-Milano',
            title: 'Red Hook Criterium Milano',
            description: 'The third and final race in the 2014 Championship Series took place in Milano Italy on the fastest of this y...',
            image: '/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/big/RHC-B-001.jpg_small.jpg',
            authors: "Words by Adam Leddin<br>Photos by Luca Mathia",
            text: '<p>The third and final race in the 2014 Championship Series took place in Milano Italy on the fastest of this year’s courses. With 250 racers vying for the 95 spots in the main race, the qualifying rounds were hotly contested and it soon became apparent that last year’s qualifying course record of 1:37.90 would be surpassed and it was the Barcelona champion, Julio Padilla, who shattered the record with the race’s fastest qualifying time of 1:33.43.</p><p><img src="/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/big/RHC-B-001.jpg_small.jpg"/></p><p>With more than 8,000 cheering spectators lining the race course, the men’s race began with a full on 95 racer sprint into the first turn and Mario Paz Duque, at this point a specialist in the discipline, captured the first lap prime followed closely by Eduard Grosu and Julio Padilla.</p><p>The successive laps were characterized by a continuous upping of the tempo especially by the Puerto Rican based Team Impulse Stage, who yet again proved itself to be the only team – probably in the history of the Red Hook Series – to be able to strategically impose itself on the front of the race.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-003.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-004.jpg_small.jpg\');"></div></div></div><p>The 12th lap prime was easily won by Grosu in a sign of things to come. After a slight slowing of the pace on the 13th lap, the 14th saw Diego Yepez break free off the front in an echo of a similar move made by his Team Impulse Stage teammate in Barcelona. He was followed only by Ivan Ravaioli. By the next lap the duo’s gap had opened up to a notable 9 seconds with Iride – Demode Sc’s Nicholas Varani and championship leader Thibaud Lhenry taking it upon themselves to do the chase work. By the 17th lap we again had gruppo compatto and the race relaxed for just a moment. But it was Team Impulse Stage which again took the initiative on lap 19, with older brother, Alejandro Padilla, and teammate Diego Yepez going to the front to further drive the pace and split the front group.</p><p><img src="/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/big/RHC-B-002.jpg_small.jpg"/></p><p>Going into the last lap, Julio Padilla hit the front followed by Grosu, Ravaioli, Agusto Reati, Paz Duque, Neil Bezdek, Pablo Rodriguez, Nicoletti Stefano, Evan Murphy and Umberto Marengo holding the top ten positions. The last lap was an all out battle for positioning with Murphy setting the fastest lap of the race of 1:32.88 while he moved up from 10th to 5th in the final 1,300 meters.</p><p>Grosu, seasoned from a year racing against the world’s best pros, made all the right moves, coming out of the final turn first wheel, starting his sprint early, and quickly distancing his rivals in the final 200 meters to defend his crown at the Milano Red Hook Criterium. In the sprint for second, Ravaioli came out of the final turn in third wheel but was able to pass Julio Padilla who took third place.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-006.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-007.jpg_small.jpg\');"></div></div></div><p>Thibaud Henry, coming in 16th, did enough to seal the individual points classification, while Iride – Demode Sc, always visible and active near the front of the race stole the overall team classification from Team Cinelli Chrome. Diego Yepez took home the Rockstar Games Top Antagonist Award for his selfless and aggressive ride to benefit his team leader, Julio Padilla.</p>',
            topImage: '/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/big/top.jpg',
            gallery:[
                {
                    small: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/RED HOOK CRITERIUM - MILANO/gallery/RHC-G-007.jpg_big.jpg",
                    id: 6
                },

            ]
        },
        {
            id: 'Superenduro-B-Road',
            title: 'Superenduro B-Road',
            description: 'A brilliant intuition, a format fun and a great battle for the win are the ingredients of the success of thi...',
            image: '/content/images_api/stories_new/SUPERENDURO BROAD/big/BROAD-B-001.jpg_small.jpg',
            authors: "Words by Superenduro b-road<br>Photos by Nicola Damonte",
            text: '<p>A brilliant intuition, a format fun and a great battle for the win are the ingredients of the success of this edition number zero Superenduro B Road in La Morra.</p><p><img src="/content/images_api/stories_new/SUPERENDURO BROAD/big/BROAD-B-001.jpg_small.jpg"/></p><p>The bet is won. Superenduro B Road intrigued, amazed and eventually won over the 120 competitors who took part to experiment of enduro bike racing, "conceived by Franco Monchiero. Sun, wine and spirit of this exciting new formula did the rest!</p><p>At the start, the battery characteristics from 6 installments with detachments of one minute of each other to make transfers a pleasant ride with friends, sports athletes and enthusiasts but not only. There are also numerous "working in the sector" present in the race, in order to understand and evaluate not so much the event itself itself as perhaps the movement in which the show Superenduro B-Road will appeal and is giving voice. Not only Italian, but also important international figures from the US and France were on the starting line to personally test the format. Now we are sure that they will return home with the knowledge that Italy is still able to innovate.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-003.jpg_small.jpg\');"></div></div></div><p>The beauty of the landscapes of the Langhe, made even more remarkable by the summer day, the incredible hospitality of the territory, topped with delicious food and wine offered at the refreshment of the countries touched by race and the impeccable organization of Superenduro, they have certainly contributed to the participants\' satisfaction and the success of the event. But, as in any self-respecting race bike, what it really passionate audience and media was the battle for the victory, compelling and fought until the last meters.</p><p><img src="/content/images_api/stories_new/SUPERENDURO BROAD/big/BROAD-B-002.jpg_small.jpg"/></p><p>Stefano Corino and Andrea Bugnone have kept everyone in suspense for 7 hours of the race! The gap between the two has always been on the edge of the latter and at the finish of the last special stage in the paddock of La Morra the audience shuddered to know the outcome. Upon arrival of the two rivals there was a moment of suspense as to who was the winner ... the time was really tight. On the top step of the podium salt Corino for just 2/10 (about 1h and 15min of time trials) on Bugnone. In third place ranks Emanuele Poli +3: 26 from the first.</p><p>In women, the victory goes to Angela Stefani, ahead of the two sisters Elena and Valentina Busin, second and third respectively.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-004.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-006.jpg_small.jpg\');"></div></div></div><p>The compliments are still to all finishers of this first edition of Superenduro B-Road, is for believing in an event even without classification, it is to be able to finish a race, so fun and exciting, but also very hard, challenging: 110km of ups and downs of different terrains with sometimes very tiring as the long climb Champions PS3 or highly uneven as the middle section of the special Red Bull, the PS2.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-007.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-008.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-009.jpg_small.jpg\');"></div></div></div><p><img src="/content/images_api/stories_new/SUPERENDURO BROAD/big/BROAD-B-003.jpg_small.jpg"/></p><p>Even the organizational effort was not indifferent and valuable was the support of technical partners Scott, Ritchey, pedaled, Effect Mariposa, Santacruz, Brooks England, Red Bull, Muc Off and territorial Association RDR, Mangialonga Cantina Contratto, La Cantina Comunale Morra, Barolo Night, Study Physiotherapy Leonardo Plan and the municipalities of La Morra, Bossolasco, Niella Belbo, Feisoglio, Cravanzana, Borgomale, who have not only made the event possible, but also actively participated in the construction.</p><p>Now eyes are already focused on Superenduro B-Road # 2. The feedback gathered are many and equally different from each other. Will certainly make the calibrations, to refine some details, but what until recently was a subdued, it is now a reality and the curiosity that drove the participation now gives way to the expectation and the desire to be there to find out how the B -road will surprise us again in the next edition!</p>',
            topImage: '/content/images_api/stories_new/SUPERENDURO BROAD/big/top.jpg',
            gallery:[
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0012.jpg_big.jpg",
                    id: 11
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0013.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0013.jpg_big.jpg",
                    id: 12
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0014.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0014.jpg_big.jpg",
                    id: 13
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0015.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0015.jpg_big.jpg",
                    id: 14
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0016.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0016.jpg_big.jpg",
                    id: 15
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0017.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0017.jpg_big.jpg",
                    id: 16
                },
                {
                    small: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0018.jpg_small.jpg",
                    big: "/content/images_api/stories_new/SUPERENDURO BROAD/gallery/BROAD-G-0018.jpg_big.jpg",
                    id: 17
                },

            ]
        },
        {
            id: 'Transcontinental-Race-2013',
            title: 'Transcontinental Race 2013',
            description: '“London to Istanbul within two weeks, overcoming more than 3100km, 30.000m of altitude gain and crossing 14 ...',
            image: '/content/images_api/stories_new/TCR 1/big/TCR1-B-001.jpg_small.jpg',
            authors: "Words by Recep Yesil<br>Photos by ertzui ° film",
            text: '<p>“London to Istanbul within two weeks, overcoming more than 3100km, 30.000m of altitude gain and crossing 14 european borders. Unsupported, following a route of their choice, day and night, rain or shine, crossing the whole european continent.</p><p><img src="/content/images_api/stories_new/TCR 1/big/TCR1-B-001.jpg_small.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 1/gallery/TCR1-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 1/gallery/TCR1-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 1/gallery/TCR1-G-003.jpg_small.jpg\');"></div></div></div><p>We supported Recep Yesil on the first Transcontinental Race.</p>',
            topImage: '/content/images_api/stories_new/TCR 1/big/top.jpg',
            gallery:[
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 1/gallery/TCR1-G-0012.jpg_big.jpg",
                    id: 11
                },

            ]
        },
        {
            id: 'Transcontinental-Race-2014',
            title: 'Transcontinental Race 2014',
            description: 'As winner of the 1st edition, the expectations were quite high so I came out to meet the expectations. Fairl...',
            image: '/content/images_api/stories_new/TCR 2/big/TCR2-B-001.jpg_small.jpg',
            authors: "Words by Kristof Allegaert<br>Photos by Matthias Wjst",
            text: '<p>As winner of the 1st edition, the expectations were quite high so I came out to meet the expectations.</p><p>Fairly early in the year I started reeling off my mileage so as to have a good foundation. Once the list of participants was available I took a good look at what names were on it, of course, there was Richard D. and he wanted to take revenge. Also keep in mind that there were some strangers who wanted to set a very good time.</p><p>Because there is no route set I got to make one, this was work that was quite meticulously done. What is the “right” way, this road is faster …? I created a rough schedule of the day distances.</p><p><img src="/content/images_api/stories_new/TCR 2/big/TCR2-B-001.jpg_small.jpg"/></p><p>August 9: London > Persan: 357 km</p><p>At the start I had a quick chat with Richard who told me that it was possible to catch the 12 noon ferry from Dover. My first thought was this is impossible but because we rode with a neat bunch, with a bicycle speed of 40km / h between London and Dover, we arrived in Dover at 11:35. Because the queue at the counter for the company we’d booked with was taking a long time we bought another ticket with competitor, the check-in had already ended but issuing 3 tickets wasn’t too much work and we took a ferry at 12:15. In short, I was ahead of my schedule. Once in Calais we each chose our own best path. My plan was to sleep before the first control.</p><p><img src="/content/images_api/stories_new/TCR 2/big/TCR2-B-002.jpg_small.jpg"/></p><p>August 10: Persan > Belfort: 450 km</p><p>Crossing the centre of Paris is always a bit of adventure, even on a Sunday morning but finally sometime before 6am I arrived at the first control in 6th place. To receive the stamp from Mike Hall was fantastic but I was more than 3 hours behind the first racer. My question was actually who is Josh, as an experienced 24 hour rider, I had to take account of him. In the afternoon, near Troyes I see him standing with his bike at a bakery, we wave and I was very happy to know that we were “together” again.</p><p>The weather went from bad to worse, first normal rain then changing to pouring rain and in the evening even to a small tornado. The trees in the forest gave some shelter but I had to hold my handlebars with both hands to avoid being blown away. In retrospect, I should have just waited half an hour at the side of the road, but yes, it’s a race and you don’t want to lose too much time so you continue. Because there was no better weather in sight I decided to sleep near Belfort.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-001.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-002.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-003.jpg_small.jpg\');"></div></div></div><p>August 11: Belfort > Stelvio: 439 km</p><p>Mostly rain. After a short night of sleep much rain had already fallen but apparently there was still enough in the air to make it keep raining. All day. Because everything you have is wet it can’t get even wetter! Continuing is the only solution, keep cycling. The advantage of this trick is of course that the miles accumulate. Switzerland is familiar ground to me, and Flüelapass Ofenpass was reached around 5pm. The latter, of course, opened the perspective of still cycling the Stelvio Pass the same day.</p><p><img src="/content/images_api/stories_new/TCR 2/big/TCR2-B-003.jpg_small.jpg"/></p><p>August 12: Stelvio > Bakar: 487 km</p><p>The advantage today is that it will be downhill for a long time now and I can leave the clouds behind me. Today it will be just a day to do kilometers.</p><p>Around Trieste it was clear that we are back in a very touristic place, the advantage is that there are many shops and restaurants. I’ve actually never seen the border with Slovenia, but the drive out of it was just a little show of your passport. Slovenia was still good for 30 kilometers. On to the third country for today “Long Live Europe”, Croatia, where the border was a serious matter. Once over the border it was back downhill to Rijeka where there are too many police were around. OK, no police is also something but this was too much.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-004.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-005.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-006.jpg_small.jpg\');"></div></div></div><p>August 13: Bakar > Capljina: 438 km</p><p>Soon I was cycling on a nice stretch of road along the coastline in Croatia. But because I feared large crowds along this was I chose a parallel way inland. This road ran through an incredibly beautiful piece of nature that certainly amazed me.</p><p>Although it was a very empty area there was quite a lot of traffic. Begining around noon I started looking for somewhere to have a meal but there was not much choice, the only solution to continue cycling until I found something. Around 1pm I see advertising for a restaurant 5km further, hoping I’m not too late because I was really starting to get hungry and thirsty too. Once there, all tables are perfectly laid out but it turns out I cannot get a meal! They are expecting three full buses with tourists and need to operate very smoothly.</p><p>15km from here, there is another restaurant where they certainly could serve as I explained that I had really big appetite and half an hour is worth 15 km of cycling. She wanted to give me a sandwich “on the go”. Simply fantastic. This turned out to be a kind of religious region, hence the “many tourist” traffic.</p><p>The rest of the afternoon a little up and down with a nice head wind, it would be too easy to be sure. Once darkness fell I came from many small groups of people who were walking along the road. My first idea was that this was something like a night walk, but there were just too many. Then once again stopped to ask for an explanation and I got a response that they were from a kind of pilgrimage / penance.</p><p><img src="/content/images_api/stories_new/TCR 2/big/TCR2-B-004.jpg_small.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-007.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-008.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-009.jpg_small.jpg\');"></div></div></div><p>August 14: Capljina > Podgorica: 344km</p><p>Today I planned to arrive at CP3 around noon but first I had to do my last kilometers in Bosnia, the border to Monte Negro and then the famous climb to Lovcen.</p><p>Because the ride to the lake took longer than expected, I took a meal at the lake before I started the climb.</p><p>Once I get to the top of the checkpoint Mike told me that I had taken an illegal ferry. UHHH it felt like a slap in the face, ferry / illegal / ..? Of course I could not say anything when Mike showed it on paper, partly because I did this unintentionally. To correct this error there was nothing else to do but to redo the whole piece around the lake, which is actually an inlet of the sea, cycling back. 5 hrs later I came back up to the checkpoint where I heard that everything is in order.</p><p>I had by this error / detour / loss of time found a new motivation to go!</p><p>August 15: Podgorica > Sofia: 704 km</p><p>My first job today was to go over an unknown col, I knew neither the length nor height, I just knew I had to go.</p><p><img src="/content/images_api/stories_new/TCR 2/big/TCR2-B-005.jpg_small.jpg"/></p><p>Because I was in Kosovo, near the border, I saw quite a lot of soldiers, this was a result of the war, there is still a fire smouldering? The people there have a fantastic piece of nature but all their waste was dumped along one piece of road. If you should happen to be cycling against wind it is a dreadful smell. That night I tried to send a message via my mobile phone to my wife but it seemed to not work. Phone off and back on might be a solution but no. What is going on here? Again I try and I actually started to get scared when there really was nothing very wrong. In fact, the only solution was to just keep cycling for as long as possible, to give those watching at home a reassuring feeling. As long as my “dot” moved on the map I was alive. When I stopped around midnight at a restaurant there I asked if I could send a message to say that everything was OK via their mobile phone. I’ve heard that the service of the national telephone company is very bad. Because the border with Bulgaria came into view I then once again rode well. Back to three countries in one day.</p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0010.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0011.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0012.jpg_small.jpg\');"></div></div></div><p>August 16: Sofia > Istanbul: 608 km</p><p>Now the finish line was in sight and I had passed the largest mountains so I could start the countdown. But boredom began to strike after a short time, monotony, concentration faded, actually a very dangerous combination.</p><p><img src="/content/images_api/stories_new/TCR 2/big/TCR2-B-001.jpg_small.jpg"/></p><div class="gallery"><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0013.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0014.jpg_small.jpg\');"></div></div><div class="gallery-item"><div style="background-image: url(\'/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0015.jpg_small.jpg\');"></div></div></div><p>Then I got the bright idea to finish before 8am English time! This felt agreeable, 400km and 16 hours before Big Ben struck, very risky given the circumstances, but I accepted the challenge within the challenge!  This also meant that I had to keep cycling, no time to sleep or even have a slightly longer rest break.  Only then did I begin to realize what I had said, hopefully I would not lose too much time at the border with Turkey, no flat tyre(s) …. Because of the late hour, there was not much traffic on the road so I could keep cycling at a nice pace which made it good and the challenge went ahead. So good that I was able to build a small margin, so why not refine my goal, build on this a little more?  I eventually arrived at 7am exactly English time, the sprint to arrive at 6:59 failed, a total time between the start in London and finish in Istanbul 7 days 23h 00min.</p>',
            topImage: '/content/images_api/stories_new/TCR 2/big/top.jpg',
            gallery:[
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-001.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-001.jpg_big.jpg",
                    id: 0
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-002.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-002.jpg_big.jpg",
                    id: 1
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-003.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-003.jpg_big.jpg",
                    id: 2
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-004.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-004.jpg_big.jpg",
                    id: 3
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-005.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-005.jpg_big.jpg",
                    id: 4
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-006.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-006.jpg_big.jpg",
                    id: 5
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-007.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-007.jpg_big.jpg",
                    id: 6
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-008.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-008.jpg_big.jpg",
                    id: 7
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-009.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-009.jpg_big.jpg",
                    id: 8
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0010.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0010.jpg_big.jpg",
                    id: 9
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0011.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0011.jpg_big.jpg",
                    id: 10
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0012.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0012.jpg_big.jpg",
                    id: 11
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0013.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0013.jpg_big.jpg",
                    id: 12
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0014.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0014.jpg_big.jpg",
                    id: 13
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0015.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0015.jpg_big.jpg",
                    id: 14
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0016.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0016.jpg_big.jpg",
                    id: 15
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0017.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0017.jpg_big.jpg",
                    id: 16
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0018.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0018.jpg_big.jpg",
                    id: 17
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0019.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0019.jpg_big.jpg",
                    id: 18
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0020.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0020.jpg_big.jpg",
                    id: 19
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0021.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0021.jpg_big.jpg",
                    id: 20
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0022.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0022.jpg_big.jpg",
                    id: 21
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0023.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0023.jpg_big.jpg",
                    id: 22
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0024.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0024.jpg_big.jpg",
                    id: 23
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0025.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0025.jpg_big.jpg",
                    id: 24
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0026.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0026.jpg_big.jpg",
                    id: 25
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0027.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0027.jpg_big.jpg",
                    id: 26
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0028.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0028.jpg_big.jpg",
                    id: 27
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0029.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0029.jpg_big.jpg",
                    id: 28
                },
                {
                    small: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0030.jpg_small.jpg",
                    big: "/content/images_api/stories_new/TCR 2/gallery/TCR2-G-0030.jpg_big.jpg",
                    id: 29
                },

            ]
        },
    ];


    return items;
}
